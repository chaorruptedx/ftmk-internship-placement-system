<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Button;
use kartik\form\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use kartik\time\TimePicker;
use kartik\tabs\TabsX;
use common\components\Helper;
use common\models\User;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\OrganizationDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organization-detail-profile">

    <?php if ($update !== null) : ?>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <p>
            <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['view'], ['class' => 'btn btn-secondary']) ?>
            <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <div class="container">
        <div class="main-body">    
            <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center">
                                <?php if ($update === null) : ?>
                                    <?php if ($modelAttachment->path !== null) : ?>
                                        <?= Html::img('@web/'.$modelAttachment->path, ['style' => 'width: 100%']); ?>
                                    <?php else : ?>
                                        <?= Html::img('@web/image/default_user_profile_picture.png', ['style' => 'width: 100%']); ?>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <?= $form->field($modelAttachment, 'imageFile')->widget(FileInput::classname(), [
                                        'options' => [
                                            'accept' => 'image/*',
                                        ],
                                        'pluginOptions' => [
                                            'initialPreview' => [
                                                ($modelAttachment->path !== null) ? Url::to('@web/'.$modelAttachment->path) : null,
                                            ],
                                            'initialPreviewAsData' => true,
                                            'initialCaption' => $modelAttachment->name,
                                            'initialPreviewConfig' => [
                                                ['caption' => $modelAttachment->name.'.'.$modelAttachment->type],
                                            ],
                                            'overwriteInitial' => true,
                                        ]
                                    ])->label(false); ?>
                                <?php endif; ?>
                                <div class="mt-3">
                                    <?php if ($update === null) : ?>
                                        <h4><?= $modelOrganizationDetail->name ?></h4>
                                        <p class="text-secondary mb-1">
                                            <?= Helper::getOrganizationType($modelOrganizationDetail->type) ?>
                                        </p>
                                    <?php else : ?>
                                        <?= $form->field($modelOrganizationDetail, 'name')->textInput(['placeholder' => 'Organization Name'])->label(false) ?>
                                        <?= $form->field($modelOrganizationDetail, 'type')->widget(Select2::classname(), [
                                            'data' => Helper::listOrganizationType(),
                                            'options' => ['placeholder' => 'Select Sector Type ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label(false); ?>
                                    <?php endif; ?>  
                                    <?php if (User::isUserStudent(Yii::$app->user->identity->username)) : ?>
                                        <?php if ($application_exist === false) : ?>
                                            <?= Html::a(Yii::t('app', '<span class="fas fa-check">&nbsp;</span>Apply Internship'), [
                                                '/internship-application/apply-internship',
                                                'id_organization_detail' => $modelOrganizationDetail->id,
                                            ],
                                            [
                                                'class' => 'btn btn-success',
                                                'data' => [
                                                    'confirm' => Yii::t('app', 'Are you sure you want to apply internship at <b>'.$modelOrganizationDetail->name.'</b>?'),
                                                    'method' => 'post',
                                                ],
                                            ]); ?>
                                        <?php else : ?>
                                            <?= Button::widget([
                                                'label' => '<span class="fas fa-check">&nbsp;</span>Internship Applied',
                                                'encodeLabel' => false,
                                                'options' => [
                                                    'class' => 'btn btn-secondary',
                                                    'disabled' => true,
                                                ],
                                            ]); ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">Address</h6>
                                </div>
                                <div class="col-sm-9 ">
                                    <?php if ($update === null) : ?>
                                        <?= $modelOrganizationDetail->address ?>
                                    <?php else : ?>
                                        <?= $form->field($modelOrganizationDetail, 'address')->textArea(['placeholder' => 'Organization Address'])->label(false) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">State</h6>
                                </div>
                                <div class="col-sm-9 ">
                                    <?php if ($update === null) : ?>
                                        <?= $modelOrganizationDetail->state->name ?>
                                    <?php else : ?>
                                        <?= $form->field($modelOrganizationDetail, 'id_state')->widget(Select2::classname(), [
                                            'data' => BankStatus::listState(),
                                            'options' => ['placeholder' => 'Select State ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ])->label(false); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary ">
                                    <h6 class="mb-0">Email</h6>
                                </div>
                                <div class="col-sm-9 ">
                                    <?= $modelUser->email ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">Contact Number</h6>
                                </div>
                                <div class="col-sm-9 ">
                                    <?php if ($update === null) : ?>
                                        <?= $modelOrganizationDetail->tel_no ?>
                                    <?php else : ?>
                                        <?= $form->field($modelOrganizationDetail, 'tel_no')->textInput(['placeholder' => 'Tel. No.'])->label(false) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">Working Days</h6>
                                </div>
                                <div class="col-sm-9 ">
                                    <?php if ($update === null) : ?>
                                        <?php if ($modelOrganizationDetail->start_day !== null && $modelOrganizationDetail->end_day !== null) : ?>
                                            <?= Helper::getDay($modelOrganizationDetail->start_day) ?> to <?= Helper::getDay($modelOrganizationDetail->end_day) ?>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <?= $form->field($modelOrganizationDetail, 'start_day')->widget(Select2::classname(), [
                                            'data' => Helper::listDay(),
                                            'options' => ['placeholder' => 'Select Start Day ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                        <?= $form->field($modelOrganizationDetail, 'end_day')->widget(Select2::classname(), [
                                            'data' => Helper::listDay(),
                                            'options' => ['placeholder' => 'Select End Day ...'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">Working Hours</h6>
                                </div>
                                <div class="col-sm-9">
                                <?php if ($update === null) : ?>
                                    <?php if (!empty($modelOrganizationDetail->open_hour) && !empty($modelOrganizationDetail->close_hour)) : ?>
                                        <strong>Open<br></strong><?= date("g:i A", strtotime($modelOrganizationDetail->open_hour)); ?><br>
                                        <strong>Close<br></strong><?= date("g:i A", strtotime($modelOrganizationDetail->close_hour)); ?>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <?= $form->field($modelOrganizationDetail, 'open_hour')->widget(TimePicker::classname(), [
                                        'pluginOptions' => [
                                            'showMeridian' => false,
                                        ]
                                    ]); ?>
                                    <?= $form->field($modelOrganizationDetail, 'close_hour')->widget(TimePicker::classname(), [
                                        'pluginOptions' => [
                                            'showMeridian' => false,
                                        ]
                                    ]); ?>
                                <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">Job Scope Requirements</h6>
                                    <br>
                                    <?php if ($update !== null) : ?>
                                        <?= Html::button('<span class="fas fa-info-circle">&nbsp;</span>More Info', ['class' => 'btn btn-info', 'data-toggle' => 'modal', 'data-target' => '#job-description-scope-information']) ?>
                                    <?php endif; ?>
                                </div>
                                <div class="col-sm-9">
                                <?php if ($update === null) : ?>
                                    <?php
                                        if (isset($modelOrganizationDetail->jobScopeRequirements) && is_array($modelOrganizationDetail->jobScopeRequirements))
                                        {
                                            foreach ($modelOrganizationDetail->jobScopeRequirements as $key_modelJobScopeRequirement => $modelJobScopeRequirement)
                                            {
                                                echo ++$key_modelJobScopeRequirement. ') ' .$modelJobScopeRequirement->programme->name. '<br>';
                                            }
                                        }
                                    ?>
                                <?php else : ?>
                                    <?= $form->field($modelJobScopeRequirement , 'id_programme_array')->widget(Select2::classname(), [
                                        'data' => BankStatus::listProgramme(),
                                        'options' => [
                                            'placeholder' => 'Select job scope requirement ...',
                                            'multiple' => true,
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label(''); ?>
                                <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">Job Description</h6>
                                </div>
                                <div class="col-sm-9">
                                <?php if ($update === null) : ?>
                                    <?= $modelOrganizationDetail->job_description ?>
                                <?php else : ?>
                                    <?= $form->field($modelOrganizationDetail, 'job_description')->textArea(['placeholder' => 'List Out All the Job in the Organization for Internship'])->label(false) ?>
                                <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3 text-primary">
                                    <h6 class="mb-0">Accepting Internship</h6>
                                </div>
                                <div class="col-sm-9 ">
                                    <?php if ($update === null) : ?>
                                        <?= ($modelOrganizationDetail->accepting_internship) ? 'Yes' : 'No'; ?>
                                    <?php else : ?>
                                        <?= $form->field($modelOrganizationDetail, 'accepting_internship')->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                'onText' => 'Yes',
                                                'offText' => 'No',
                                                'onColor' => 'success',
                                                'offColor' => 'danger',
                                            ]
                                        ])->label(false); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($update !== null) : ?>
        <?php ActiveForm::end(); ?>
    <?php endif; ?>

    <?php
        $items = [
            [
                'label' => 'BITI',
                'content' => $this->render('job-scope\artificial-intelligence'),
                'active' => true,
            ],
            [
                'label' => 'BITC',
                'content' => $this->render('job-scope\computer-networking'),
            ],
            [
                'label' => 'BITZ',
                'content' => $this->render('job-scope\computer-security'),
            ],
            [
                'label' => 'BITD',
                'content' => $this->render('job-scope\database-management'),
            ],
            [
                'label' => 'BITE',
                'content' => $this->render('job-scope\game-technology'),
            ],
            [
                'label' => 'BITM',
                'content' => $this->render('job-scope\interactive-media'),
            ],
            [
                'label' => 'BITS',
                'content' => $this->render('job-scope\software-development'),
            ],
            [
                'label' => 'DIT',
                'content' => $this->render('job-scope\information-technology-and-communication'),
            ],
        ];
    ?>

    <div class="modal fade" id="job-description-scope-information" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Job Descrition Scope Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= TabsX::widget([
                        'items' => $items,
                        'position' => TabsX::POS_ABOVE,
                        'encodeLabels' => false,
                    ]); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
