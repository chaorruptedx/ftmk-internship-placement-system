<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ResumeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update Organization Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'View Organization Profile'), 'url' => ['view']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_profile', [
        'update' => 'update',
        'modelUser' => $modelUser,
        'modelOrganizationDetail' => $modelOrganizationDetail,
        'modelAttachment' => $modelAttachment,
        'modelJobScopeRequirement' => $modelJobScopeRequirement,
    ]) ?>

</div>
