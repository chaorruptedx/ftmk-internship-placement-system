<?php
use yii\helpers\Html;
use kartik\dialog\Dialog;
use common\models\User;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ResumeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'View Organization Profile');
if (User::isUserStudent(Yii::$app->user->identity->username))
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Organization List'), 'url' => ['/internship-application/organization-list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (User::isUserOrganization(Yii::$app->user->identity->username)) : ?>
    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-pencil-alt">&nbsp;</span>Update Profile'), ['update'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php elseif (User::isUserStudent(Yii::$app->user->identity->username)) : ?>
    <p>
        <?= Dialog::widget([
            'options' => [
                'type' => Dialog::TYPE_SUCCESS,
                'title' => 'Apply Internship',
                'btnOKClass' => 'btn-success',
            ],
        ]); ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/internship-application/organization-list'], ['class' => 'btn btn-secondary']) ?>
    </p>
    <?php endif; ?>

    <?= $this->render('_profile', [
        'modelUser' => $modelUser,
        'modelOrganizationDetail' => $modelOrganizationDetail,
        'modelAttachment' => $modelAttachment,
        'application_exist' => $application_exist,
    ]) ?>

</div>
