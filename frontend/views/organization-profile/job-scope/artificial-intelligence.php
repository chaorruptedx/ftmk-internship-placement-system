<h1>Bachelor of Computer Science (Artificial Intelligence)</h1>

<p>
    The Bachelor of Computer Science (Artificial Intelligence) academic program is offered to provide graduates with a comprehensive understanding and excellence in Computer Science, particularly in Information & Communication Technology. Graduates will also be equipped with prior knowledge of engineering knowledge and skills in Artificial Intelligence to meet industry needs especially in the areas of ICT, robotics and manufacturing.
</p>


<h2>Career Prospects</h2>

<p>
    Extensive career opportunities in computer science and information technology are open to graduates who specialize in artificial intelligence. Graduates who specialize in artificial intelligence can also pursue their post-graduate studies. Among the career opportunities are knowledge engineers, smart systems or expert system developers, system analysts, system programmers, system designers, software developers, software consultants, computer scientists, and researchers.
</p>


<h2>Learning Outcomes</h2>

<p>
    The goal of FTMK to run a Bachelor of Computer Science (Artificial Intelligence) program is to produce students with the following characteristics:
</p>

<ul>
  <li>Able to acquire and apply knowledge in computer science and information technology.</li>
  <li>Able to analyze, design and develop ICT applications.</li>
  <li>Can use artificial intelligence techniques such as search techniques, fuzzy logic, neural networks, evolutionary computing, machine learning, and intelligent agents when developing a system.</li>
  <li>Equipped with skills to develop systems individually or in groups based on artificial intelligence such as smart systems, expert systems, intelligent agent systems and robot systems.</li>
  <li>Able to conduct research in related fields and based on artificial intelligence.</li>
  <li>Able to think creatively and critically in problem solving and communicate effectively to convey ideas.</li>
  <li>Able to contribute skills individually or as a group to different disciplines and domains.</li>
  <li>Able to demonstrate good personal, ethical, leadership and entrepreneurial skills.</li>
  <li>Able to carry out his own learning continuously to gain knowledge and skills.</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/BITI.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">