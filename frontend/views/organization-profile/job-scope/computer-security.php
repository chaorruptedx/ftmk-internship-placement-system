<h1>Bachelor of Computer Science (Computer Security)</h1>

<p>
    A bachelor’s degree course in Computer Science, B. CS. (Computer Security) (Honours) is aimed to produce highly knowledgeable and skillful graduates in the field of security related to computer science and information technology. Graduates are competent in advanced specialized knowledge and skill to analyze, design, install, configure, implement, administer, maintain and monitor the security infrastructure.
</p>


<h2>Career Prospects</h2>

<p>
    The graduates can be employed in the government and private sectors as well as undertaking business ventures of their own. The positions suitable for the graduates including Information Technology Executive, System Analyst, Network Security Manager/Administrator, System Security Manager/Administrator, IT Project Manager, Network Security Engineer, Network Security Executive, System/Network Security Consultant, Researcher.
</p>


<h2>Learning Outcomes</h2>

<p>
    The aim of FTMK’s Bachelor of Computer Science (Computer Security) degree program is to produce students
    with the following characteristics:
</p>

<ul>
    <li>Able to apply knowledge of computer science and information technology.</li>
    <li>Able to analyze, design and develop ICT applications.</li>
    <li>Able to analyze, create, assemble, configure, implement, manage, maintain and administer network infrastructure and security.</li>
    <li>Able to analyze and design the physical and cybersecurity policy.</li>
    <li>Able to obtain recognition from professional bodies.</li>
    <li>Able to resolve problems in creative way and able to communicate effectively.</li>
    <li>Able to contribute individually or in a team in various disciplines and domains.</li>
    <li>Able to lead with ethics and have Entrepreneurship skills.</li>
    <li>Able to perform continuous self-learning to obtain knowledge and skills.</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/BITZ.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">