<h1>Diploma in Information and Communication Technology</h1>

<p>
    The Diploma in Information and Communication Technology programme deals with designing innovative methodologies and sophisticated tools for developing software systems. Students are exposed to various techniques of analyzing user requirements and specifications, as well as design and implementation of software systems. Some of the core courses include object-oriented programming, database systems, software engineering and introduction to multimedia.
</p>


<h2>Career Prospects</h2>

<p>
    Graduates of the program will be able to work as Programmers, Analyst Programmers, Multimedia Programmers, Network Administrators, Software Developers and any IT related positions. They may also pursue further education at degree level at UTeM.
</p>


<h2>Learning Outcomes</h2>

<p>
    Diploma in IT program at FTMK intended to produce graduates with the following characteristic:
</p>

<ul>
    <li>Graduates should be able to understand fundamental principles of Computer Science and Information Technology.</li>
    <li>Graduates should be able analyze , design and develop database ICT system.</li>
    <li>Graduates should be able configure hardware , maintain and admin computer operation systems and network.</li>
    <li>Graduates should be able apply multimedia authoring tools for multimedia system development and simple multimedia presentation.</li>
    <li>Graduates should be able exhibit critical and creative thinking in resolving problems and able communicate effectively .across audience.</li>
    <li>Graduates should be able to contribute individually or in a team in various discipline and domain.</li>
    <li>Graduates should have good personalities and ethics with leadership and entrepreneurship skills.</li>
    <li>Graduates should be able continue learning independently in the acquisition of new knowledge and skill.</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/DIT.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">