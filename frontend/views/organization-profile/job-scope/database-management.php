<h1>Bachelor of Computer Science (Database Management)</h1>

<p>
    The learning objectives of this course are to produce knowledge and highly skilled graduates in the field of information and communication technology. Graduates pursuing the program are equipped with the in depth knowledge and specialized skills in database management area. This includes the ability to analyze, design, develop program using structured programming methods, manage and maintain database system which could meet the industrial needs in the field. Students should be able to develop data mining application with required security standard to protect the system database.
</p>


<h2>Career Prospects</h2>

<p>
    Graduates specialized in Database Management have the opportunity to work as Database Analyst, Database System Administrator and Database Designer. They also could work as System Programmer, Information System Officer and System Analyst. The graduates also have the opportunity to further up their studies in Master and Doctorate level.
</p>


<h2>Learning Outcomes</h2>

<p>
    Bachelor of Computer Science (Database Management) programme at FTMK intended to produce graduates with the following characteristics:
</p>

<ul>
    <li>Able to apply knowledge of computer science and information technology.</li>
    <li>Able to analyze, design and develop ICT applications.</li>
    <li>Able to develop database by applying database concept using latest technology.</li>
    <li>Able to develop database application with standard security measures.</li>
    <li>Able to administer and maintain database according to the standard procedure and policy.</li>
    <li>Able to resolve problems in creative way and able to communicate effectively.</li>
    <li>Able to contribute individually or in a team in various discipline and domains.</li>
    <li>Able to lead with ethics and have Entrepreneurship skills.</li>
    <li>Able to perform continuous self learning to obtain knowledge and skills.</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/BITD.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">