<h1>Bachelor of Computer Science (Interactive Media)</h1>

<p>
    Bachelor of Computer Science (Interactive Media) academic programme is offered to prepare graduates with a thorough understanding and superior skills in information technology particularly in the area of multimedia. The learning outcomes of this programme are to equip the students with the basic knowledge in every aspect of information technology, to provide the students with sufficient theoretical knowledge and skills to apply the knowledge learnt through the practiced concept , enable the students to be able to apply the interactivity concept in the design and development of multimedia-based application, equip the students with deep understanding and high skills in the development and management of web sites, animation, computer graphics, virtual reality and development of computer games, produce graduates that are capable to develop high quality interactive media products and multimedia applications which fulfill the industry specifications.
</p>


<h2>Career Prospects</h2>

<p>
    Our aim is to give produce highly knowledgeable and skillful graduates in the field of multimedia. They will have the opportunities to start careers such as Web designer or developer, computer games designer, computer graphics designer, animator, digital audio video engineer, user interface designer, interactive media application developer and multimedia consultant. On the other hand, the graduates may also choose career based on their basic knowledge in Computer Science and ICT such as programmer and information system officer or system analyst.
</p>


<h2>Learning Outcomes</h2>

<p>
    The purpose of FTMK offering the Bachelor of Computer Science (Interactive Media) is to produce students with the following qualities:
</p>

<ul>
    <li>Able to apply knowledge of computer science and information technology</li>
    <li>Able to analyze, design and develop ICT applications</li>
    <li>Able to apply interactivity concept in designing and developing multimedia-based applications and products</li>
    <li>Able to analyze requirements, configure, implement and maintain digital audio/video equipments</li>
    <li>Able to develop multimedia application with the quality that fulfills industry specifications</li>
    <li>Able to resolve problems in creative way and able to communicate effectively</li>
    <li>Able to contribute individually or in a team in various disciplines and domains</li>
    <li>Able to lead with ethics and have Entrepreneurshipship skills</li>
    <li>Able to perform continuous self learning to obtain knowledge and skills</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/BITM.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">