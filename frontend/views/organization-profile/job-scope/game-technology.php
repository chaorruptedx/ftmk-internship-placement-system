<h1>Bachelor of Information Technology (Game Technology)</h1>

<p>
    This course is offered to produce graduates who are highly knowledgeable and skilled in the field of computer games technology. The graduates are well equipped with knowledge and specific skills such as computer game programming, design and develop various types of computer games, the principle of games, web-based games, project management as well as 2D and 3D game development. Graduates of this course are able to contribute their expertise and skills to the education and entertainment industri such as game-based education and game content development.
</p>


<h2>Career Prospects</h2>

<p>
    A wide range of career opportunities in the field of computer science and information technology is open to graduates who specialized in computer games. Among the career opportunities such as game designer, game developer, game programmer, game animator, game audio engineer, game tester, technical support, and specialist computer game researcher.
</p>


<h2>Learning Outcomes</h2>

<p>
    The aim of FTMK to conduct the Bachelor of Information Technology (Game Technology) program is to produce students with the following characteristics:
</p>

<ul>
    <li>Able to obtain and apply knowledge in computer science and information technology.</li>
    <li>Able to analyse, design and develop game applications.</li>
    <li>Able to apply artificial intelligent techniques such as searching technique, fuzzy logic, neural network, evolutionary computing, machine </li>learning, and intelligent agent when developing a system. Equipped with skills to develop game.
    <li>Able to conduct research in the fields related and based on artificial intelligence.</li>
    <li>Able to think creatively and critically in problem solving and able to communicate effectively to deliver ideas.</li>
    <li>Able to contribute skills individually or in group in difference disciplines and domains.</li>
    <li>Able to present good personality, ethics, leadership and entrepreneurship skills.</li>
    <li>Able to perform continuous self learning to obtain knowledge and skills.</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/BITE.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">