<h1>Bachelor of Computer Science (Software Development)</h1>

<p>
    The Bachelor in Computer Science (Software Development) degree course is offered in order to produce knowledgeable and highly skilled graduates in the field of information technology and communication. Graduates pursuing the program are equipped with the necessary knowledge and specialized skills in engineering and software development which could meet the industrial needs in the field. This includes the ability to analyze, synthesize, design complex systems, maintain, test, control software quality and manage software projects.
</p>


<h2>Career Prospects</h2>

<p>
    Graduates specialized in Software Engineering have the opportunity to work either in the Government or private sector. They could work as Information System Officer, System Analyst, Software Engineer, Software development Manager, Team member Software Quality Assurance, System Analyst, System Administrator, Software Tester or Software Development Consultant. Graduates have the opportunity too to further up their studies in Master and Doctorate level.
</p>


<h2>Learning Outcomes</h2>

<p>
    Bachelor of Computer Science (Software Development) programme at FTMK intended to produce graduates with the following characteristic:
</p>

<ul>
    <li>Able to apply knowledge of computer science and information technology.</li>
    <li>Able to analyze, design and develop ICT applications.</li>
    <li>Able to perform system coding using relevant programming language according to industry need.</li>
    <li>Able to manage software development project by applying software engineering concepts.</li>
    <li>Able to perform research in software engineering field.</li>
    <li>Able to resolve problems in creative way and able to communicate effectively.</li>
    <li>Able to contribute individually or in a team in various disciplines and domains.</li>
    <li>Able to lead with ethics and have Entrepreneurshipship skills.</li>
    <li>Able to perform continuous self learning to obtain knowledge and skills.</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/BITS.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">