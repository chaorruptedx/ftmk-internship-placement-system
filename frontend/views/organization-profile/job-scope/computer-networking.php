<h1>Bachelor of Computer Science (Computer Networking)</h1>

<p>
    Bachelor’s degree courses in Computer Science, B.C. (Computer Networks) (Honors) aims to produce knowledgeable and skilled graduates in information and communication technology. Graduates are competent in specialized knowledge and specialized skills in analyzing, developing, installing, administering, servicing, and controlling network and communication systems.
</p>


<h2>Career Prospects</h2>

<p>
    Graduates can work in the government and private sectors as well as run their own businesses. Suitable positions for graduates include Information Systems Executive, System Analyst, Computer Security Executive, Network Project Administrator, Network Programmer and Network Engineer. Graduates also have the opportunity to pursue graduate and PhD studies.
</p>


<h2>Learning Outcomes</h2>

<p>
    The goal of the FTMK undergraduate program is to produce students with the following characteristics:
</p>

<ul>
  <li>Able to apply computer science and information technology knowledge.</li>
  <li>Able to analyze, design and develop ICT applications.</li>
  <li>Able to analyze, create, install, organize, execute, manage, maintain and administer network infrastructure and security.</li>
  <li>Able to develop advanced computer network applications.</li>
  <li>Can get recognition from a professional body.</li>
  <li>Able to solve problems creatively and communicate effectively.</li>
  <li>Can contribute individually or in teams across multiple disciplines and domains.</li>
  <li>Can lead ethically and have Entrepreneurial skills.</li>
  <li>Able to practice self-directed learning to gain knowledge and skills.</li>
</ul>

<img src="http://ftmk.utem.edu.my/web/wp-content/uploads/2020/10/BITC.jpg" alt="BITI" style="display: block; margin-left: auto; margin-right: auto;">