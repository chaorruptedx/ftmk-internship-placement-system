<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Button;
use kartik\dialog\Dialog;
use common\models\User;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ResumeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'View Student Resume');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (User::isUserStudent(Yii::$app->user->identity->username)) : ?>
    <p class="float-left">
        <?= Html::a(Yii::t('app', '<span class="fas fa-pencil-alt">&nbsp;</span>Update Resume'), ['update'], ['class' => 'btn btn-success']) ?>
        <?= Button::widget([
                'label' => '<span class="fas fa-print">&nbsp;</span>Print Resume',
                'encodeLabel' => false,
                'options' => [
                    'class' => 'btn btn-info print-this',
                ],
        ]); ?>
    </p>
    <p class="float-right">
        <b>Last Resume Updated :</b> <?= isset($modelAttachment->id) ? date("l, j F Y, g:i A", $modelPersonalDetail->updated_at) : "None"; ?>
    </p>
    <?php elseif (User::isUserOrganization(Yii::$app->user->identity->username)) : ?>
        <?= Dialog::widget([
            'libName' => 'krajeeDialogAccept',
            'options' => [
                'type' => Dialog::TYPE_SUCCESS,
                'title' => 'Application Approval',
                'btnOKClass' => 'btn-success',
            ],
        ]); ?>
        <?= Dialog::widget([
            'libName' => 'krajeeDialogReject',
            'options' => [
                'type' => Dialog::TYPE_DANGER,
                'title' => 'Application Rejection',
                'btnOKClass' => 'btn-danger',
            ],
        ]); ?>

        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/student-applicant/index'], ['class' => 'btn btn-secondary']) ?>

        <?php if ($modelInternshipApplication !== null && in_array($modelInternshipApplication->application_status, [1, 2])) : ?>
            <?= Button::widget([
                'label' => '<span class="fas fa-check">&nbsp;</span>Approve',
                'encodeLabel' => false,
                'options' => [
                    'id' => 'accept-application',
                    'class' => 'btn btn-success',
                ],
            ]); ?>
        <?php elseif ($modelInternshipApplication !== null && in_array($modelInternshipApplication->application_status, [3, 4])) : ?>
            <?= Button::widget([
                'label' => '<span class="fas fa-check">&nbsp;</span>Approved',
                'encodeLabel' => false,
                'options' => [
                    'disabled' => true,
                    'class' => 'btn btn-success',
                ],
            ]); ?>
        <?php endif; ?>

        <?php if ($modelInternshipApplication !== null && in_array($modelInternshipApplication->application_status, [1, 2])) : ?>
            <?= Button::widget([
                'label' => '<span class="fas fa-times">&nbsp;</span>Reject',
                'encodeLabel' => false,
                'options' => [
                    'id' => 'reject-application',
                    'class' => 'btn btn-danger',
                ],
            ]); ?>
        <?php elseif ($modelInternshipApplication !== null && in_array($modelInternshipApplication->application_status, [0])) : ?>
            <?= Button::widget([
                'label' => '<span class="fas fa-times">&nbsp;</span>Rejected',
                'encodeLabel' => false,
                'options' => [
                    'disabled' => true,
                    'class' => 'btn btn-danger',
                ],
            ]); ?>
        <?php endif; ?>

        <?= Button::widget([
                'label' => '<span class="fas fa-print">&nbsp;</span>Print Resume',
                'encodeLabel' => false,
                'options' => [
                    'class' => 'btn btn-info print-this',
                ],
        ]); ?>
    <?php endif; ?>
    
    <?php if (User::isUserOrganization(Yii::$app->user->identity->username)) : ?>

    <br><br>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><b>Internship Duration :</b></h5>
            <p class="card-text"><?= date("j F Y", strtotime($modelInternshipApplication->academicSession->start_internship_date)) ?> until <?= date("j F Y", strtotime($modelInternshipApplication->academicSession->end_internship_date)) ?></p>
        </div>
    </div>

    <?php endif; ?>

    <?= $this->render('_resume', [
        'modelPersonalDetail' => $modelPersonalDetail,
        'modelAttachment' => $modelAttachment,
        'modelSkills' => $modelSkills,
        'modelLanguages' => $modelLanguages,
        'modelEducations' => $modelEducations,
        'modelExperiences' => $modelExperiences,
        'modelAchievements' => $modelAchievements,
    ]) ?>

</div>

<?php if (User::isUserOrganization(Yii::$app->user->identity->username)) : ?>
<?php

$url_accept_internship = Url::toRoute([
    '/student-applicant/accept-reject-internship-application',
    'id_personal_detail' => $modelPersonalDetail->id,
    'application_status' => 3,
]);

$url_reject_internship = Url::toRoute([
    '/student-applicant/accept-reject-internship-application',
    'id_personal_detail' => $modelPersonalDetail->id,
    'application_status' => 0,
]);

$js = <<< JS

var student_name = "$modelPersonalDetail->name";

$("button#accept-application").on("click", function() {
    krajeeDialogAccept.confirm("Are you sure you want to approve student <b>" + student_name + "</b> internship application?", function (result) {
        if (result) {
            return window.location.replace("$url_accept_internship");
        }
    });
});

$("button#reject-application").on("click", function() {
    krajeeDialogReject.confirm("Are you sure you want to reject student <b>" + student_name + "</b> internship application?", function (result) {
        if (result) {
            return window.location.replace("$url_reject_internship");
        }
    });
});

JS;
$this->registerJs($js);
?>
<?php endif; ?>