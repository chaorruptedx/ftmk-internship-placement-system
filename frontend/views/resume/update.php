<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ResumeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update Student Resume');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'View Student Resume'), 'url' => ['view']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_resume', [
        'update' => 'update',
        'modelPersonalDetail' => $modelPersonalDetail,
        'modelAttachment' => $modelAttachment,
        'modelAttachment' => $modelAttachment,
        'modelSkills' => $modelSkills,
        'modelLanguages' => $modelLanguages,
        'modelEducations' => $modelEducations,
        'modelExperiences' => $modelExperiences,
        'modelAchievements' => $modelAchievements,
    ]) ?>

</div>
