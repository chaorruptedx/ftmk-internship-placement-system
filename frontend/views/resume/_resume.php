<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\touchspin\TouchSpin;
use kartik\date\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use common\components\Helper;
use common\models\BankStatus;
use frontend\assets\ResumeAsset;

ResumeAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel common\models\ResumeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php if ($update !== null) : ?>
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['view'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
    </p>
<?php endif; ?>

<!-- Page Container -->
<div id="resume-print" class="w3-content w3-margin-top" style="max-width:1400px;">

<!-- The Grid -->
<div class="w3-row-padding">

    <!-- Left Column -->
    <div class="w3-third">
    
    <div class="w3-white w3-text-grey w3-card-4">
        <div class="w3-display-container">
            <?php if ($update === null) : ?>
                <?php if ($modelAttachment->path !== null) : ?>
                    <?= Html::img('@web/'.$modelAttachment->path, ['style' => 'width: 100%']); ?>
                <?php else : ?>
                    <?= Html::img('@web/image/default_user_profile_picture.png', ['style' => 'width: 100%']); ?>
                <?php endif; ?>
            <?php else : ?>
                <?= $form->field($modelAttachment, 'imageFile')->widget(FileInput::classname(), [
                    'options' => [
                        'accept' => 'image/*',
                    ],
                    'pluginOptions' => [
                        'initialPreview'=>[
                            ($modelAttachment->path !== null) ? Url::to('@web/'.$modelAttachment->path) : null,
                        ],
                        'initialPreviewAsData' => true,
                        'initialCaption' => $modelAttachment->name,
                        'initialPreviewConfig' => [
                            ['caption' => $modelAttachment->name.'.'.$modelAttachment->type],
                        ],
                        'overwriteInitial' => true,
                    ]
                ])->label(false); ?>
            <?php endif; ?>
            <div class="w3-container w3-text-black">
                <h2><?= $modelPersonalDetail->name ?></h2>
            </div>
        </div>
        <div class="w3-container">
            <p><i class="fa fa-id-card fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= $modelPersonalDetail->nric_no ?></p>
            <p><i class="fa fa-id-badge fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= $modelPersonalDetail->user_no ?></p>
            <p><i class="fa fa-venus-mars fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= Helper::getGender($modelPersonalDetail->gender_no) ?></p>
            <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= $modelPersonalDetail->user->email ?></p>
            <p>
                <?php if ($update === null) : ?>
                    <i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= $modelPersonalDetail->tel_no ?>
                <?php else : ?>
                    <?= $form->field($modelPersonalDetail, 'tel_no', [
                        'addon' => [
                            'prepend' => [
                                'content' => '<i class="fa fa-phone fa-fw w3-large w3-text-indigo"></i>'
                            ]
                        ]
                    ])->textInput(['placeholder' => 'Tel. No.'])->label(false) ?>
                <?php endif; ?>
            </p>
            <p><i class="fa fa-building fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= $modelPersonalDetail->programme->department->faculty->name ?></p>
            <p><i class="fa fa-graduation-cap fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= $modelPersonalDetail->programme->name ?></p>
            <p>
                <?php if ($update === null) : ?>
                    <i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-indigo"></i><?= $modelPersonalDetail->address ?>
                <?php else : ?>
                    <?= $form->field($modelPersonalDetail, 'address', [
                        'addon' => [
                            'prepend' => [
                                'content' => '<i class="fa fa-home fa-fw w3-large w3-text-indigo"></i>'
                            ]
                        ]
                    ])->textArea(['placeholder' => 'Address'])->label(false) ?>
                <?php endif; ?>
            </p>
            <hr>

            <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-indigo"></i>Skills</b></p>
            
            <?php if ($update === null) : ?>
                <?php if (!empty($modelSkills) && is_array($modelSkills)) : ?>
                    <?php foreach ($modelSkills as $modelSkill) : ?>
                        <p><?= $modelSkill->name ?></p>
                        <div class="w3-light-grey w3-round-xlarge w3-small">
                            <div class="w3-container w3-center w3-round-xlarge w3-indigo" style="width:<?= $modelSkill->scale ?>%"><?= $modelSkill->scale ?>%</div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else : ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_skills', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-skills', // required: css class selector
                            'widgetItem' => '.skill', // required: css class
                            'limit' => 5, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-skill', // css class
                            'deleteButton' => '.remove-skill', // css class
                            'model' => $modelSkills[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'name',
                                'scale',
                            ],
                        ]); ?>

                        <div class="container-skills"><!-- widgetContainer -->
                        <button type="button" class="add-skill btn btn-success btn-xs"><i class="fas fa-plus"></i></button>
                        <?php foreach ($modelSkills as $i => $modelSkill): ?>
                            <div class="skill panel panel-default"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <div class="float-right">
                                        <button type="button" class="remove-skill btn btn-danger btn-xs"><i class="fas fa-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (!$modelSkill->isNewRecord) {
                                            echo Html::activeHiddenInput($modelSkill, "[{$i}]id");
                                        }
                                    ?>
                                    <?= $form->field($modelSkill, "[{$i}]name")->textInput(['placeholder' => "Skill Name"])->label(false) ?>
                                    <?= $form->field($modelSkill, "[{$i}]scale")->widget(TouchSpin::classname(), [
                                        'options' => ['placeholder' => 'Rate (0 - 100)...'],
                                        'pluginOptions' => [
                                            'min' => 0,
                                            'max' => 100,
                                            'postfix' => '%',
                                        ],
                                    ])->label(false); ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                        <?php DynamicFormWidget::end(); ?>
                    </div>
                </div>
            <?php endif; ?>

            <br>

            <p class="w3-large w3-text-theme"><b><i class="fa fa-globe fa-fw w3-margin-right w3-text-indigo"></i>Languages</b></p>
            
            <?php if ($update === null) : ?>
                <?php if (!empty($modelLanguages) && is_array($modelLanguages)) : ?>
                    <?php foreach ($modelLanguages as $modelLanguage) : ?>
                        <p><?= $modelLanguage->language->name ?></p>
                        <div class="w3-light-grey w3-round-xlarge">
                            <div class="w3-round-xlarge w3-indigo" style="height:24px;width:<?= $modelLanguage->scale ?>%"></div>
                        </div>
                        <br>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else : ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_languages', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-languages', // required: css class selector
                            'widgetItem' => '.language', // required: css class
                            'limit' => 3, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-language', // css class
                            'deleteButton' => '.remove-language', // css class
                            'model' => $modelLanguages[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'id_language',
                                'scale',
                            ],
                        ]); ?>

                        <div class="container-languages"><!-- widgetContainer -->
                        <button type="button" class="add-language btn btn-success btn-xs"><i class="fas fa-plus"></i></button>
                        <?php foreach ($modelLanguages as $j => $modelLanguage): ?>
                            <div class="language panel panel-default"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <div class="float-right">
                                        <button type="button" class="remove-language btn btn-danger btn-xs"><i class="fas fa-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (!$modelLanguage->isNewRecord) {
                                            echo Html::activeHiddenInput($modelLanguage, "[{$j}]id");
                                        }
                                    ?>
                                    <?= $form->field($modelLanguage, "[{$j}]id_language")->widget(Select2::classname(), [
                                        'data' => BankStatus::listLanguage(),
                                        'options' => ['placeholder' => 'Select language ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label(false); ?>
                                    <?= $form->field($modelLanguage, "[{$j}]scale")->widget(TouchSpin::classname(), [
                                        'options' => ['placeholder' => 'Rate (0 - 100)...'],
                                        'pluginOptions' => [
                                            'min' => 0,
                                            'max' => 100,
                                            'postfix' => '%',
                                        ],
                                    ])->label(false); ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                        <?php DynamicFormWidget::end(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div><br>

    <!-- End Left Column -->
    </div>

    <!-- Right Column -->
    <div class="w3-twothird">

    <div class="w3-container w3-card w3-white w3-margin-bottom">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-indigo"></i>Education</h2>
        <?php if ($update === null) : ?>
            <?php if (!empty($modelEducations) && is_array($modelEducations)) : ?>
                <?php foreach ($modelEducations as $modelEducation) : ?>
                    <div class="w3-container">
                        <h5 class="w3-opacity"><b><?= $modelEducation->name ?></b></h5>
                        <h6 class="w3-text-indigo"><i class="fa fa-calendar fa-fw w3-margin-right"></i><?= $modelEducation->start_year ?> - <?= $modelEducation->end_year ?></h6>
                        <p><?= $modelEducation->description ?></p>
                        <hr>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php else : ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper_educations', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-educations', // required: css class selector
                        'widgetItem' => '.education', // required: css class
                        'limit' => 4, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-education', // css class
                        'deleteButton' => '.remove-education', // css class
                        'model' => $modelEducations[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'name',
                        ],
                    ]); ?>

                    <div class="container-educations"><!-- widgetContainer -->
                    <button type="button" class="add-education btn btn-success btn-xs"><i class="fas fa-plus"></i></button>
                    <?php foreach ($modelEducations as $k => $modelEducation): ?>
                        <div class="education panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <div class="float-right">
                                    <button type="button" class="remove-education btn btn-danger btn-xs"><i class="fas fa-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (!$modelEducation->isNewRecord) {
                                        echo Html::activeHiddenInput($modelEducation, "[{$k}]id");
                                    }
                                ?>
                                <?= $form->field($modelEducation, "[{$k}]name")->textInput(['placeholder' => "Education Name"])->label(false) ?>
                                <?php
                                    $education_datepicker_layout = <<< HTML
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                                    {input1}
                                    {separator}
                                    {input2}
                                    HTML;
                                ?>
                                <?= DatePicker::widget([
                                    'model' => $modelEducation,
                                    'attribute' => "[{$k}]start_year",
                                    'attribute2' => "[{$k}]end_year",
                                    'options' => ['placeholder' => 'Start Year'],
                                    'options2' => ['placeholder' => 'End Year'],
                                    'type' => DatePicker::TYPE_RANGE,
                                    'form' => $form,
                                    'layout' => $education_datepicker_layout,
                                    'pluginOptions' => [
                                        'format' => 'yyyy',
                                        'autoclose' => true,
                                        'minViewMode' => 2,
                                    ]
                                ]); ?>
                                <br>
                                <?= $form->field($modelEducation, "[{$k}]description")->textArea(['placeholder' => "Education Description"])->label(false) ?>
                                <hr>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    
    <div class="w3-container w3-card w3-white w3-margin-bottom">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-indigo"></i>Work Experience</h2>
        <?php if ($update === null) : ?>
            <?php if (!empty($modelExperiences) && is_array($modelExperiences)) : ?>
                <?php foreach ($modelExperiences as $modelExperience) : ?>
                    <div class="w3-container">
                        <h5 class="w3-opacity"><b><?= $modelExperience->name ?></b></h5>
                        <h6 class="w3-text-indigo"><i class="fa fa-calendar fa-fw w3-margin-right"></i><?= $modelExperience->start_year ?> - <?= $modelExperience->end_year ?></h6>
                        <p><?= $modelExperience->description ?></p>
                        <hr>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php else : ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper_experiences', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-experiences', // required: css class selector
                        'widgetItem' => '.experience', // required: css class
                        'limit' => 5, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-experience', // css class
                        'deleteButton' => '.remove-experience', // css class
                        'model' => $modelExperiences[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'name',
                        ],
                    ]); ?>

                    <div class="container-experiences"><!-- widgetContainer -->
                    <button type="button" class="add-experience btn btn-success btn-xs"><i class="fas fa-plus"></i></button>
                    <?php foreach ($modelExperiences as $l => $modelExperience): ?>
                        <div class="experience panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <div class="float-right">
                                    <button type="button" class="remove-experience btn btn-danger btn-xs"><i class="fas fa-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (!$modelExperience->isNewRecord) {
                                        echo Html::activeHiddenInput($modelExperience, "[{$l}]id");
                                    }
                                ?>
                                <?= $form->field($modelExperience, "[{$l}]name")->textInput(['placeholder' => "Experience Name"])->label(false) ?>
                                <?php
                                    $experience_datepicker_layout = <<< HTML
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                                    {input1}
                                    {separator}
                                    {input2}
                                    HTML;
                                ?>
                                <?= DatePicker::widget([
                                    'model' => $modelExperience,
                                    'attribute' => "[{$l}]start_year",
                                    'attribute2' => "[{$l}]end_year",
                                    'options' => ['placeholder' => 'Start Year'],
                                    'options2' => ['placeholder' => 'End Year'],
                                    'type' => DatePicker::TYPE_RANGE,
                                    'form' => $form,
                                    'layout' => $experience_datepicker_layout,
                                    'pluginOptions' => [
                                        'format' => 'yyyy',
                                        'autoclose' => true,
                                        'minViewMode' => 2,
                                    ]
                                ]); ?>
                                <br>
                                <?= $form->field($modelExperience, "[{$l}]description")->textArea(['placeholder' => "Experience Description"])->label(false) ?>
                                <hr>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="w3-container w3-card w3-white w3-margin-bottom">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-trophy fa-fw w3-margin-right w3-xxlarge w3-text-indigo"></i>Achievement</h2>
        <?php if ($update === null) : ?>
            <?php if (!empty($modelAchievements) && is_array($modelAchievements)) : ?>
                <?php foreach ($modelAchievements as $modelAchievement) : ?>
                    <div class="w3-container">
                        <h5 class="w3-opacity"><b><?= $modelAchievement->name ?></b></h5>
                        <h6 class="w3-text-indigo"><i class="fa fa-calendar fa-fw w3-margin-right"></i><?= $modelAchievement->year ?></h6>
                        <p><?= $modelAchievement->description ?></p>
                        <hr>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php else : ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper_achievements', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-achievements', // required: css class selector
                        'widgetItem' => '.achievement', // required: css class
                        'limit' => 5, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-achievement', // css class
                        'deleteButton' => '.remove-achievement', // css class
                        'model' => $modelAchievements[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'name',
                        ],
                    ]); ?>

                    <div class="container-achievements"><!-- widgetContainer -->
                    <button type="button" class="add-achievement btn btn-success btn-xs"><i class="fas fa-plus"></i></button>
                    <?php foreach ($modelAchievements as $m => $modelAchievement): ?>
                        <div class="achievement panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <div class="float-right">
                                    <button type="button" class="remove-achievement btn btn-danger btn-xs"><i class="fas fa-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (!$modelAchievement->isNewRecord) {
                                        echo Html::activeHiddenInput($modelAchievement, "[{$m}]id");
                                    }
                                ?>
                                <?= $form->field($modelAchievement, "[{$m}]name")->textInput(['placeholder' => "Achievement Name"])->label(false) ?>
                                <?= $form->field($modelAchievement, "[{$m}]year")->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Enter Year ...'],
                                    'pluginOptions' => [
                                        'format' => 'yyyy',
                                        'autoclose' => true,
                                        'minViewMode' => 2,
                                    ]
                                ]); ?>
                                <?= $form->field($modelAchievement, "[{$l}]description")->textArea(['placeholder' => "Achievement Description"])->label(false) ?>
                                <hr>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="w3-container w3-card w3-white w3-margin-bottom">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-asterisk fa-fw w3-margin-right w3-xxlarge w3-text-indigo"></i>Referees</h2>
        <div class="w3-container">
            <div class="row">
                <div class="col-sm-6">
                    <?php if ($update === null) : ?>
                        <h5 class="w3-opacity"><b><?= $modelPersonalDetail->refereeOne->name ?></b></h5>
                        <h6 class="w3-text-indigo"><i class="fa fa-envelope fa-fw w3-margin-right"></i><?= $modelPersonalDetail->refereeOne->user->email ?></h6>
                        <h6 class="w3-text-indigo"><i class="fa fa-phone-alt fa-fw w3-margin-right"></i><?= $modelPersonalDetail->refereeOne->tel_no ?></h6>
                    <?php else : ?>
                        <?= $form->field($modelPersonalDetail, 'id_referee_one')->widget(Select2::classname(), [
                            'data' => BankStatus::listLecturer(),
                            'options' => [
                                'placeholder' => 'Select first referee ...',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-6">
                    <?php if ($update === null) : ?>
                        <h5 class="w3-opacity"><b><?= $modelPersonalDetail->refereeTwo->name ?></b></h5>
                        <h6 class="w3-text-indigo"><i class="fa fa-envelope fa-fw w3-margin-right"></i><?= $modelPersonalDetail->refereeTwo->user->email ?></h6>
                        <h6 class="w3-text-indigo"><i class="fa fa-phone-alt fa-fw w3-margin-right"></i><?= $modelPersonalDetail->refereeTwo->tel_no ?></h6>
                    <?php else : ?>
                        <?= $form->field($modelPersonalDetail, 'id_referee_two')->widget(Select2::classname(), [
                            'data' => BankStatus::listLecturer(),
                            'options' => [
                                'placeholder' => 'Select second referee ...',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false); ?>
                    <?php endif; ?>
                </div>
            </div>
            <hr>
        </div>
    </div>

    <!-- End Right Column -->
    </div>
    
<!-- End Grid -->
</div>

<!-- End Page Container -->
</div>

<?php if ($update !== null) : ?>
    <?php ActiveForm::end(); ?>
<?php endif; ?>

<?php
$script1 = <<< JS

$(".dynamicform_wrapper_skills").on("limitReached", function(e, item) {
    alert("Maximum 5 Skills only.");
});

$(".dynamicform_wrapper_languages").on("limitReached", function(e, item) {
    alert("Maximum 3 Languages only.");
});

$(".dynamicform_wrapper_educations").on("limitReached", function(e, item) {
    alert("Maximum 4 Educations only.");
});

$(".dynamicform_wrapper_experiences").on("limitReached", function(e, item) {
    alert("Maximum 5 Experiences only.");
});

$(".dynamicform_wrapper_achievements").on("limitReached", function(e, item) {
    alert("Maximum 5 Achievements only.");
});

JS;
$this->registerJs($script1);


$script2 = <<< JS

function initSelect2DropStyle(a, b, c) {
    initS2Loading(a, b, c);
}
function initSelect2Loading(a, b) {
    initS2Loading(a, b);
}

JS;
$this->registerJs($script2, yii\web\View::POS_HEAD);


// $script3 = <<< JS

// $(".dynamicform_wrapper_educations").on("afterInsert", function(e, item) {
//     var datePickers = $(this).find('[data-krajee-kvdatepicker]');
//     datePickers.each(function(index, el) {
//         // $(this).parent().removeData().kvDatepicker('initDPRemove');
//         $(this).parent().kvDatepicker(eval($(this).attr('data-krajee-kvdatepicker')));
//     });
// });

// JS;
// $this->registerJs($script3, yii\web\View::POS_END);

$bg_url = Url::to('@web/image/background.jpg');
$script3 = <<< JS

$('.print-this').on("click", function() {

    // window.print();


    // $('#resume-print').printThis({
    //     idebug: false,               // show the iframe for debugging
    //     importCSS: true,            // import parent page css
    //     importStyle: true,         // import style tags
    //     printContainer: true,       // print outer container/$.selector
    //     loadCSS: "",                // path to additional css file - use an array [] for multiple
    //     pageTitle: "",              // add title to print page
    //     removeInline: false,        // remove inline styles from print elements
    //     removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
    //     printDelay: 333,            // variable print delay
    //     header: null,               // prefix to html
    //     footer: null,               // postfix to html
    //     base: false,                // preserve the BASE tag or accept a string for the URL
    //     formValues: true,           // preserve input/form values
    //     canvas: true,              // copy canvas content
    //     doctypeString: '...',       // enter a different doctype for older markup
    //     removeScripts: false,       // remove script tags from print content
    //     copyTagClasses: true,      // copy classes from the html & body tag
    //     beforePrintEvent: null,     // function for printEvent in iframe
    //     beforePrint: null,          // function called before iframe is filled
    //     afterPrint: null            // function called before iframe is removed
    // });


    var bg_url = '$bg_url';
    var restorepage = $('body').html();
    var printcontent = $('div#resume-print').clone();
    $("body").css('background-image', 'none');
    $("body").css('background-color', 'white');
    $('body').empty().html(printcontent);

    setTimeout(function(){
        window.print();
        $('body').html(restorepage);
        $("body").css('background-image', bg_url);
        $("body").css('background-color', 'whitesmoke');
    }, 500);
});

JS;
$this->registerJs($script3);
?>