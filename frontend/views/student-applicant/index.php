<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use common\components\Helper;
use common\models\BankStatus;
/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentApplicantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Student Applicant List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internship-application-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'personalDetail.name',
            [
                'attribute' => 'personalDetail.programme.name',
                'value' => function ($model) {
                    return $model->personalDetail->programme->code.' - '.$model->personalDetail->programme->name;
                },
                'filter' => BankStatus::listProgramme(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'attribute' => 'application_status',
                'value' => function ($model) {
                    return Helper::getApplicationStatusOrganization($model->application_status);
                },
                'contentOptions' => function ($model, $key, $index, $column) {

                    if ($model->application_status == 0)
                        $color = 'danger';
                    elseif ($model->application_status == 1)
                        $color = 'info';
                    elseif ($model->application_status == 2)
                        $color = 'warning';
                    elseif ($model->application_status == 3)
                        $color = 'primary';
                    elseif ($model->application_status == 4)
                        $color = 'success';

                    return [
                        'class' => 'text-' . $color
                    ];
                },
                'filter' => Helper::listApplicationStatusOrganization(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {

                        $url = Url::to(['/resume/view', 'id_student' => $model->personalDetail->user->id]);

                        return Html::a('<span class="fas fa-eye">&nbsp;</span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <div class="card">
        <div class="card-body">
            <h3>Status Flow Note :</h3>                 
            <p class="text-center"><span class="text-success">New</span> <span class="fas fa-angle-right"></span> <span class="text-warning">In Review</span> <span class="fas fa-angle-right"></span> <span class="text-info">Awaiting Student Reply</span> <span class="fas fa-angle-right"></span> <span class="text-primary">Student Accepted</span></p>
        </div>
    </div>
</div>
