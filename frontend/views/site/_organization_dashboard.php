<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\DashboardOrganizationAsset;

DashboardOrganizationAsset::register($this);
?>
<style>
    .cardcon {
    background-color: white;
    padding: 20px;
    margin-top: 20px;
    border-radius: 20px;
    }
</style>
<div class="organization-dashboard">

    <h1><?= Html::encode('Announcement') ?></h1>

    <div class="cardcon">
        <?php if (!empty($modelAnnouncements) && is_array($modelAnnouncements)) : ?>
            <ul>
                <?php foreach ($modelAnnouncements as $modelAnnouncement) : ?>
                    <h4><?= $modelAnnouncement->title; ?></h4>
                    <li><?= $modelAnnouncement->content; ?></li><hr>
                <?php endforeach ?>
            </ul>
        <?php else : ?>
            No announcement.
        <?php endif; ?>
    </div>

    <br>

    <h1><?= Html::encode('Dashboard') ?></h1>

    <div class="card">
        <div class="card-body">
        
            <div class="row">

                <div class="col-sm-12">
                    <h3 class="text-center">Student Internship Application</h3>
                    <br>
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading green"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content green">
                                    <div class="circle-tile-description text-faded">New</div>
                                    <div class="circle-tile-number text-faded "><?= $modelInternshipApplication->new; ?></div>
                                    <a class="circle-tile-footer" href="<?= Url::to(['/student-applicant']); ?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading orange"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content orange">
                                    <div class="circle-tile-description text-faded">In Review</div>
                                    <div class="circle-tile-number text-faded "><?= $modelInternshipApplication->in_review; ?></div>
                                    <a class="circle-tile-footer" href="<?= Url::to(['/student-applicant']); ?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading dark-blue"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content dark-blue">
                                    <div class="circle-tile-description text-faded">Awaiting Student Reply</div>
                                    <div class="circle-tile-number text-faded "><?= $modelInternshipApplication->awaiting_student_reply; ?></div>
                                    <a class="circle-tile-footer" href="<?= Url::to(['/student-applicant']); ?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading blue"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content blue">
                                    <div class="circle-tile-description text-faded">Student Accepted</div>
                                    <div class="circle-tile-number text-faded "><?= $modelInternshipApplication->student_accepted; ?></div>
                                    <a class="circle-tile-footer" href="<?= Url::to(['/student-applicant']); ?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

</div>
