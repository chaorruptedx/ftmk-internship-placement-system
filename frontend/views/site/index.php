<?php
use common\models\User;
/* @var $this yii\web\View */

$this->title = 'FTMK Internship Placement System';
?>
<div class="site-index">
    <div class="body-content">

        <?php if (User::isUserStudent(Yii::$app->user->identity->username)) : ?>
            <?= $this->render('_student_dashboard', [
                'modelAnnouncements' => $modelAnnouncements,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]); ?>
        <?php elseif (User::isUserOrganization(Yii::$app->user->identity->username)) : ?>
            <?= $this->render('_organization_dashboard', [
                'modelAnnouncements' => $modelAnnouncements,
                'modelInternshipApplication' => $modelInternshipApplication,
            ]); ?>
        <?php elseif (!Yii::$app->user->identity->username) : ?> <!-- Guest -->
            <h1 class="text-center">Welcome to FTMK Internship Placement System</h1>
        <?php endif ?>

    </div>
</div>
