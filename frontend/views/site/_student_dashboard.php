<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\dialog\Dialog;
use kartik\grid\GridView;
use common\components\Helper;
?>
<style>
    .cardcon {
    background-color: white;
    padding: 20px;
    margin-top: 20px;
    border-radius: 20px;
    }
</style>
<div class="student-dashboard">

    <h1><?= Html::encode('Announcement') ?></h1>

    <div class="cardcon">
        <?php if (!empty($modelAnnouncements) && is_array($modelAnnouncements)) : ?>
            <ul>
                <?php foreach ($modelAnnouncements as $modelAnnouncement) : ?>
                    <h4><?= $modelAnnouncement->title; ?></h4>
                    <li><?= $modelAnnouncement->content; ?></li><hr>
                <?php endforeach ?>
            </ul>
        <?php else : ?>
            No announcement.
        <?php endif; ?>
    </div>

    <br>

    <h1><?= Html::encode('List of Application') ?></h1>

    <?= Dialog::widget([
        'libName' => 'krajeeDialogAccept',
        'options' => [
            'type' => Dialog::TYPE_SUCCESS,
            'title' => 'Application Approval',
            'btnOKClass' => 'btn-success',
        ],
    ]); ?>
    <?= Dialog::widget([
        'libName' => 'krajeeDialogReject',
        'options' => [
            'type' => Dialog::TYPE_DANGER,
            'title' => 'Application Rejection',
            'btnOKClass' => 'btn-danger',
        ],
    ]); ?>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'organizationDetail.name',
            [
                'attribute' => 'application_status',
                'value' => function ($model) {
                    return Helper::getApplicationStatusStudent($model->application_status);
                },
                'contentOptions' => function ($model, $key, $index, $column) {

                    if ($model->application_status == 0)
                        $color = 'danger';
                    elseif ($model->application_status == 1)
                        $color = 'info';
                    elseif ($model->application_status == 2)
                        $color = 'warning';
                    elseif ($model->application_status == 3)
                        $color = 'primary';
                    elseif ($model->application_status == 4)
                        $color = 'success';

                    return [
                        'class' => 'text-' . $color
                    ];
                },
                'filter' => Helper::listApplicationStatusStudent(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'mergeHeader' => true,
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return date("l, j F Y, g:i A", $model->created_at);
                }

            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view} {accept} {reject}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {

                        $url = Url::to(['/organization-profile/view', 'id_organization_detail' => $model->organizationDetail->id]);

                        return Html::a('<span class="fas fa-eye">&nbsp;</span>', $url, $options);
                    },
                    'accept' => function ($url, $model, $key) {

                        $url = "#";

                        $data_url = Url::to(['/internship-application/accept-reject-organization', 'id_internship_application' => $model->id, 'application_status' => 4]);

                        $options = [
                            'id' => 'accept-application',
                            'data-organization-name' => $model->organizationDetail->name,
                            'data-url' => $data_url,
                            'data-pjax' => '0',
                        ];

                        if ($model->application_status == 3)
                            return Html::a('<span class="fas fa-check">&nbsp;</span>', $url, $options);
                    },
                    'reject' => function ($url, $model, $key) {

                        $url = "#";

                        $data_url = Url::to(['/internship-application/accept-reject-organization', 'id_internship_application' => $model->id, 'application_status' => '0']);

                        $options = [
                            'id' => 'reject-application',
                            'data-organization-name' => $model->organizationDetail->name,
                            'data-url' => $data_url,
                            'data-pjax' => '0',
                        ];

                        if ($model->application_status == 3)
                            return Html::a('<span class="fas fa-times">&nbsp;</span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <div class="card">
        <div class="card-body">
            <h3>Status Flow Note :</h3>                 
            <p class="text-center"><span class="text-info">Applied</span> <span class="fas fa-angle-right"></span> <span class="text-warning">In Review</span> <span class="fas fa-angle-right"></span> <span class="text-primary">Waiting for Student Approval</span> <span class="fas fa-angle-right"></span> <span class="text-success">Student Accepted</span></p>
        </div>
    </div>
</div>

<?php

$js = <<< JS

$("a#accept-application").on("click", function(event ) {
    var data_url = $(this).data('url');
    var data_organization_name = $(this).data('organization-name');
    krajeeDialogAccept.confirm("Are you sure you want to approve your internship placement at <b>" + data_organization_name + "</b>?<br><br>All other applications will be automatically rejected.", function (result) {
        if (result) {
            return window.location.replace(data_url);
        }
    });
});

$("a#reject-application").on("click", function() {
    var data_url = $(this).data('url');
    var data_organization_name = $(this).data('organization-name');
    krajeeDialogReject.confirm("Are you sure you want to reject your internship placement at <b>" + data_organization_name + "</b>?", function (result) {
        if (result) {
            return window.location.replace(data_url);
        }
    });
});

JS;
$this->registerJs($js);
?>