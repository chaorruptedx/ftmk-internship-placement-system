<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Account Management', [
    'name' => $modelUser->username,
]);

$this->params['breadcrumbs'][] = Yii::t('app', 'Account Management');
?>

<div class="account-management-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelUser, 'username')->textInput(['autofocus' => true, 'readOnly' => true]) ?>

    <?= $form->field($modelUser, 'email')->textInput(['autofocus' => true, 'readOnly' => true]) ?>

    <?= $form->field($modelUser, 'password')->passwordInput() ?>

    <div class="form-group">
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/site/index'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>