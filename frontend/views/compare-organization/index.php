<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CompareOrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Compare Organization');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover table-light">
            <thead>
                <tr>
                    <th scope="col">&nbsp;</th>
                    <th scope="col">
                        <div class="form-group">
                            <select id="compare-organization-1" name="compare_organization_1" class="form-control">
                                <option value="" selected disabled>Choose Organization ...</option>
                                <?php if (!empty($listOrganizationDetail) && is_array($listOrganizationDetail)) : ?>
                                    <?php foreach ($listOrganizationDetail as $organization_detail_id => $organization_detail_name) : ?>
                                        <option value="<?= $organization_detail_id; ?>"><?= $organization_detail_name; ?></option>
                                    <?php endforeach; ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </th>
                    <th scope="col">
                        <div class="form-group">
                            <select id="compare-organization-2" name="compare_organization_2" class="form-control">
                                <option value="" selected disabled>Choose Organization ...</option>
                                <?php if (!empty($listOrganizationDetail) && is_array($listOrganizationDetail)) : ?>
                                    <?php foreach ($listOrganizationDetail as $organization_detail_id => $organization_detail_name) : ?>
                                        <option value="<?= $organization_detail_id; ?>"><?= $organization_detail_name; ?></option>
                                    <?php endforeach; ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </th>
                    <th scope="col">
                        <div class="form-group">
                            <select id="compare-organization-3" name="compare_organization_3" class="form-control">
                                <option value="" selected disabled>Choose Organization ...</option>
                                <?php if (!empty($listOrganizationDetail) && is_array($listOrganizationDetail)) : ?>
                                    <?php foreach ($listOrganizationDetail as $organization_detail_id => $organization_detail_name) : ?>
                                        <option value="<?= $organization_detail_id; ?>"><?= $organization_detail_name; ?></option>
                                    <?php endforeach; ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody class="text-center">
                <tr>
                    <th>Name</th>
                    <td id="compare-1-name"></td>
                    <td id="compare-2-name"></td>
                    <td id="compare-3-name"></td>
                </tr>
                <tr>
                    <th>Organization Sector</th>
                    <td id="compare-1-sector"></td>
                    <td id="compare-2-sector"></td>
                    <td id="compare-3-sector"></td>
                </tr>
                <tr>
                    <th>Organization Address</th>
                    <td id="compare-1-address"></td>
                    <td id="compare-2-address"></td>
                    <td id="compare-3-address"></td>
                </tr>
                <tr>
                    <th>Organization State</th>
                    <td id="compare-1-state"></td>
                    <td id="compare-2-state"></td>
                    <td id="compare-3-state"></td>
                </tr>
                <tr>
                    <th>Working Days</th>
                    <td id="compare-1-days"></td>
                    <td id="compare-2-days"></td>
                    <td id="compare-3-days"></td>
                </tr>
                <tr>
                    <th>Working Hours</th>
                    <td id="compare-1-hours"></td>
                    <td id="compare-2-hours"></td>
                    <td id="compare-3-hours"></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td id="compare-1-email"></td>
                    <td id="compare-2-email"></td>
                    <td id="compare-3-email"></td>
                </tr>
                <tr>
                    <th>Tel. No.</th>
                    <td id="compare-1-tel"></td>
                    <td id="compare-2-tel"></td>
                    <td id="compare-3-tel"></td>
                </tr>
            </tbody>
            <tfoot class="text-center">
                <tr>
                    <th>&nbsp;</th>
                    <!-- <th class=""><a id="compare-1-view" href="<?//= base_url('public/student/studentapplicants/view_organization');?>" class="btn btn-success" style="visibility: hidden;">View Details</a></th>
                    <th class=""><a id="compare-2-view" href="<?//= base_url('public/student/studentapplicants/view_organization');?>" class="btn btn-success" style="visibility: hidden;">View Details</a></th>
                    <th class=""><a id="compare-3-view" href="<?//= base_url('public/student/studentapplicants/view_organization');?>" class="btn btn-success" style="visibility: hidden;">View Details</a></th> -->
                </tr>
            </tfoot>
        </table>
    </div>

    <?php Pjax::end(); ?>

</div>

<?php
$url = Url::to(['compare-organization']);

$script = <<< JS

$("select#compare-organization-1").on("change", function() {

    var id_organization_1 = $("select#compare-organization-1").val();

    var request = $.ajax({
        url: "$url",
        method: "POST",
        data: {id_organization: id_organization_1},
        dataType: "json"
    });

    request.done( function(data) {

        $('td#compare-1-name').empty().append(data.name);
        $('td#compare-1-sector').empty().append(data.type);
        $('td#compare-1-address').empty().append(data.address);
        $('td#compare-1-state').empty().append(data.id_state);
        $('td#compare-1-days').empty().append(data.start_day);
        $('td#compare-1-hours').empty().append(data.open_hour);
        $('td#compare-1-email').empty().append(data.status);
        $('td#compare-1-tel').empty().append(data.tel_no);

        // $('a#compare-1-view').css("visibility", "visible").attr("href", "view_organization/"+data.id);
    });
});

$("select#compare-organization-2").on("change", function() {

    var id_organization_2 = $("select#compare-organization-2").val();

    var request = $.ajax({
        url: "$url",
        method: "POST",
        data: {id_organization: id_organization_2},
        dataType: "json"
    });

    request.done( function(data) {

        $('td#compare-2-name').empty().append(data.name);
        $('td#compare-2-sector').empty().append(data.type);
        $('td#compare-2-address').empty().append(data.address);
        $('td#compare-2-state').empty().append(data.id_state);
        $('td#compare-2-days').empty().append(data.start_day);
        $('td#compare-2-hours').empty().append(data.open_hour);
        $('td#compare-2-email').empty().append(data.status);
        $('td#compare-2-tel').empty().append(data.tel_no);

        // $('a#compare-2-view').css("visibility", "visible").attr("href", "view_organization/"+data.id);
    });
});

$("select#compare-organization-3").on("change", function() {

var id_organization_3 = $("select#compare-organization-3").val();

var request = $.ajax({
    url: "$url",
    method: "POST",
    data: {id_organization: id_organization_3},
    dataType: "json"
});

request.done( function(data) {

    $('td#compare-3-name').empty().append(data.name);
    $('td#compare-3-sector').empty().append(data.type);
    $('td#compare-3-address').empty().append(data.address);
    $('td#compare-3-state').empty().append(data.id_state);
    $('td#compare-3-days').empty().append(data.start_day);
    $('td#compare-3-hours').empty().append(data.open_hour);
    $('td#compare-3-email').empty().append(data.status);
    $('td#compare-3-tel').empty().append(data.tel_no);

    // $('a#compare-3-view').css("visibility", "visible").attr("href", "view_organization/"+data.id);
});
});

JS;
$this->registerJs($script);
?>