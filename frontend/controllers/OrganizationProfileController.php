<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\OrganizationDetail;
use common\models\OrganizationProfileSearch;
use common\models\User;
use common\models\Attachment;
use common\models\JobScopeRequirement;
use common\models\InternshipApplication;

/**
 * OrganizationProfileController implements the CRUD actions for OrganizationDetail model.
 */
class OrganizationProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'update'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return (User::isUserStudent(Yii::$app->user->identity->username) || User::isUserOrganization(Yii::$app->user->identity->username));
                        }
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserOrganization(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays a single OrganizationDetail model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_organization_detail = null)
    {
        if ($id_organization_detail === null)
        {
            $modelUser = User::findIdentity(Yii::$app->user->identity->id);
            $modelOrganizationDetail = $modelUser->organizationDetail;

            if ($modelOrganizationDetail === null)
                $modelOrganizationDetail = new OrganizationDetail();
            
            $modelAttachment = $modelOrganizationDetail->attachment;

            if ($modelAttachment === null)
                $modelAttachment = new Attachment();
        }
        else
        {
            $modelUserStudent = User::findIdentity(Yii::$app->user->identity->id);
            $modelOrganizationDetail = $this->findModel($id_organization_detail);
            $modelUser = $modelOrganizationDetail->user;
            $modelAttachment = $modelOrganizationDetail->attachment;

            $application_exist = InternshipApplication::checkExistingApplication($modelUserStudent->personalDetail->id, $modelOrganizationDetail->id, [1, 2, 3, 4]);
        }

        return $this->render('view', [
            'modelUser' => $modelUser,
            'modelOrganizationDetail' => $modelOrganizationDetail,
            'modelAttachment' => $modelAttachment,
            'application_exist' => isset($application_exist) ? $application_exist : false,
        ]);
    }

    /**
     * Updates an existing OrganizationDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);
        $modelOrganizationDetail = $modelUser->organizationDetail;

        if ($modelOrganizationDetail === null)
            $modelOrganizationDetail = new OrganizationDetail();

        $modelAttachment = $modelOrganizationDetail->attachment;

        if ($modelAttachment === null)
        {
            $modelAttachment = new Attachment();
            $modelAttachment->scenario = Attachment::SCENARIO_CREATE;
        }
        else
        {
            $modelAttachment->scenario = Attachment::SCENARIO_UPDATE;
        }

        $modelJobScopeRequirement = new JobScopeRequirement;

        if (isset($modelOrganizationDetail->jobScopeRequirements) && is_array($modelOrganizationDetail->jobScopeRequirements))
        {
            foreach ($modelOrganizationDetail->jobScopeRequirements as $key => $value)
            {
                $modelJobScopeRequirement->id_programme_array[] = $value['id_programme'];
            }
        }

        if ($modelOrganizationDetail->load(Yii::$app->request->post()) && $modelJobScopeRequirement->load(Yii::$app->request->post()))
        {
            $modelAttachment->imageFile = UploadedFile::getInstance($modelAttachment, 'imageFile');

            if ($modelOrganizationDetail->validate() && $modelJobScopeRequirement->validate() && $modelAttachment->validate())
            {
                if ($modelAttachment->imageFile !== null)
                    $modelAttachment->saveAttachment($modelUser->id, 1);

                $modelOrganizationDetail->saveOrganizationDetail($modelUser->id, $modelAttachment->id);

                $modelJobScopeRequirement->saveJobScopeRequirement($modelOrganizationDetail->id);

                Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Organization profile has been updated successfully.');
            }
        }

        return $this->render('update', [
            'modelUser' => $modelUser,
            'modelOrganizationDetail' => $modelOrganizationDetail,
            'modelAttachment' => $modelAttachment,
            'modelJobScopeRequirement' => $modelJobScopeRequirement,
        ]);
    }

    /**
     * Finds the OrganizationDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrganizationDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrganizationDetail::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
