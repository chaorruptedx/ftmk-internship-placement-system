<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use common\models\OrganizationDetail;
use common\models\CompareOrganizationSearch;
use common\models\User;
use common\models\BankStatus;
use common\components\Helper;

/**
 * CompareOrganizationController implements the CRUD actions for OrganizationDetail model.
 */
class CompareOrganizationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserStudent(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all OrganizationDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $listOrganizationDetail = BankStatus::listOrganizationDetail();

        return $this->render('index', [
            'listOrganizationDetail' => $listOrganizationDetail,
        ]);
    }

    public function actionCompareOrganization()
    {
        $model = $this->findModel(Yii::$app->request->post('id_organization'));
        $model->type = Helper::getOrganizationType($model->type);
        $model->id_state = $model->state->name;
        $model->start_day = Helper::getDay($model->start_day).' until '.Helper::getDay($model->end_day);
        $model->open_hour = date("g:i A", $model->open_hour).' until '. date("g:i A", $model->close_hour);
        $model->status = $model->user->email;

        return Json::encode($model);
    }

    /**
     * Finds the OrganizationDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrganizationDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrganizationDetail::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
