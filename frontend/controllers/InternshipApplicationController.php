<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\OrganizationDetail;
use common\models\InternshipApplicationSearch;
use common\models\User;
use common\models\PersonalDetail;
use common\models\InternshipApplication;
use common\models\AcademicSession;

/**
 * InternshipApplicationController implements the CRUD actions for OrganizationDetail model.
 */
class InternshipApplicationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['organization-list', 'apply-internship'],
                'rules' => [
                    [
                        'actions' => ['organization-list', 'apply-internship'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserStudent(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all OrganizationDetail models.
     * @return mixed
     */
    public function actionOrganizationList()
    {
        $searchModel = new InternshipApplicationSearch();
        $searchModel->id_programme = User::findIdentity(Yii::$app->user->identity->id)->personalDetail->programme->id;
        $searchModel->accepting_internship = 1; // Yes
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('organization-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing OrganizationDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionApplyInternship($id_organization_detail)
    {
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);
        $modelPersonalDetail = $modelUser->personalDetail;
        $modelOrganizationDetail = $this->findModel($id_organization_detail);
        $modelInternshipApplication = new InternshipApplication();
        $modelAcademicSession = AcademicSession::find()
            ->where(['status' => 1])
            ->one();

        if (empty($modelAcademicSession))
            throw new NotFoundHttpException(Yii::t('app', 'Active academic session does not exist.'));

        if (Yii::$app->request->post())
        {
            $modelInternshipApplication->saveInternshipApplication($modelPersonalDetail->id, $modelOrganizationDetail->id, 1, $modelAcademicSession->id);    
        }

        return $this->redirect(['/organization-profile/view', 'id_organization_detail' => $id_organization_detail]);
    }

    public function actionAcceptRejectOrganization($id_internship_application, $application_status)
    {
        $modelInternshipApplication = InternshipApplication::findModelInternshipApplication($id_internship_application);
        
        $modelInternshipApplication->application_status = $application_status;
        $modelInternshipApplication->save();

        if ($application_status == 4) // After Accept, Reject All Other Applications
        {
            $modelInternshipApplications = InternshipApplication::find()
                ->where(['<', 'internship_application.application_status', $application_status])
                ->active()
                ->all();

            if (!empty($modelInternshipApplications) && is_array($modelInternshipApplications))
            {
                foreach ($modelInternshipApplications as $modelInternshipApplication_reject)
                {
                    $modelInternshipApplication_reject->application_status = 0; // Reject

                    $modelInternshipApplication_reject->save();
                }
            }
        }

        return $this->redirect(['/site/index']);
    }

    /**
     * Finds the OrganizationDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrganizationDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrganizationDetail::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
