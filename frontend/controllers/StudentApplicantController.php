<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\InternshipApplication;
use common\models\StudentApplicantSearch;
use common\models\User;
use common\models\PersonalDetail;

/**
 * StudentApplicantController implements the CRUD actions for InternshipApplication model.
 */
class StudentApplicantController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserOrganization(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all InternshipApplication models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);

        $searchModel = new StudentApplicantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $modelUser->organizationDetail->id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAcceptRejectInternshipApplication($id_personal_detail, $application_status)
    {
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);
        $modelOrganizationDetail = $modelUser->organizationDetail;

        $modelPersonalDetail = PersonalDetail::findByID($id_personal_detail);
 
        $modelInternshipApplication = InternshipApplication::getActiveInternshipApplication($modelPersonalDetail->id, $modelOrganizationDetail->id);

        $modelInternshipApplication->saveInternshipApplication($modelPersonalDetail->id, $modelOrganizationDetail->id, $application_status); 

        return $this->redirect(['/resume/view', 'id_student' => $modelPersonalDetail->user->id]);
    }

    /**
     * Finds the InternshipApplication model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InternshipApplication the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InternshipApplication::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
