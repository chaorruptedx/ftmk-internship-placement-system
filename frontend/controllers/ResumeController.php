<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\models\PersonalDetail;
use common\models\ResumeSearch;
use common\models\User;
use common\models\Attachment;
use common\models\DynamicForm;
use common\models\Skill;
use common\models\Language;
use common\models\Education;
use common\models\Experience;
use common\models\Achievement;
use common\models\OrganizationDetail;
use common\models\InternshipApplication;

/**
 * ResumeController implements the CRUD actions for PersonalDetail model.
 */
class ResumeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'update'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return (User::isUserStudent(Yii::$app->user->identity->username) || User::isUserOrganization(Yii::$app->user->identity->username));
                        }
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserStudent(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays a single PersonalDetail model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_student = null)
    {
        if ($id_student === null)
            $modelPersonalDetail = PersonalDetail::findByIDUser(Yii::$app->user->identity->id);
        else
        {
            $modelPersonalDetail = PersonalDetail::findByIDUser($id_student);
            $modelOrganizationDetail = OrganizationDetail::findByIDUser(Yii::$app->user->identity->id);
            $modelInternshipApplication = InternshipApplication::getInternshipApplication($modelPersonalDetail->id, $modelOrganizationDetail->id);

            if ($modelInternshipApplication->application_status == 1)
            {
                $modelInternshipApplication->application_status = 2;
                $modelInternshipApplication->save();
            }
        }
        
        $modelAttachment = $modelPersonalDetail->attachment;
        $modelSkills = $modelPersonalDetail->skills;
        $modelLanguages = $modelPersonalDetail->languages;
        $modelEducations = $modelPersonalDetail->educations;
        $modelExperiences = $modelPersonalDetail->experiences;
        $modelAchievements = $modelPersonalDetail->achievements;

        return $this->render('view', [
            'modelPersonalDetail' => $modelPersonalDetail,
            'modelAttachment' => $modelAttachment,
            'modelSkills' => $modelSkills,
            'modelLanguages' => $modelLanguages,
            'modelEducations' => $modelEducations,
            'modelExperiences' => $modelExperiences,
            'modelAchievements' => $modelAchievements,
            'modelInternshipApplication' => isset($modelInternshipApplication) ? $modelInternshipApplication : null,
        ]);
    }

    
    /**
     * Updates an existing PersonalDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate()
    {
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);
        $modelPersonalDetail = $modelUser->personalDetail;
        $modelAttachment = $modelPersonalDetail->attachment;
        $modelSkills = $modelPersonalDetail->skills;
        $modelLanguages = $modelPersonalDetail->languages;
        $modelEducations = $modelPersonalDetail->educations;
        $modelExperiences = $modelPersonalDetail->experiences;
        $modelAchievements = $modelPersonalDetail->achievements;

        if ($modelAttachment === null)
        {
            $modelAttachment = new Attachment();
            $modelAttachment->scenario = Attachment::SCENARIO_CREATE;
        }
        else
        {
            $modelAttachment->scenario = Attachment::SCENARIO_UPDATE;
        }

        if (empty($modelSkills))
            $modelSkills = [new Skill()];

        if (empty($modelLanguages))
            $modelLanguages = [new Language()];

        if (empty($modelEducations))
            $modelEducations = [new Education()];

        if (empty($modelExperiences))
            $modelExperiences = [new Experience()];

        if (empty($modelAchievements))
            $modelAchievements = [new Achievement()];

        if ($modelPersonalDetail->load(Yii::$app->request->post()))
        {
            $modelAttachment->imageFile = UploadedFile::getInstance($modelAttachment, 'imageFile');
            
            $skillOldIDs = ArrayHelper::map($modelSkills, 'id', 'id');
            $modelSkills = DynamicForm::createMultiple(Skill::classname(), $modelSkills);

            $languageOldIDs = ArrayHelper::map($modelLanguages, 'id', 'id');
            $modelLanguages = DynamicForm::createMultiple(Language::classname(), $modelLanguages);

            $educationOldIDs = ArrayHelper::map($modelEducations, 'id', 'id');
            $modelEducations = DynamicForm::createMultiple(Education::classname(), $modelEducations);

            $experienceOldIDs = ArrayHelper::map($modelExperiences, 'id', 'id');
            $modelExperiences = DynamicForm::createMultiple(Experience::classname(), $modelExperiences);

            $achievementOldIDs = ArrayHelper::map($modelAchievements, 'id', 'id');
            $modelAchievements = DynamicForm::createMultiple(Achievement::classname(), $modelAchievements);

            if (Model::loadMultiple($modelSkills, Yii::$app->request->post()) && Model::loadMultiple($modelLanguages, Yii::$app->request->post()) && Model::loadMultiple($modelEducations, Yii::$app->request->post()) && Model::loadMultiple($modelExperiences, Yii::$app->request->post()) && Model::loadMultiple($modelAchievements, Yii::$app->request->post()))
            {
                $skillDeletedIDs = array_diff($skillOldIDs, array_filter(ArrayHelper::map($modelSkills, 'id', 'id')));
                $languageDeletedIDs = array_diff($languageOldIDs, array_filter(ArrayHelper::map($modelLanguages, 'id', 'id')));
                $educationDeletedIDs = array_diff($educationOldIDs, array_filter(ArrayHelper::map($modelEducations, 'id', 'id')));
                $experienceDeletedIDs = array_diff($experienceOldIDs, array_filter(ArrayHelper::map($modelExperiences, 'id', 'id')));
                $achievementDeletedIDs = array_diff($achievementOldIDs, array_filter(ArrayHelper::map($modelAchievements, 'id', 'id')));
                
                $modelPersonalDetail->scenario = PersonalDetail::SCENARIO_SAVE_STUDENT_RESUME;

                if ($modelPersonalDetail->validate() && $modelAttachment->validate() && Model::validateMultiple($modelSkills) && Model::validateMultiple($modelLanguages) && Model::validateMultiple($modelEducations) && Model::validateMultiple($modelExperiences) && Model::validateMultiple($modelAchievements))
                {
                    if ($modelAttachment->imageFile !== null)
                        $modelAttachment->saveAttachment($modelUser->id, 1);

                    $modelPersonalDetail->savePersonalDetailStudentResume($modelAttachment->id);

                    if (!empty($skillDeletedIDs) && !in_array(null, $skillDeletedIDs, true))
                    {
                        Skill::deleteSkills($skillDeletedIDs);
                    }

                    foreach ($modelSkills as $modelSkill)
                    {
                        $modelSkill->saveSkill($modelPersonalDetail->id);
                    }

                    if (!empty($languageDeletedIDs) && !in_array(null, $languageDeletedIDs, true))
                    {
                        Language::deleteLanguages($languageDeletedIDs);
                    }

                    foreach ($modelLanguages as $modelLanguage)
                    {
                        $modelLanguage->saveLanguage($modelPersonalDetail->id);
                    }
                    
                    if (!empty($educationDeletedIDs) && !in_array(null, $educationDeletedIDs, true))
                    {
                        Education::deleteEducations($educationDeletedIDs);
                    }

                    foreach ($modelEducations as $modelEducation)
                    {
                        $modelEducation->saveEducation($modelPersonalDetail->id);
                    }

                    if (!empty($experienceDeletedIDs) && !in_array(null, $experienceDeletedIDs, true))
                    {
                        Experience::deleteExperiences($experienceDeletedIDs);
                    }

                    foreach ($modelExperiences as $modelExperience)
                    {
                        $modelExperience->saveExperience($modelPersonalDetail->id);
                    }

                    if (!empty($achievementDeletedIDs) && !in_array(null, $achievementDeletedIDs, true))
                    {
                        Achievement::deleteAchievements($achievementDeletedIDs);
                    }

                    foreach ($modelAchievements as $modelAchievement)
                    {
                        $modelAchievement->saveAchievement($modelPersonalDetail->id);
                    }

                    Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Student resume details has been updated successfully.');
                }
            }
        }

        return $this->render('update', [
            'modelPersonalDetail' => $modelPersonalDetail,
            'modelAttachment' => $modelAttachment,
            'modelSkills' => $modelSkills,
            'modelLanguages' => $modelLanguages,
            'modelEducations' => $modelEducations,
            'modelExperiences' => $modelExperiences,
            'modelAchievements' => $modelAchievements,
        ]);
    }

    /**
     * Finds the PersonalDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PersonalDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PersonalDetail::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
