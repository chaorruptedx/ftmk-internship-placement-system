# FTMK Internship Placement System

FTMK Internship Placement System is a system that facilitate staff to manage students for industrial training placement.

## Requirements

* Apache (Apache 2.4.46 Recommended)
* PHP version 5.4 or above (PHP 7.4.16 Recommended)
	* [intl](http://php.net/manual/en/intl.requirements.php)
* MySQL (MySQL 8.0.23 Recommended)
* Git - https://git-scm.com/
* Composer - https://getcomposer.org/download/
* Brain

## Installation

* Download source code => ```git clone https://gitlab.com/chaorruptedx/ftmk-internship-placement-system```
* In project directory, open a console terminal
    * Execute the ```init``` command and select ```dev``` as environment
    * Execute the ```composer update``` command
* Create database `ftmk_internship_placement_system` in MySQL
* Adjust the database configuration in `common/config/main-local.php`, for example:
```php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=ftmk_internship_placement_system',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
```
*  In project directory, open a console terminal, execute the ```yii migrate``` command

## Git Command

* To Pull System from GitLab:
	* ```git pull```

* To Push System to GitLab:
	* ```git pull```
	* ```git status```
	* ```git add YourFilePathwayName```
	* ```git status```
	* ```git commit -m "YourMessage"```
    * ```git pull```
	* ```git push```

## References

* Yii2 Framework - https://www.yiiframework.com/doc/guide/2.0/en/intro-yii
* Fontawesome 5.15.3 - https://fontawesome.com/icons