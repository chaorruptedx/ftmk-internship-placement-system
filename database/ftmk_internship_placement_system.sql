-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 10, 2021 at 05:14 PM
-- Server version: 8.0.23
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ftmk_internship_placement_system`
--
CREATE DATABASE IF NOT EXISTS `ftmk_internship_placement_system` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ftmk_internship_placement_system`;

-- --------------------------------------------------------

--
-- Table structure for table `academic_session`
--

CREATE TABLE `academic_session` (
  `id` int NOT NULL,
  `semester` int NOT NULL,
  `start_year` int NOT NULL,
  `end_year` int NOT NULL,
  `start_internship_date` date NOT NULL,
  `end_internship_date` date NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `achievement`
--

CREATE TABLE `achievement` (
  `id` int NOT NULL,
  `id_personal_detail` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `year` int NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int NOT NULL,
  `id_user` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `announcement_role`
--

CREATE TABLE `announcement_role` (
  `id` int NOT NULL,
  `id_announcement` int NOT NULL,
  `role_no` smallint NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

CREATE TABLE `attachment` (
  `id` int NOT NULL,
  `category` smallint NOT NULL COMMENT '1 = Profile Picture, 2 = Industrial Training Report',
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int NOT NULL,
  `id_personal_detail` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_year` int NOT NULL,
  `end_year` int NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `id` int NOT NULL,
  `id_personal_detail` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_year` int NOT NULL,
  `end_year` int NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `internship_application`
--

CREATE TABLE `internship_application` (
  `id` int NOT NULL,
  `id_personal_detail` int NOT NULL,
  `id_organization_detail` int NOT NULL,
  `id_academic_session` int NOT NULL,
  `application_status` smallint NOT NULL COMMENT '0 = Reject, 1 = Pending, 2 = In Review, 3 = Organization Accept, 4 = Student Accept',
  `notes` varchar(255) DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_scope_requirement`
--

CREATE TABLE `job_scope_requirement` (
  `id` int NOT NULL,
  `id_programme` int NOT NULL,
  `id_organization_detail` int NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int NOT NULL,
  `id_personal_detail` int NOT NULL,
  `id_language` int NOT NULL,
  `scale` smallint NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lookup_department`
--

CREATE TABLE `lookup_department` (
  `id` int NOT NULL,
  `id_faculty` int NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lookup_department`
--

INSERT INTO `lookup_department` (`id`, `id_faculty`, `code`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'CSC', 'Department of Computer System & Communication', 1, 1631264018, 1631264018, 0),
(2, 5, 'ICA', 'Department of Intelligent Computing and Analytics', 1, 1631264018, 1631264018, 0),
(3, 5, 'IM', 'Department of Interactive Media', 1, 1631264018, 1631264018, 0),
(4, 5, 'SE', 'Department of Software Engineering', 1, 1631264018, 1631264018, 0),
(5, 5, 'DIP', 'Department of Diploma', 1, 1631264018, 1631264018, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lookup_faculty`
--

CREATE TABLE `lookup_faculty` (
  `id` int NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lookup_faculty`
--

INSERT INTO `lookup_faculty` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'FKEKK', 'Faculty of Electronics and Computer Engineering', 1, 1631264018, 1631264018, 0),
(2, 'FKE', 'Faculty of Electrical Engineering', 1, 1631264018, 1631264018, 0),
(3, 'FKM', 'Faculty of Mechanical Engineering', 1, 1631264018, 1631264018, 0),
(4, 'FKP', 'Faculty of Manufacturing Engineering', 1, 1631264018, 1631264018, 0),
(5, 'FTMK', 'Faculty of Information and Communications Technology', 1, 1631264018, 1631264018, 0),
(6, 'FPTT', 'Faculty of Technology Management and Technopreneurship', 1, 1631264018, 1631264018, 0),
(7, 'FTKEE', 'Faculty of Electrical and Electronic Engineering Technology', 1, 1631264018, 1631264018, 0),
(8, 'FTKMP', 'Faculty of Mechanical and Manufacturing Engineering Technology', 1, 1631264018, 1631264018, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lookup_language`
--

CREATE TABLE `lookup_language` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lookup_programme`
--

CREATE TABLE `lookup_programme` (
  `id` int NOT NULL,
  `id_department` int NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lookup_programme`
--

INSERT INTO `lookup_programme` (`id`, `id_department`, `code`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'BITC', 'Bachelor of Computer Science (Computer Networking)', 1, 1631264018, 1631264018, 0),
(2, 1, 'BITZ', 'Bachelor of Computer Science (Computer Security)', 1, 1631264018, 1631264018, 0),
(3, 2, 'BITI', 'Bachelor of Computer Science (Artificial Intelligence)', 1, 1631264018, 1631264018, 0),
(4, 3, 'BITE', 'Bachelor of Information Technology (Game Technology)', 1, 1631264018, 1631264018, 0),
(5, 3, 'BITM', 'Bachelor of Computer Science (Interactive Media)', 1, 1631264018, 1631264018, 0),
(6, 4, 'BITD', 'Bachelor of Computer Science (Database Management)', 1, 1631264018, 1631264018, 0),
(7, 4, 'BITS', 'Bachelor of Computer Science (Software Development)', 1, 1631264018, 1631264018, 0),
(8, 5, 'DIT', 'Diploma in Information and Communication Technology', 1, 1631264018, 1631264018, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lookup_state`
--

CREATE TABLE `lookup_state` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` smallint NOT NULL COMMENT '1 = Saturday and Sunday Holidays, 2 = Friday and Saturday Holidays',
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lookup_state`
--

INSERT INTO `lookup_state` (`id`, `name`, `type`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Johor', 2, 1, 1631264018, 1631264018, 0),
(2, 'Kedah', 2, 1, 1631264018, 1631264018, 0),
(3, 'Kelantan', 2, 1, 1631264018, 1631264018, 0),
(4, 'Malacca', 1, 1, 1631264018, 1631264018, 0),
(5, 'Negeri Sembilan', 1, 1, 1631264018, 1631264018, 0),
(6, 'Pahang', 1, 1, 1631264018, 1631264018, 0),
(7, 'Penang', 1, 1, 1631264018, 1631264018, 0),
(8, 'Perak', 1, 1, 1631264018, 1631264018, 0),
(9, 'Perlis', 1, 1, 1631264018, 1631264018, 0),
(10, 'Sabah', 1, 1, 1631264018, 1631264018, 0),
(11, 'Sarawak', 1, 1, 1631264018, 1631264018, 0),
(12, 'Selangor', 1, 1, 1631264018, 1631264018, 0),
(13, 'Terengganu', 2, 1, 1631264018, 1631264018, 0),
(14, 'Wilayah Persekutuan', 1, 1, 1631264018, 1631264018, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1631264014),
('m130524_201442_init', 1631264017),
('m190124_110200_add_verification_token_column_to_user_table', 1631264017),
('m210417_120352_configure_index_for_user_table', 1631264017),
('m210420_101531_create_announcement_table', 1631264018),
('m210421_042017_create_lookup_faculty_table', 1631264018),
('m210421_195503_create_lookup_department_table', 1631264018),
('m210421_235938_create_lookup_state_table', 1631264018),
('m210422_012538_create_lookup_language_table', 1631264018),
('m210516_012113_create_lookup_programme_table', 1631264018),
('m210516_032130_create_academic_session_table', 1631264018),
('m210516_121717_create_announcement_role_table', 1631264018),
('m210517_195116_create_attachment_table', 1631264018),
('m210517_195955_create_personal_detail_table', 1631264171),
('m210519_234832_create_supervision_table', 1631264171),
('m210527_033218_create_language_table', 1631264171),
('m210527_034625_create_education_table', 1631264171),
('m210527_035133_create_experience_table', 1631264171),
('m210527_035350_create_skill_table', 1631264171),
('m210527_035409_create_achievement_table', 1631264172),
('m210607_185741_create_organization_detail_table', 1631264172),
('m210608_233649_create_internship_application_table', 1631264172),
('m210817_011120_create_job_scope_requirement_table', 1631264172);

-- --------------------------------------------------------

--
-- Table structure for table `organization_detail`
--

CREATE TABLE `organization_detail` (
  `id` int NOT NULL,
  `id_user` int NOT NULL,
  `id_state` int NOT NULL,
  `id_attachment` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` smallint NOT NULL COMMENT '1 = Government, 2 = Private',
  `address` varchar(255) NOT NULL,
  `tel_no` int NOT NULL,
  `start_day` smallint NOT NULL COMMENT '0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday, 5 = Saturday, 6 = Sunday',
  `end_day` smallint NOT NULL COMMENT '0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday, 5 = Saturday, 6 = Sunday',
  `open_hour` time NOT NULL,
  `close_hour` time NOT NULL,
  `job_description` text NOT NULL,
  `accepting_internship` smallint NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_detail`
--

CREATE TABLE `personal_detail` (
  `id` int NOT NULL,
  `id_user` int NOT NULL,
  `id_programme` int NOT NULL,
  `id_state` int DEFAULT NULL,
  `id_attachment` int DEFAULT NULL COMMENT 'Category = Profile Picture',
  `id_referee_one` int DEFAULT NULL,
  `id_referee_two` int DEFAULT NULL,
  `nric_no` bigint DEFAULT NULL,
  `user_no` varchar(255) NOT NULL COMMENT 'Staff No. or Matric No.',
  `name` varchar(255) NOT NULL,
  `gender_no` smallint NOT NULL COMMENT '1 = Male, 2 = Female',
  `religion` varchar(255) DEFAULT NULL,
  `tel_no` int DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE `skill` (
  `id` int NOT NULL,
  `id_personal_detail` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `scale` smallint NOT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supervision`
--

CREATE TABLE `supervision` (
  `id` int NOT NULL,
  `id_supervisor` int NOT NULL,
  `id_supervisee` int DEFAULT NULL,
  `id_academic_session` int DEFAULT NULL,
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `verification_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `role_no` smallint NOT NULL COMMENT '1 = Administrator, 2 = Coordinator, 3 = Supervisor, 4 = Student, 5 = Organization',
  `status` smallint NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive, -1 = Deleted',
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `deleted_at` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `verification_token`, `email`, `role_no`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'u4HbOQNtMaWg9b5eTl7EhSU_9x6yHFhu', '$2y$13$zfSBB4Y/kDXwq6nKkB2aaOZZwTBMYhdZVi0RVwVyhZ1wMxUi6hw9C', NULL, 'jdfYLOk214x4PZ2kSqDnEIpWDxbzwGO__1631264500', 'admin.fips@utem.edu.my', 1, 1, 1631264500, 1631264500, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_session`
--
ALTER TABLE `academic_session`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk-acdmc_session-semester-start_year-end_year-status-deleted_at` (`semester`,`start_year`,`end_year`,`status`,`deleted_at`);

--
-- Indexes for table `achievement`
--
ALTER TABLE `achievement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-achievement-id_personal_detail` (`id_personal_detail`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-announcement-id_user` (`id_user`);

--
-- Indexes for table `announcement_role`
--
ALTER TABLE `announcement_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-announcement_role-id_announcement` (`id_announcement`);

--
-- Indexes for table `attachment`
--
ALTER TABLE `attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-education-id_personal_detail` (`id_personal_detail`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-experience-id_personal_detail` (`id_personal_detail`);

--
-- Indexes for table `internship_application`
--
ALTER TABLE `internship_application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-internship_application-id_personal_detail` (`id_personal_detail`),
  ADD KEY `fk-internship_application-id_organization_detail` (`id_organization_detail`),
  ADD KEY `fk-internship_application-id_academic_session` (`id_academic_session`);

--
-- Indexes for table `job_scope_requirement`
--
ALTER TABLE `job_scope_requirement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-job_scope_requirement-id_programme` (`id_programme`),
  ADD KEY `fk-job_scope_requirement-id_organization_detail` (`id_organization_detail`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-language-id_personal_detail` (`id_personal_detail`),
  ADD KEY `fk-language-id_language` (`id_language`);

--
-- Indexes for table `lookup_department`
--
ALTER TABLE `lookup_department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk-lookup_department-code-status-deleted_at` (`code`,`status`,`deleted_at`),
  ADD KEY `fk-lookup_department-id_faculty` (`id_faculty`);

--
-- Indexes for table `lookup_faculty`
--
ALTER TABLE `lookup_faculty`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk-lookup_faculty-code-status-deleted_at` (`code`,`status`,`deleted_at`);

--
-- Indexes for table `lookup_language`
--
ALTER TABLE `lookup_language`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk-lookup_language-name-status-deleted_at` (`name`,`status`,`deleted_at`);

--
-- Indexes for table `lookup_programme`
--
ALTER TABLE `lookup_programme`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk-lookup_programme-code-status-deleted_at` (`code`,`status`,`deleted_at`),
  ADD KEY `fk-lookup_programme-id_department` (`id_department`);

--
-- Indexes for table `lookup_state`
--
ALTER TABLE `lookup_state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk-lookup_state-name-status-deleted_at` (`name`,`status`,`deleted_at`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `organization_detail`
--
ALTER TABLE `organization_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-organization_detail-id_user` (`id_user`),
  ADD KEY `fk-organization_detail-id_state` (`id_state`),
  ADD KEY `fk-organization_detail-id_attachment` (`id_attachment`);

--
-- Indexes for table `personal_detail`
--
ALTER TABLE `personal_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-personal_detail-id_user` (`id_user`),
  ADD KEY `fk-personal_detail-id_programme` (`id_programme`),
  ADD KEY `fk-personal_detail-id_state` (`id_state`),
  ADD KEY `fk-personal_detail-id_attachment` (`id_attachment`),
  ADD KEY `fk-personal_detail-id_referee_one` (`id_referee_one`),
  ADD KEY `fk-personal_detail-id_referee_two` (`id_referee_two`);

--
-- Indexes for table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-skill-id_personal_detail` (`id_personal_detail`);

--
-- Indexes for table `supervision`
--
ALTER TABLE `supervision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-supervision-id_supervisor` (`id_supervisor`),
  ADD KEY `fk-supervision-id_supervisee` (`id_supervisee`),
  ADD KEY `fk-supervision-id_academic_session` (`id_academic_session`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk-user-username-status-deleted_at` (`username`,`status`,`deleted_at`),
  ADD UNIQUE KEY `uk-user-email-status-deleted_at` (`email`,`status`,`deleted_at`),
  ADD UNIQUE KEY `uk-user-password_reset_token-status-deleted_at` (`password_reset_token`,`status`,`deleted_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_session`
--
ALTER TABLE `academic_session`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `achievement`
--
ALTER TABLE `achievement`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement_role`
--
ALTER TABLE `announcement_role`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attachment`
--
ALTER TABLE `attachment`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `internship_application`
--
ALTER TABLE `internship_application`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_scope_requirement`
--
ALTER TABLE `job_scope_requirement`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lookup_department`
--
ALTER TABLE `lookup_department`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lookup_faculty`
--
ALTER TABLE `lookup_faculty`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `lookup_language`
--
ALTER TABLE `lookup_language`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lookup_programme`
--
ALTER TABLE `lookup_programme`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `lookup_state`
--
ALTER TABLE `lookup_state`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `organization_detail`
--
ALTER TABLE `organization_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_detail`
--
ALTER TABLE `personal_detail`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skill`
--
ALTER TABLE `skill`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supervision`
--
ALTER TABLE `supervision`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `achievement`
--
ALTER TABLE `achievement`
  ADD CONSTRAINT `fk-achievement-id_personal_detail` FOREIGN KEY (`id_personal_detail`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
  ADD CONSTRAINT `fk-announcement-id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `announcement_role`
--
ALTER TABLE `announcement_role`
  ADD CONSTRAINT `fk-announcement_role-id_announcement` FOREIGN KEY (`id_announcement`) REFERENCES `announcement` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `education`
--
ALTER TABLE `education`
  ADD CONSTRAINT `fk-education-id_personal_detail` FOREIGN KEY (`id_personal_detail`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `experience`
--
ALTER TABLE `experience`
  ADD CONSTRAINT `fk-experience-id_personal_detail` FOREIGN KEY (`id_personal_detail`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `internship_application`
--
ALTER TABLE `internship_application`
  ADD CONSTRAINT `fk-internship_application-id_academic_session` FOREIGN KEY (`id_academic_session`) REFERENCES `academic_session` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-internship_application-id_organization_detail` FOREIGN KEY (`id_organization_detail`) REFERENCES `organization_detail` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-internship_application-id_personal_detail` FOREIGN KEY (`id_personal_detail`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_scope_requirement`
--
ALTER TABLE `job_scope_requirement`
  ADD CONSTRAINT `fk-job_scope_requirement-id_organization_detail` FOREIGN KEY (`id_organization_detail`) REFERENCES `organization_detail` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-job_scope_requirement-id_programme` FOREIGN KEY (`id_programme`) REFERENCES `lookup_programme` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `language`
--
ALTER TABLE `language`
  ADD CONSTRAINT `fk-language-id_language` FOREIGN KEY (`id_language`) REFERENCES `lookup_language` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-language-id_personal_detail` FOREIGN KEY (`id_personal_detail`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lookup_department`
--
ALTER TABLE `lookup_department`
  ADD CONSTRAINT `fk-lookup_department-id_faculty` FOREIGN KEY (`id_faculty`) REFERENCES `lookup_faculty` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lookup_programme`
--
ALTER TABLE `lookup_programme`
  ADD CONSTRAINT `fk-lookup_programme-id_department` FOREIGN KEY (`id_department`) REFERENCES `lookup_department` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `organization_detail`
--
ALTER TABLE `organization_detail`
  ADD CONSTRAINT `fk-organization_detail-id_attachment` FOREIGN KEY (`id_attachment`) REFERENCES `attachment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-organization_detail-id_state` FOREIGN KEY (`id_state`) REFERENCES `lookup_state` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-organization_detail-id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `personal_detail`
--
ALTER TABLE `personal_detail`
  ADD CONSTRAINT `fk-personal_detail-id_attachment` FOREIGN KEY (`id_attachment`) REFERENCES `attachment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-personal_detail-id_programme` FOREIGN KEY (`id_programme`) REFERENCES `lookup_programme` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-personal_detail-id_referee_one` FOREIGN KEY (`id_referee_one`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-personal_detail-id_referee_two` FOREIGN KEY (`id_referee_two`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-personal_detail-id_state` FOREIGN KEY (`id_state`) REFERENCES `lookup_state` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-personal_detail-id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `skill`
--
ALTER TABLE `skill`
  ADD CONSTRAINT `fk-skill-id_personal_detail` FOREIGN KEY (`id_personal_detail`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `supervision`
--
ALTER TABLE `supervision`
  ADD CONSTRAINT `fk-supervision-id_academic_session` FOREIGN KEY (`id_academic_session`) REFERENCES `academic_session` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-supervision-id_supervisee` FOREIGN KEY (`id_supervisee`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-supervision-id_supervisor` FOREIGN KEY (`id_supervisor`) REFERENCES `personal_detail` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
