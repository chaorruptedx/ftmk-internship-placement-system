<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\PersonalDetail;
use common\models\SupervisionAssignSearch;
use common\models\Supervision;

/**
 * SupervisionAssignController implements the CRUD actions for PersonalDetail model.
 */
class SupervisionAssignController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserCoordinator(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PersonalDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SupervisionAssignSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PersonalDetail model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_personal_detail)
    {
        $model = $this->findModel($id_personal_detail);

        $searchModel = new SupervisionAssignSearch();
        $searchModel->id_supervisor = $id_personal_detail;
        $dataProvider = $searchModel->searchStudent(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAssignStudent($id_personal_detail)
    {
        $model = $this->findModel($id_personal_detail);

        $searchModel = new SupervisionAssignSearch();
        $searchModel->id_supervisor = $id_personal_detail;
        $dataProvider = $searchModel->searchAssignStudent(Yii::$app->request->queryParams);

        return $this->render('assign-student', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateAssignStudent($id_personal_detail)
    {
        Supervision::saveAssignStudent($id_personal_detail, Yii::$app->request->post('id_personal_detail_students'));

        Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Student assign has been updated successfully.');

        return $this->redirect(['assign-student', 'id_personal_detail' => $id_personal_detail]);
    }

    /**
     * Finds the PersonalDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PersonalDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PersonalDetail::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
