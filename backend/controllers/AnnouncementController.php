<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Announcement;
use common\models\AnnouncementSearch;
use common\models\User;
use common\models\AnnouncementRole;

/**
 * AnnouncementController implements the CRUD actions for Announcement model.
 */
class AnnouncementController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Announcement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AnnouncementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Announcement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Announcement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Announcement();
        $modelAnnouncementRole = new AnnouncementRole();

        if ($model->load(Yii::$app->request->post()) && $modelAnnouncementRole->load(Yii::$app->request->post()) && $model->saveAnnouncement() && $modelAnnouncementRole->saveAnnouncementRole($model->id)) {
            Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Announcement <b>'.$model->title.'</b> has been created successfully.');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelAnnouncementRole' => $modelAnnouncementRole,
        ]);
    }

    /**
     * Updates an existing Announcement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAnnouncementRole = new AnnouncementRole();

        if (isset($model->announcementRoles) && is_array($model->announcementRoles))
        {
            foreach ($model->announcementRoles as $key => $value)
            {
                $modelAnnouncementRole->role_no_array[] = $value['role_no'];
            }
        }

        if ($model->load(Yii::$app->request->post()) && $modelAnnouncementRole->load(Yii::$app->request->post()) && $model->saveAnnouncement() && $modelAnnouncementRole->saveAnnouncementRole($model->id)) {
            Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Announcement <b>'.$model->title.'</b> has been updated successfully.');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelAnnouncementRole' => $modelAnnouncementRole,
        ]);
    }

    /**
     * Deletes an existing Announcement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $announcement_title = $this->findModel($id)->title;
        $this->findModel($id)->deleteAnnouncement();
        Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Announcement <b>'.$announcement_title.'</b> has been deleted.');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Announcement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Announcement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Announcement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
