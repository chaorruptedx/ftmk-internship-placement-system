<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\UserManagementSearch;
use backend\models\SignupForm;
use common\models\PersonalDetail;

/**
 * UserManagementController implements the CRUD actions for User model.
 */
class UserManagementController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (in_array($model->role_no, array('2', '3', '4')))
            $modelPersonalDetail = $model->personalDetail;
        else
            $modelPersonalDetail = new PersonalDetail();


        return $this->render('view', [
            'model' => $model,
            'modelPersonalDetail' => $modelPersonalDetail,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        $modelUser = new User();
        $modelPersonalDetail = new PersonalDetail();

        if ($model->load(Yii::$app->request->post()))
        {
            if (in_array($model->role_no, array(1, 5)) && $model->signup(Yii::$app->request->post())) // Administrator, Organization
            {
                $modelUser = $modelUser->findByUsernameAll($model->username);

                Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account <b>'.$modelUser->username.'</b> has been registered successfully.');

                return $this->redirect(['index']);
            }
            else if (in_array($model->role_no, array(2, 3))) // Coordinator, Supervisor
            {
                $modelPersonalDetail->scenario = PersonalDetail::SCENARIO_SAVE_COORDINATOR_SUPERVISOR;

                if ($modelPersonalDetail->load(Yii::$app->request->post()) && $modelPersonalDetail->validate() && $model->signup(Yii::$app->request->post()))
                {
                    $modelUser = $modelUser->findByUsernameAll($model->username);
                    $modelPersonalDetail->savePersonalDetail($modelUser->id, $modelUser->role_no);

                    Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account and personal details <b>'.$modelUser->username.'</b> has been registered successfully.');
                    
                    return $this->redirect(['index']);
                }
            }
            else if ($model->role_no == 4) // Student
            {
                $modelPersonalDetail->scenario = PersonalDetail::SCENARIO_SAVE_STUDENT;

                if ($modelPersonalDetail->load(Yii::$app->request->post()) && $modelPersonalDetail->validate() && $model->signup(Yii::$app->request->post()))
                {
                    $modelUser = $modelUser->findByUsernameAll($model->username);
                    $modelPersonalDetail->savePersonalDetail($modelUser->id, $modelUser->role_no);

                    Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account and personal details <b>'.$modelUser->username.'</b> has been registered successfully.');
                    
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelPersonalDetail' => $modelPersonalDetail,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelPersonalDetail = $model->personalDetail;
        if (empty($modelPersonalDetail))
            $modelPersonalDetail = new PersonalDetail();

        if ($model->load(Yii::$app->request->post()))
        {
            if (in_array($model->role_no, array(1, 5)) && $model->saveUser(Yii::$app->request->post())) // Administrator, Organization
            {
                Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account <b>'.$model->username.'</b> has been updated successfully.');
    
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else if (in_array($model->role_no, array(2, 3))) // Coordinator, Supervisor
            {
                $modelPersonalDetail->scenario = PersonalDetail::SCENARIO_SAVE_COORDINATOR_SUPERVISOR;

                if ($modelPersonalDetail->load(Yii::$app->request->post()) && $model->validate() && $modelPersonalDetail->validate())
                {
                    $model->saveUser(Yii::$app->request->post());
                    $modelPersonalDetail->savePersonalDetail($model->id, $model->role_no);

                    Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account and personal details <b>'.$model->username.'</b> has been updated successfully.');
                    
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            else if ($model->role_no == 4) // Student
            {
                $modelPersonalDetail->scenario = PersonalDetail::SCENARIO_SAVE_STUDENT;

                if ($modelPersonalDetail->load(Yii::$app->request->post()) && $model->validate() && $modelPersonalDetail->validate())
                {
                    $model->saveUser(Yii::$app->request->post());
                    $modelPersonalDetail->savePersonalDetail($model->id, $model->role_no);

                    Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account and personal details <b>'.$model->username.'</b> has been updated successfully.');
                    
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelPersonalDetail' => $modelPersonalDetail,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
        $username = $this->findModel($id)->username;
        $this->findModel($id)->deleteUser();
        Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account <b>'.$username.'</b> has been deleted.');
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
