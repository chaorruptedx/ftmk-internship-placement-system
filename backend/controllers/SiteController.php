<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\LoginForm;
use common\models\User;
use common\models\Announcement;
use common\models\PersonalDetail;
use common\models\LookupProgramme;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'login', 'login-student', 'account-management', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'account-management', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'logout' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (User::isUserAdmin(Yii::$app->user->identity->username))
        {
            $modelAnnouncements = Announcement::find()
                ->joinWith('announcementRoles')
                ->where([
                    'announcement_role.role_no' => '1', // Admin
                ])
                ->active()
                ->all();
            
            $modelUser = User::find()
                ->select([
                    'SUM(CASE
                        WHEN role_no = 1 THEN 1
                        ELSE 0
                    END) AS admin',
                    'SUM(CASE
                        WHEN role_no = 2 THEN 1
                        ELSE 0
                    END) AS coordinator',
                    'SUM(CASE
                        WHEN role_no = 3 THEN 1
                        ELSE 0
                    END) AS supervisor',
                    'SUM(CASE
                        WHEN role_no = 4 THEN 1
                        ELSE 0
                    END) AS student',
                    'SUM(CASE
                        WHEN role_no = 5 THEN 1
                        ELSE 0
                    END) AS organization',
                ])
                ->active()
                ->one();


            return $this->render('index', [
                'modelAnnouncements' => $modelAnnouncements,
                'modelUser' => $modelUser,
            ]);
        }
        elseif (User::isUserCoordinator(Yii::$app->user->identity->username))
        {
            $modelAnnouncements = Announcement::find()
                ->joinWith('announcementRoles')
                ->where([
                    'announcement_role.role_no' => '2', // Coordinator
                ])
                ->active()
                ->all();

            $student_supervision_status = PersonalDetail::find()
                ->select([
                    'SUM(CASE
                        WHEN supervision.id IS NULL THEN 1
                        ELSE 0
                    END) AS student_unsupervised',
                    'SUM(CASE
                        WHEN supervision.id_supervisor IS NOT NULL THEN 1
                        ELSE 0
                    END) AS student_supervised',
                ])
                ->joinWith(['user'])
                ->leftJoin('supervision', 'personal_detail.id = supervision.id_supervisee AND supervision.status = 1')
                ->where([
                    'user.role_no' => 4, // Student
                ])
                ->active()
                ->asArray()
                ->one();
            
                $student_internship_status = PersonalDetail::find()
                ->select([
                    'DISTINCT(internship_application.id_personal_detail)',
                    'CASE
                        WHEN MAX(internship_application.application_status) = 4 THEN "got_internship"
                        WHEN MAX(internship_application.application_status) = 1 OR MAX(internship_application.application_status) = 2 OR MAX(internship_application.application_status) = 3 THEN "pending"
                        WHEN MAX(internship_application.application_status) IS NULL OR MAX(internship_application.application_status) = 0 THEN "not_apply"
                    END AS student_status',
                ])
                ->joinWith(['supervisor'])
                ->leftJoin('personal_detail student_personal_detail', 'student_personal_detail.id = supervision.id_supervisee AND student_personal_detail.status = 1')
                ->leftJoin('internship_application', 'internship_application.id_personal_detail = student_personal_detail.id AND internship_application.status = 1')
                ->groupBy(['internship_application.id_personal_detail'])
                ->active()
                ->asArray()
                ->all();

            if (!empty($student_internship_status) && is_array($student_internship_status))
            {
                $not_apply = 0;
                $pending = 0;
                $got_internship = 0;
    
                foreach ($student_internship_status as $student_internship_status_data)
                {
                    if ($student_internship_status_data['student_status'] == 'not_apply')
                    {
                        $not_apply++;
                    }
                    else if ($student_internship_status_data['student_status'] == 'pending')
                    {
                        $pending++;
                    }
                    else if ($student_internship_status_data['student_status'] == 'got_internship')
                    {
                        $got_internship++;
                    }
                }
    
                $data_internship['not_apply'] = $not_apply;
                $data_internship['pending'] = $pending;
                $data_internship['got_internship'] = $got_internship;
            }

            $modelLookupProgrammes = LookupProgramme::find()->active()->all();
            $arrayLookupProgrammeCode = ArrayHelper::map($modelLookupProgrammes, 'id', 'code');

            if (!empty($modelLookupProgrammes) && is_array($modelLookupProgrammes))
            {
                foreach ($modelLookupProgrammes as $modelLookupProgramme)
                {
                    $query[] = "
                        SUM(CASE
                            WHEN lookup_programme.code = '".$modelLookupProgramme->code."' THEN 1
                            ELSE 0
                        END) AS ".$modelLookupProgramme->code."
                    ";
                }

                $get_student_under_programme = PersonalDetail::find()
                    ->select($query)
                    ->joinWith(['programme', 'user'])
                    ->where([
                        'user.role_no' => 4, // Student
                    ])
                    ->active()
                    ->asArray()
                    ->one();
            }

            if ($get_student_under_programme)
            {
                $student_under_programme['number_of_course'][] = 'Programme';

                foreach ($get_student_under_programme as $course_code => $number_of_course)
                {   
                    if (in_array($course_code, $arrayLookupProgrammeCode))
                    {
                        $student_under_programme['course_code'][] = $course_code;
                        $student_under_programme['number_of_course'][] = $number_of_course;
                    }
                }
            }
            else
            {
                $student_under_programme['course_code'][] = 'Programme';
                $student_under_programme['number_of_course'][] = 'Programme';
                $student_under_programme['number_of_course'][] = 0;
            }

            return $this->render('index', [
                'modelAnnouncements' => $modelAnnouncements,
                'student_supervision_status' => Json::encode($student_supervision_status),
                'student_internship_status' => Json::encode($data_internship),
                'student_under_programme' => Json::encode($student_under_programme),
            ]);
        }
        elseif (User::isUserSupervisor(Yii::$app->user->identity->username))
        {
            $modelAnnouncements = Announcement::find()
                ->joinWith('announcementRoles')
                ->where([
                    'announcement_role.role_no' => '3', // Supervisor
                ])
                ->active()
                ->all();

            $modelUser = User::findIdentity(Yii::$app->user->identity->id);

            $student_internship_status = PersonalDetail::find()
                ->select([
                    'DISTINCT(internship_application.id_personal_detail)',
                    'CASE
                        WHEN MAX(internship_application.application_status) = 4 THEN "got_internship"
                        WHEN MAX(internship_application.application_status) = 1 OR MAX(internship_application.application_status) = 2 OR MAX(internship_application.application_status) = 3 THEN "pending"
                        WHEN MAX(internship_application.application_status) IS NULL OR MAX(internship_application.application_status) = 0 THEN "not_apply"
                    END AS student_status',
                ])
                ->joinWith(['supervisor'])
                ->leftJoin('personal_detail student_personal_detail', 'student_personal_detail.id = supervision.id_supervisee AND student_personal_detail.status = 1')
                ->leftJoin('internship_application', 'internship_application.id_personal_detail = student_personal_detail.id AND internship_application.status = 1')
                ->where([
                    'personal_detail.id_user' => $modelUser->id,
                ])
                ->groupBy(['internship_application.id_personal_detail'])
                ->active()
                ->asArray()
                ->all();

            if (!empty($student_internship_status) && is_array($student_internship_status))
            {
                $not_apply = 0;
                $pending = 0;
                $got_internship = 0;
    
                foreach ($student_internship_status as $student_internship_status_data)
                {
                    if ($student_internship_status_data['student_status'] == 'not_apply')
                    {
                        $not_apply++;
                    }
                    else if ($student_internship_status_data['student_status'] == 'pending')
                    {
                        $pending++;
                    }
                    else if ($student_internship_status_data['student_status'] == 'got_internship')
                    {
                        $got_internship++;
                    }
                }
    
                $data_internship['not_apply'] = $not_apply;
                $data_internship['pending'] = $pending;
                $data_internship['got_internship'] = $got_internship;
            }

            return $this->render('index', [
                'modelAnnouncements' => $modelAnnouncements,
                'student_internship_status' => Json::encode($data_internship),
            ]);
        }
        else
        {
            return $this->render('index');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionAccountManagement()
    {
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);

        if ($modelUser->load(Yii::$app->request->post()))
        {
            $modelUser->saveUser(Yii::$app->request->post());

            Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>User account <b>'.$modelUser->username.'</b> has been updated successfully.');
        }

        return $this->render('account-management', [
            'modelUser' => $modelUser,
        ]);
    }

    public function actionLoginStudent()
    {
        return $this->redirect(str_replace('/admin', '/site/login', Url::base(true)));
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
