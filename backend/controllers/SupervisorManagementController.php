<?php

namespace backend\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Supervision;
use common\models\SupervisorManagementSearch;
use common\models\User;
use common\models\BankStatus;

/**
 * SupervisorManagementController implements the CRUD actions for Supervision model.
 */
class SupervisorManagementController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserCoordinator(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Supervision models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SupervisorManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Supervision model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Supervision model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Supervision();

        if ($model->load(Yii::$app->request->post()) && $model->saveSupervision()) {
            Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Supervisor <b>'.$model->supervisor->name.'</b> for session <b>'.BankStatus::getAcademicSession($model->academicSession->id).'</b> has been created successfully.');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Supervision model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveSupervision()) {
            Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Supervisor <b>'.$model->supervisor->name.'</b> for session <b>'.BankStatus::getAcademicSession($model->academicSession->id).'</b> has been updated successfully.');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Supervision model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->findModel($id)->deleteSupervision();
        Yii::$app->session->setFlash('success', '<span class="fas fa-check-circle">&nbsp;</span>Supervisor <b>'.$model->supervisor->name.'</b> for session <b>'.BankStatus::getAcademicSession($model->academicSession->id).'</b> has been deleted.');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Supervision model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Supervision the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Supervision::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
