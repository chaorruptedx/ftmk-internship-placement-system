<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LookupProgramme */

$this->title = Yii::t('app', 'Create Programme');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Programme Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-programme-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
