<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LookupProgramme */

$this->title = Yii::t('app', 'Update Programme: {code}', [
    'code' => $model->code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Programme Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lookup-programme-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
