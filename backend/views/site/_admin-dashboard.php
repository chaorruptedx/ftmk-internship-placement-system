<?php
use yii\helpers\Html;
use backend\assets\DashboardAdminAsset;

DashboardAdminAsset::register($this);
?>
<style>
    .cardcon {
    background-color: white;
    padding: 20px;
    margin-top: 20px;
    border-radius: 20px;
    }
</style>
<div class="admin-dashboard">
    <h1><?= Html::encode('Announcement') ?></h1>

    <div class="cardcon">
        <?php if (!empty($modelAnnouncements) && is_array($modelAnnouncements)) : ?>
            <ul>
                <?php foreach ($modelAnnouncements as $modelAnnouncement) : ?>
                    <h4><?= $modelAnnouncement->title; ?></h4>
                    <li><?= $modelAnnouncement->content; ?></li><hr>
                <?php endforeach ?>
            </ul>
        <?php else : ?>
            No announcement.
        <?php endif; ?>
    </div>

    <br>

    <h1><?= Html::encode('Dashboard') ?></h1>

    <br>

    <div class="card">
        <div class="card-body">
        
            <div class="row">

                <div class="col-sm-12">
                    <h3 class="text-center">Number of Users</h3>
                    <div class="row">

                        <div class="col-sm">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading red"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content red">
                                    <div class="circle-tile-description text-faded">Admin</div>
                                    <div class="circle-tile-number text-faded "><?= $modelUser->admin; ?></div>
                                    <a class="circle-tile-footer" href="<?//= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading orange"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content orange">
                                    <div class="circle-tile-description text-faded">Coordinator</div>
                                    <div class="circle-tile-number text-faded "><?= $modelUser->coordinator; ?></div>
                                    <a class="circle-tile-footer" href="<?//= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading dark-blue"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content dark-blue">
                                    <div class="circle-tile-description text-faded">Supervisor</div>
                                    <div class="circle-tile-number text-faded "><?= $modelUser->supervisor; ?></div>
                                    <a class="circle-tile-footer" href="<?//= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading green"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content green">
                                    <div class="circle-tile-description text-faded">Student</div>
                                    <div class="circle-tile-number text-faded "><?= $modelUser->student; ?></div>
                                    <a class="circle-tile-footer" href="<?//= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="circle-tile ">
                                <a href="#"><div class="circle-tile-heading purple"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                                <div class="circle-tile-content purple">
                                    <div class="circle-tile-description text-faded">Organization</div>
                                    <div class="circle-tile-number text-faded "><?= $modelUser->organization; ?></div>
                                    <a class="circle-tile-footer" href="<?//= base_url('public/admin/user_management');?>">More Info&nbsp;<i class="fa fa-chevron-circle-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

</div>