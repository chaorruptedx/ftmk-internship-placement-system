<?php
use yii\helpers\Html;
use backend\assets\DashboardSupervisorAsset;

DashboardSupervisorAsset::register($this);
?>
<style>
    .cardcon {
    background-color: white;
    padding: 20px;
    margin-top: 20px;
    border-radius: 20px;
    }
</style>
<div class="supervisor-dashboard">
    <h1><?= Html::encode('Announcement') ?></h1>

    <div class="cardcon">
        <?php if (!empty($modelAnnouncements) && is_array($modelAnnouncements)) : ?>
            <ul>
                <?php foreach ($modelAnnouncements as $modelAnnouncement) : ?>
                    <h4><?= $modelAnnouncement->title; ?></h4>
                    <li><?= $modelAnnouncement->content; ?></li><hr>
                <?php endforeach ?>
            </ul>
        <?php else : ?>
            No announcement.
        <?php endif; ?>
    </div>

    <br>

    <h1><?= Html::encode('Dashboard') ?></h1>

    <br>

    <div class="card">
        <div class="card-body">
        
            <div class="row">

                <div class="col-sm-12">
                    <br><br>
                    <h3 class="text-center">Student Internship Status</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="student-internship-status-bar"></div>
                        </div>
                        <div class="col-sm-6">
                            <div id="student-internship-status"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<?php
$script = <<< JS

var student_internship_status_bar_chart = c3.generate({
    bindto: '#student-internship-status-bar',
    data: {
        x : 'x',
        columns: [
            ['x', 'Internship Status'],
            ['data1', 0],
            ['data2', 0],
            ['data3', 0],
        ],
        names: {
            data1: 'Not Applied Yet',
            data2: 'In Progress',
            data3: 'Got Internship',
        },
        type: 'bar',
        colors: {
            data1: 'red',
            data2: 'orange',
            data3: 'green'
        },
        color: function (color, d) {
            // d will be 'id' when called for legends
            return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
        }
    },
    axis: {
        rotated: true,
        x: {
            type: 'category' // this needed to load string x value
        }
    }
});

var student_internship_status_chart = c3.generate({
    bindto: '#student-internship-status',
    data: {
        columns: [
            ['data1', 0],
            ['data2', 0],
            ['data3', 0],
        ],
        names: {
            data1: 'Not Applied Yet',
            data2: 'In Progress',
            data3: 'Got Internship',
        },
        type : 'pie',
        colors: {
            data1: 'red',
            data2: 'orange',
            data3: 'green'
        },
        color: function (color, d) {
            // d will be 'id' when called for legends
            return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
        }
    }
});

var student_internship_status_data = JSON.parse('$student_internship_status');

if (student_internship_status_data != null)
{
    setTimeout(function () {
        student_internship_status_bar_chart.load({
            columns: [
                ['data1', student_internship_status_data.not_apply],
                ['data2', student_internship_status_data.pending],
                ['data3', student_internship_status_data.got_internship],
            ],
        });
    }, 100);

    setTimeout(function () {
        student_internship_status_chart.load({
            columns: [
                ['data1', student_internship_status_data.not_apply],
                ['data2', student_internship_status_data.pending],
                ['data3', student_internship_status_data.got_internship],
            ],
        });
    }, 100);
}

var student_placement_state_chart = c3.generate({
bindto: '#student-placement-state',
data: {
    columns: [
        ['data1', 0],
        ['data2', 0],
    ],
    names: {
        data1: 'Got Internship',
        data2: 'In Progress',
    },
    type: 'bar',
    groups: [
        ['data1', 'data2']
    ],
    colors: {
        data1: 'green',
        data2: 'orange',
    },
    color: function (color, d) {
        // d will be 'id' when called for legends
        return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
    }
},
axis: {
    x: {
        type: 'category',
        categories: ['State']
    }
}
});

JS;
$this->registerJs($script);
?>