<?php
use common\models\User;
/* @var $this yii\web\View */

$this->title = 'FTMK Internship Placement System';
?>
<div class="site-index">
    <div class="body-content">

        <?php if (User::isUserAdmin(Yii::$app->user->identity->username)) : ?>
            <?= $this->render('_admin-dashboard', [
                'modelAnnouncements' => $modelAnnouncements,
                'modelUser' => $modelUser,
            ]); ?>
        <?php elseif (User::isUserCoordinator(Yii::$app->user->identity->username)) : ?>
            <?= $this->render('_coordinator-dashboard', [
                'modelAnnouncements' => $modelAnnouncements,
                'student_internship_status' => $student_internship_status,
                'student_supervision_status' => $student_supervision_status,
                'student_under_programme' => $student_under_programme,
            ]); ?>
        <?php elseif (User::isUserSupervisor(Yii::$app->user->identity->username)) : ?>
            <?= $this->render('_supervisor-dashboard', [
                'modelAnnouncements' => $modelAnnouncements,
                'student_internship_status' => $student_internship_status,
            ]); ?>
        <?php elseif (!Yii::$app->user->identity->username) : ?> <!-- Guest -->
            <h1 class="text-center">Welcome to FTMK Internship Placement System</h1>
        <?php endif ?>

    </div>
</div>
