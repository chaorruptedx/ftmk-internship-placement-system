<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\LookupState */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'State Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lookup-state-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/state-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-pencil-alt">&nbsp;</span>Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-trash-alt">&nbsp;</span>Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete <b>'.$model->name.'</b> state?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'type',
                'value' => Helper::getStateTypeName($model->type),
            ],
        ],
    ]) ?>

</div>
