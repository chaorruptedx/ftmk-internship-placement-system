<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\LookupState */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-state-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->widget(Select2::classname(), [
        'data' => Helper::listStateType(),
        'options' => ['placeholder' => 'Select a role ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/state-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
