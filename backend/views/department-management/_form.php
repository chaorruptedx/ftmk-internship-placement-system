<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\LookupDepartment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-department-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_faculty')->widget(Select2::classname(), [
        'data' => BankStatus::listFaculty(),
        'options' => [
            'disabled' => true,
            'placeholder' => 'Select faculty ...',
            'value' => 5,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/department-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
