<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\AcademicSession */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="academic-session-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'semester')->widget(Select2::classname(), [
        'data' => Helper::getSemester(),
        'options' => ['placeholder' => 'Select semester ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'start_year')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter start year ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'startView' => 'year',
            'minViewMode' => 'years',
            'format' => 'yyyy',
        ]
    ]); ?>

    <?= $form->field($model, 'end_year')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter end year ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'startView' => 'year',
            'minViewMode' => 'years',
            'format' => 'yyyy',
        ]
    ]); ?>

    <?= $form->field($model, 'start_internship_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter start internship date ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

<?= $form->field($model, 'end_internship_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter end internship date ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => Helper::listStatus(),
        'options' => ['placeholder' => 'Select status ...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/academic-session-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
