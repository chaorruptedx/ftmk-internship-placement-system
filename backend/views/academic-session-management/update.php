<?php

use yii\helpers\Html;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\AcademicSession */

$this->title = Yii::t('app', 'Update Academic Session: {name}', [
    'name' => BankStatus::getAcademicSession($model->id),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Academic Sessions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => BankStatus::getAcademicSession($model->id), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="academic-session-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
