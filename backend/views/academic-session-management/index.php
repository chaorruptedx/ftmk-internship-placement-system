<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use common\components\Helper;
use common\models\BankStatus;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AcademicSessionManagementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Academic Session Management');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-session-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-plus">&nbsp;</span>Create Academic Session'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'semester',
            'start_year',
            'end_year',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return Helper::getStatusName($model->status);
                },
                'filter' => Helper::listStatus(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete <b>'.BankStatus::getAcademicSession($model->id).'</b> academic session?'),
                            'data-method' => 'post',
                        ];
                        return Html::a('<span class="fas fa-trash-alt">&nbsp;</span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
