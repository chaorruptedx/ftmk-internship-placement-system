<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AcademicSession */

$this->title = Yii::t('app', 'Create Academic Session');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Academic Session Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-session-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
