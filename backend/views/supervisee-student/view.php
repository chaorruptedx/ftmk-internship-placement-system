<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\Supervision */

$this->title = $model->supervisee->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Student List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="supervision-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/supervisee-student'], ['class' => 'btn btn-secondary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Name',
                'value' => $model->supervisee->name,
            ],
            [
                'label' => 'NRIC No.',
                'value' => $model->supervisee->nric_no,
            ],
            [
                'label' => 'Matric No.',
                'value' => $model->supervisee->user_no,
            ],
            [
                'label' => 'Gender',
                'value' => Helper::getGender($model->supervisee->gender_no),
            ],
            [
                'label' => 'Religion',
                'value' => $model->supervisee->religion,
            ],
            [
                'label' => 'Tel. No.',
                'value' => $model->supervisee->tel_no,
            ],
            [
                'label' => 'Faculty',
                'value' => $model->supervisee->programme->department->faculty->code.' - '.$model->supervisee->programme->department->faculty->name,
            ],
            [
                'label' => 'Department',
                'value' => $model->supervisee->programme->department->code.' - '.$model->supervisee->programme->department->name,
            ],
            [
                'label' => 'Programme',
                'value' => $model->supervisee->programme->code.' - '.$model->supervisee->programme->name,
            ],
        ],
    ]) ?>

</div>
