<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use common\components\Helper;
use common\models\InternshipApplication;
use common\models\BankStatus;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SuperviseeStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Student List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supervision-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'supervisee.name',
            [
                'label' => 'Matric No.',
                'attribute' => 'supervisee.user_no',
            ],
            [
                'attribute' => 'supervisee.programme.code',
                'filter' => BankStatus::listProgramme(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            'supervisee.user.email',
            'supervisee.tel_no',
            [
                'mergeHeader' => true,
                'label' => 'Application Status',
                'value' => function ($model)
                {
                    $modelInternshipApplication = InternshipApplication::find()
                        ->select([
                            'MAX(application_status) AS application_status',
                        ])
                        ->where([
                            'id_personal_detail' => $model->supervisee->id,
                        ])
                        ->active()
                        ->asArray()
                        ->one();

                    return Helper::getApplicationStatusStudent($modelInternshipApplication['application_status']);
                },
                'contentOptions' => function ($model, $key, $index, $column) {

                    $modelInternshipApplication = InternshipApplication::find()
                        ->select([
                            'MAX(application_status) AS application_status',
                        ])
                        ->where([
                            'id_personal_detail' => $model->supervisee->id,
                        ])
                        ->active()
                        ->asArray()
                        ->one();

                    if ($modelInternshipApplication['application_status'] == 0)
                        $color = 'danger';
                    elseif ($modelInternshipApplication['application_status'] == 1)
                        $color = 'info';
                    elseif ($modelInternshipApplication['application_status'] == 2)
                        $color = 'warning';
                    elseif ($modelInternshipApplication['application_status'] == 3)
                        $color = 'primary';
                    elseif ($modelInternshipApplication['application_status'] == 4)
                        $color = 'success';

                    return [
                        'class' => 'text-' . $color
                    ];
                },
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <div class="card">
        <div class="card-body">
            <h3>Status Flow Note :</h3>                 
            <p class="text-center"><span class="text-info">Applied</span> <span class="fas fa-angle-right"></span> <span class="text-warning">In Review</span> <span class="fas fa-angle-right"></span> <span class="text-primary">Waiting for Student Approval</span> <span class="fas fa-angle-right"></span> <span class="text-success">Student Accepted</span></p>
        </div>
    </div>

</div>
