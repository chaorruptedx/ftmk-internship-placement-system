<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use common\widgets\Alert;
use common\models\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <style>
		body {
			background-image: url("<?= Url::to('@web/image/background.jpg'); ?>");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
	</style>
    <?php $this->head() ?>
</head>
<body style="background-color: whitesmoke;" onload="startTime()">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-lg fixed-top navbar-dark bg-primary',
        ],
    ]);
    $menuItems[] = ['label' => '<span class="fas fa-home">&nbsp;</span>Home', 'url' => ['/site/index']];
    if (User::isUserAdmin(Yii::$app->user->identity->username))
    {
        $menuItems[] = [
            'label' => '<span class="fas fa-list">&nbsp;</span>Management',
            'options' => ['class' => 'dropdown'],
            'items' => [
                ['label' => '<span class="fas fa-user">&nbsp;</span>User Management', 'url' => ['/user-management/index']],
                // ['label' => '<span class="fas fa-building">&nbsp;</span>Faculty Management', 'url' => ['/faculty-management/index'], 'disabled' => 'disabled'], // System Focus on FTMK only
                ['label' => '<span class="fas fa-building">&nbsp;</span>Department Management', 'url' => ['/department-management/index']],
                ['label' => '<span class="fas fa-graduation-cap">&nbsp;</span>Programme Management', 'url' => ['/programme-management/index']],
                ['label' => '<span class="fas fa-globe-asia">&nbsp;</span>State Management', 'url' => ['/state-management/index']],
                ['label' => '<span class="fas fa-language">&nbsp;</span>Language Management', 'url' => ['/language-management/index']],
                ['label' => '<span class="fas fa-calendar-alt">&nbsp;</span>Session Management', 'url' => ['/academic-session-management/index']],
        ]];
        $menuItems[] = ['label' => '<span class="fas fa-bullhorn"></span>&nbsp;Announcement', 'url' => ['/announcement/index']];
    }
    elseif ((User::isUserCoordinator(Yii::$app->user->identity->username)))
    {
        // $menuItems[] = ['label' => '<span class="fas fa-users"></span>&nbsp;Supervisor Management', 'url' => ['/supervisor-management/index']];
        $menuItems[] = ['label' => '<span class="fas fa-users-cog"></span>&nbsp;Assign Supervision', 'url' => ['/supervision-assign/index']];
    }
    elseif ((User::isUserSupervisor(Yii::$app->user->identity->username)))
    {
        // $menuItems[] = ['label' => '<span class="fas fa-map-marker-alt"></span>&nbsp;State Application', 'url' => ['/state-application/index']];
        $menuItems[] = ['label' => '<span class="fas fa-users"></span>&nbsp;Student List', 'url' => ['/supervisee-student/index']];
    }
    if (Yii::$app->user->isGuest) {
        $menuItemsAuth[] = ['label' => '<span class="fas fa-user">&nbsp;</span>User Login', 'url' => ['/site/login-student']];
        $menuItemsAuth[] = ['label' => '<span class="fas fa-user-tie">&nbsp;</span>Staff Login', 'url' => ['/site/login']];
    } else {
        $menuItemsAuth[] = [
            'label' => '<span class="fas fa-user-circle">&nbsp;</span>Hi, ' .Yii::$app->user->identity->username,
            'options' => ['class' => 'dropdown'],
            'items' => [
                ['label' => '<span class="fas fa-user">&nbsp;</span>Account Management', 'url' => ['/site/account-management']],
                ['label' => '<span class="fas fa-sign-out-alt">&nbsp;</span>Logout', 'url' => ['/site/logout']],
        ]];
        // $menuItemsAuth[] = '<li>'
        //     . Html::beginForm(['/site/logout'], 'post')
        //     . Html::submitButton(
        //         '<span class="fas fa-sign-out-alt">&nbsp;</span>Logout (' . Yii::$app->user->identity->username . ')',
        //         ['class' => 'btn btn-outline-light my-2 my-sm-0']
        //     )
        //     . Html::endForm()
        //     . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'activateParents' => true,
        'encodeLabels' => false,
        'items' => $menuItems,
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ml-auto'],
        'activateParents' => true,
        'encodeLabels' => false,
        'items' => $menuItemsAuth,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="float-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="float-right"><span id="autotime"></span></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
