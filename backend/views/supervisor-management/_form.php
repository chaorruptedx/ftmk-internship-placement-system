<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\Supervision */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="supervision-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_supervisor')->widget(Select2::classname(), [
        'data' => BankStatus::listSupervisor(),
        'options' => [
            'placeholder' => 'Select supervisor ...',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'id_academic_session')->widget(Select2::classname(), [
        'data' => BankStatus::listAcademicSession(),
        'options' => [
            'placeholder' => 'Select academic session ...',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/supervisor-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
