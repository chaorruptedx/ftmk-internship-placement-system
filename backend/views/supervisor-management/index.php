<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use common\components\Helper;
use common\models\BankStatus;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SupervisorManagementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Supervisor Management');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supervision-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-user-plus">&nbsp;</span>Create Supervisor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'supervisor.name',
            [
                'attribute' => 'id_academic_session',
                'value' => function ($model) {
                    return BankStatus::getAcademicSession($model->id_academic_session);
                },
                'filter' => BankStatus::listAcademicSession(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete supervisor <b>'.$model->supervisor->name.'</b> for session <b>'.BankStatus::getAcademicSession($model->academicSession->id).'</b>?'),
                            'data-method' => 'post',
                        ];
                        return Html::a('<span class="fas fa-trash-alt">&nbsp;</span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
