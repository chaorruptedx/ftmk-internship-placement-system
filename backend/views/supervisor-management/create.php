<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Supervision */

$this->title = Yii::t('app', 'Create Supervisor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supervisor Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supervision-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
