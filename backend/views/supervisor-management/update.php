<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Supervision */

$this->title = Yii::t('app', 'Update Supervisor: {name}', [
    'name' => $model->supervisor->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supervisor Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->supervisor->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="supervision-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
