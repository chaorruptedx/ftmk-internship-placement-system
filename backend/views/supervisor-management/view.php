<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\Supervision */

$this->title = $model->supervisor->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supervisor Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="supervision-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/supervisor-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-pencil-alt">&nbsp;</span>Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-trash-alt">&nbsp;</span>Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete supervisor <b>'.$model->supervisor->name.'</b> for session <b>'.BankStatus::getAcademicSession($model->academicSession->id).'</b>?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Faculty Supervisor',
                'attribute' => 'id_supervisor',
                'value' => $model->supervisor->name,
            ],
            [
                'label' => 'Academic Session',
                'attribute' => 'id_academic_session',
                'value' => BankStatus::getAcademicSession($model->academicSession->id),
            ],
        ],
    ]) ?>

</div>
