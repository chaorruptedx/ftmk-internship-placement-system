<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use common\models\BankStatus;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SupervisionAssignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Assign Supervision');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-detail-index">

    <h1><?= Html::encode('List of Supervisor') ?></h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'programme.code',
                'filter' => BankStatus::listProgramme(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            'user.email',
            'tel_no',

            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {

                        $url = Url::to(['view', 'id_personal_detail' => $key]);

                        return Html::a('<span class="fas fa-users">&nbsp;</span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
