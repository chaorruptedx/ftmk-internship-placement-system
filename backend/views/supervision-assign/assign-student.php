<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\Button;
use kartik\grid\GridView;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\PersonalDetail */

$this->title = Yii::t('app', 'Assign Student');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assign Supervision'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id_personal_detail' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="personal-detail-view">

    <?= $this->render('_supervisor-details', ['model' => $model]); ?>

    <br>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['view', 'id_personal_detail' => $model->id], ['class' => 'btn btn-secondary']) ?>
    </p>

    <hr>

    <h1 class="text-center"><?= Html::encode('Assign Student') ?></h1>

    <h2><?= Html::encode('List of All Students') ?></h2>

    <div class="row">
        <div class="col-md-6">
            <span class="border bg-success" style="font-size: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Assigned to Current Supervisor
            <br>
            <span class="border bg-secondary" style="font-size: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Assigned to Other Supervisor
            <br>
            <span class="border bg-danger" style="font-size: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;No Internship Placement Yet
        </div>
        <div class="col-md-6">
            <p class="text-right">
                <?= Button::widget([
                    'label' => '<span class="fas fa-check">&nbsp;</span>Update Student',
                    'encodeLabel' => false,
                    'options' => [
                        'id' => 'update-assign-student',
                        'class' => 'btn btn-success',
                    ],
                ]); ?>
            </p>
        </div>
    </div>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'id' => 'grid-student-list',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'striped' => false,
        'rowOptions' => function ($modelPersonalDetailStudent)
        {
            if ($modelPersonalDetailStudent->student_status == "no_placement_yet")
            {
                return [
                    'class' => 'table-danger',
                ];
            }
            else if ($modelPersonalDetailStudent->student_status == "other_supervisor")
            {
                return [
                    'class' => 'table-secondary',
                ];
            }
        },
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'name',
            [
                'label' => 'Matric No.',
                'attribute' => 'user_no',
            ],
            [
                'attribute' => 'programme.code',
                'filter' => BankStatus::listProgramme(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            'tel_no',
            [
                'label' => 'Supervisor',
                'attribute' => 'supervisee.supervisor.name',
            ],
            [
                'class' => '\kartik\grid\CheckboxColumn',
                'rowHighlight' => true,
                'rowSelectedClass' => 'table-success',
                'checkboxOptions' => function ($modelPersonalDetailStudent) use ($model)
                {   //d($modelPersonalDetailStudent);
                    if ($modelPersonalDetailStudent->student_status == "no_placement_yet")
                    {
                        return [
                            'value' => $model->id,
                            'disabled' => true,
                        ];
                    }
                    else if ($modelPersonalDetailStudent->student_status == "current_supervisor")
                    {
                        return [
                            'value' => $model->id,
                            'checked' => true,
                        ];
                    }
                    else if ($modelPersonalDetailStudent->student_status == "other_supervisor")
                    {
                        return [
                            'value' => $model->id,
                            'disabled' => true,
                        ];
                    }
                    else
                    {
                        return [
                            'value' => $model->id
                        ];
                    }
                }, 
            ]

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php
$url = Url::to(['update-assign-student', 'id_personal_detail' => $model->id]);

$script = <<< JS

$( "#update-assign-student" ).on( "click", function() {

    var id_personal_detail_students = $('#grid-student-list').yiiGridView('getSelectedRows');
    
    $.ajax({
        method: "POST",
        url: "$url",
        data: { 
            id_personal_detail_students: id_personal_detail_students
        }
    });
});

JS;
$this->registerJs($script);
?>