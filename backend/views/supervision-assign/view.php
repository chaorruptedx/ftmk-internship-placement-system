<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\PersonalDetail */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assign Supervision'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="personal-detail-view">

    <?= $this->render('_supervisor-details', ['model' => $model]); ?>

    <br>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/supervision-assign'], ['class' => 'btn btn-secondary']) ?>
    </p>

    <hr>

    <h1><?= Html::encode('List of Students under Supervisor') ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-user-plus">&nbsp;</span>Assign New Student'), ['assign-student', 'id_personal_detail' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'name',
            [
                'label' => 'Matric No.',
                'attribute' => 'user_no',
            ],
            [
                'attribute' => 'programme.code',
                'filter' => BankStatus::listProgramme(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            'tel_no',

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
