<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
?>
<div class="personal-detail-supervisor-details">

    <h1><?= Html::encode("Supervisor Details") ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'label' => 'Email',
                'attribute' => 'id_user',
                'value' => $model->user->email,
            ],
            'tel_no',
            [
                'label' => 'Faculty',
                'value' => $model->programme->department->faculty->code.' - '.$model->programme->department->faculty->name,
            ],
            [
                'label' => 'Department',
                'value' => $model->programme->department->code.' - '.$model->programme->department->name,
            ],
            [
                'label' => 'Programme',
                'value' => $model->programme->code.' - '.$model->programme->name,
            ],
        ],
    ]) ?>

</div>