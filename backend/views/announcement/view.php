<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use common\models\BankStatus;

/* @var $this yii\web\View */
/* @var $model common\models\Announcement */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Announcement'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="announcement-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/announcement'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-pencil-alt">&nbsp;</span>Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-trash-alt">&nbsp;</span>Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete <b>'.$model->title.'</b> announcement?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'content:ntext',
            [
                'label' => 'Role Name',
                'value' => BankStatus::getRoleNamesByIDAnnouncementDetailView($model->id),
                'format' => 'html',
            ],
        ],
    ]) ?>

</div>
