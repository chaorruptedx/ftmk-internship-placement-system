<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\FacultyManagementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Faculty Management');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-faculty-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-plus">&nbsp;</span>Create Faculty'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'code',
            'name',

            [
                'class' => 'kartik\grid\ActionColumn',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete <b>'.$model->code.'</b> faculty?'),
                            'data-method' => 'post',
                        ];
                        return Html::a('<span class="fas fa-trash-alt">&nbsp;</span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
