<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use common\components\Helper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserManagementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Management');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-user-plus">&nbsp;</span>Register User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'username',
            'email:email',
            [
                'attribute' => 'role_no',
                'value' => function ($model) {
                    return Helper::getRoleName($model->role_no);
                },
                'filter' => Helper::listRole(),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'data-confirm' => Yii::t('app', 'Are you sure you want to remove <b>'.$model->username.'</b> user?'),
                            'data-method' => 'post',
                        ];
                        return Html::a('<span class="fas fa-trash-alt">&nbsp;</span>', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
