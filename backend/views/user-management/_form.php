<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use common\components\Helper;
use common\models\BankStatus;

$list_role = Helper::listRole();

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'role_no')->widget(Select2::classname(), [
        'data' => $list_role,
        'options' => [
            'id' => 'role-no',
            'placeholder' => 'Select a role ...',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div id="nric-no">
        <?= $form->field($modelPersonalDetail, 'nric_no') ?>
    </div>

    <div id="user-no">
        <?= $form->field($modelPersonalDetail, 'user_no')->label('User No.') ?>
    </div>

    <div id="name">
        <?= $form->field($modelPersonalDetail, 'name') ?>
    </div>

    <div id="gender-no">
        <?= $form->field($modelPersonalDetail, 'gender_no')->widget(Select2::classname(), [
            'data' => Helper::listGender(),
            'options' => ['placeholder' => 'Select gender ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div id="religion">
        <?= $form->field($modelPersonalDetail, 'religion') ?>
    </div>

    <div id="tel-no">
        <?= $form->field($modelPersonalDetail, 'tel_no') ?>
    </div>

    <div id="id-programme">
        <?= $form->field($modelPersonalDetail, 'id_programme')->widget(Select2::classname(), [
            'data' => BankStatus::listProgramme(),
            'options' => ['placeholder' => 'Select programme ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div id="address">
        <?= $form->field($modelPersonalDetail, 'address')->textarea(['rows' => 6]) ?>
    </div>

    <div class="form-group">
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/user-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::submitButton(Yii::t('app', '<span class="fas fa-save">&nbsp;</span>Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

function showPersonalDetailByRole()
{
    if ($( "#role-no" ).val() == 1) // Administrator
    {
        $( "div#nric-no" ).hide();
        $( "div#user-no" ).hide();
        $( "div#user-no" ).find( "label" ).html("User No.");
        $( "div#name" ).hide();
        $( "div#gender-no" ).hide();
        $( "div#religion" ).hide();
        $( "div#tel-no" ).hide();
        $( "div#id-programme" ).hide();
        $( "div#address" ).hide();
    }
    else if ($( "#role-no" ).val() == 2 || $( "#role-no" ).val() == 3) // Coordinator || Supervisor
    {
        $( "div#nric-no" ).hide();
        $( "div#user-no" ).show();
        $( "div#user-no" ).find( "label" ).html("Staff No.");
        $( "div#name" ).show();
        $( "div#gender-no" ).show();
        $( "div#religion" ).hide();
        $( "div#tel-no" ).show();
        $( "div#id-programme" ).show();
        $( "div#address" ).hide();
    }
    else if ($( "#role-no" ).val() == 4) // Student
    {
        $( "div#nric-no" ).show();
        $( "div#user-no" ).show();
        $( "div#user-no" ).find( "label" ).html("Matric No.");
        $( "div#name" ).show();
        $( "div#gender-no" ).show();
        $( "div#religion" ).show();
        $( "div#tel-no" ).show();
        $( "div#id-programme" ).show();
        $( "div#address" ).show();
    }
    else if ($( "#role-no" ).val() == 5) // Organization
    {
        $( "div#nric-no" ).hide();
        $( "div#user-no" ).hide();
        $( "div#user-no" ).find( "label" ).html("User No.");
        $( "div#name" ).hide();
        $( "div#gender-no" ).hide();
        $( "div#religion" ).hide();
        $( "div#tel-no" ).hide();
        $( "div#id-programme" ).hide();
        $( "div#address" ).hide();
    }
    else if ($( "#role-no" ).val() == "") // Empty
    {
        $( "div#nric-no" ).hide();
        $( "div#user-no" ).hide();
        $( "div#user-no" ).find( "label" ).html("User No.");
        $( "div#name" ).hide();
        $( "div#gender-no" ).hide();
        $( "div#religion" ).hide();
        $( "div#tel-no" ).hide();
        $( "div#id-programme" ).hide();
        $( "div#address" ).hide();
    }
}

showPersonalDetailByRole(); // Initialize

$( "#role-no" ).change(function() {
    showPersonalDetailByRole();
});

JS;
$this->registerJs($script);
?>