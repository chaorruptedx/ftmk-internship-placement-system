<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use common\components\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<span class="fas fa-arrow-left">&nbsp;</span>Back'), ['/user-management'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-pencil-alt">&nbsp;</span>Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', '<span class="fas fa-trash-alt">&nbsp;</span>Remove'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to remove <b>'.$model->username.'</b> user?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            [
                'attribute' => 'role_no',
                'value' => Helper::getRoleName($model->role_no),
            ],
        ],
    ]) ?>

    <?php if (in_array($modelPersonalDetail, array('2', '3'))) : ?>
        <?= DetailView::widget([
            'model' => $modelPersonalDetail,
            'attributes' => [
                'user_no',
                'name',
                'gender',
                'tel_no',
                'id_programme',
            ],
        ]) ?>
    <?php endif; ?>

</div>
