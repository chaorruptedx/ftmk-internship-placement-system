<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class DashboardCoordinatorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/c3/c3.min.css',
    ];
    public $js = [
        'js/d3/d3.min.js',
        'js/c3/c3.min.js'
    ];
    public $depends = [
    ];
}
