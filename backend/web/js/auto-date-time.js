function startTime()
{
    var today = new Date();
    var year = today.getFullYear();
    var month = today.toLocaleString('en-us', {  month: 'long' })
    var date = today.getDate();
    var day = today.toLocaleString('en-us', {  weekday: 'long' });
    var hour = today.getHours();
    var minute = today.getMinutes();
    var second = today.getSeconds();
    var meridiem = hour >= 12 ? 'PM' : 'AM';

    hour = hour % 12;
    hour = hour ? hour : 12; // the hour '0' should be '12
    minute = checkTime(minute);
    second = checkTime(second);

    document.getElementById('autotime').innerHTML = day + ", " + date + " " + month + " " + year + ", " + hour + ":" + minute + ":" + second + " " + meridiem;

    var t = setTimeout(startTime, 500);
}

function checkTime(i)
{
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}