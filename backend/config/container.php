<?php
use kartik\grid\GridView;

return [
    Yii::$container->set('kartik\grid\GridView', [
        'floatHeader' => true,
        'panel' => [
            'type' => GridView::TYPE_DEFAULT
        ],
        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'enablePushState' => false,
            ]
        ],
    ]),
    Yii::$container->set('kartik\detail\DetailView', [
        'hover' => true,
    ])
];