<?php
return [
    'adminEmail' => 'admin@example.com',
    'bsVersion' => '4.x', // This will set globally `bsVersion` to Bootstrap 4.x for all Krajee Extensions
];
