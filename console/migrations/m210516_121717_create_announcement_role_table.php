<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%announcement_role}}`.
 */
class m210516_121717_create_announcement_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%announcement_role}}', [
            'id' => $this->primaryKey(),
            'id_announcement' => $this->integer()->notNull(),
            'role_no' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%announcement_role}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-announcement_role-id_announcement',
            'announcement_role',
            'id_announcement'
        );

        $this->addForeignKey(
            'fk-announcement_role-id_announcement',
            'announcement_role',
            'id_announcement',
            'announcement',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-announcement_role-id_announcement',
            'announcement'
        );

        $this->dropIndex(
            'fk-announcement_role-id_announcement',
            'announcement'
        );

        $this->dropTable('{{%announcement_role}}');
    }
}
