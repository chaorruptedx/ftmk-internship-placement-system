<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%skill}}`.
 */
class m210527_035350_create_skill_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%skill}}', [
            'id' => $this->primaryKey(),
            'id_personal_detail' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'scale' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%skill}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-skill-id_personal_detail',
            'skill',
            'id_personal_detail'
        );

        $this->addForeignKey(
            'fk-skill-id_personal_detail',
            'skill',
            'id_personal_detail',
            'personal_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-skill-id_personal_detail',
            'skill'
        );

        $this->dropIndex(
            'fk-skill-id_personal_detail',
            'skill'
        );

        $this->dropTable('{{%skill}}');
    }
}
