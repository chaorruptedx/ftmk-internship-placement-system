<?php

use yii\db\Migration;

/**
 * Class m210417_120352_configure_index_for_user_table
 */
class m210417_120352_configure_index_for_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex(
            'username',
            '{{%user}}'
        );

        $this->dropIndex(
            'email',
            '{{%user}}'
        );

        $this->dropIndex(
            'password_reset_token',
            '{{%user}}'
        );

        $this->createIndex(
            'uk-user-username-status-deleted_at',
            '{{%user}}',
            [
                'username',
                'status',
                'deleted_at'
            ],
            true
        );

        $this->createIndex(
            'uk-user-email-status-deleted_at',
            '{{%user}}',
            [
                'email',
                'status',
                'deleted_at'
            ],
            true
        );

        $this->createIndex(
            'uk-user-password_reset_token-status-deleted_at',
            '{{%user}}',
            [
                'password_reset_token',
                'status',
                'deleted_at'
            ],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210417_120352_configure_index_for_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210417_120352_configure_index_for_user_table cannot be reverted.\n";

        return false;
    }
    */
}
