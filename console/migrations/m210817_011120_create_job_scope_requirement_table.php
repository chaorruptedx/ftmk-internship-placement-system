<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%job_scope_requirement}}`.
 */
class m210817_011120_create_job_scope_requirement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%job_scope_requirement}}', [
            'id' => $this->primaryKey(),
            'id_programme' => $this->integer()->notNull(),
            'id_organization_detail' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%job_scope_requirement}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-job_scope_requirement-id_programme',
            'job_scope_requirement',
            'id_programme'
        );

        $this->addForeignKey(
            'fk-job_scope_requirement-id_programme',
            'job_scope_requirement',
            'id_programme',
            'lookup_programme',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-job_scope_requirement-id_organization_detail',
            'job_scope_requirement',
            'id_organization_detail'
        );

        $this->addForeignKey(
            'fk-job_scope_requirement-id_organization_detail',
            'job_scope_requirement',
            'id_organization_detail',
            'organization_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-job_scope_requirement-id_programme',
            'job_scope_requirement'
        );

        $this->dropIndex(
            'fk-job_scope_requirement-id_programme',
            'job_scope_requirement'
        );

        $this->dropForeignKey(
            'fk-job_scope_requirement-id_organization_detail',
            'job_scope_requirement'
        );

        $this->dropIndex(
            'fk-job_scope_requirement-id_organization_detail',
            'job_scope_requirement'
        );

        $this->dropTable('{{%job_scope_requirement}}');
    }
}
