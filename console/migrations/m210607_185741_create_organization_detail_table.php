<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%organization_detail}}`.
 */
class m210607_185741_create_organization_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%organization_detail}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_state' => $this->integer()->notNull(),
            'id_attachment' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'address' => $this->string()->notNull(),
            'tel_no' => $this->integer()->notNull(),
            'start_day' => $this->smallInteger()->notNull(),
            'end_day' => $this->smallInteger()->notNull(),
            'open_hour' => $this->time()->notNull(),
            'close_hour' => $this->time()->notNull(),
            'job_description' => $this->text()->notNull(),
            'accepting_internship' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%organization_detail}}', 'type', '1 = Government, 2 = Private');
        $this->addCommentOnColumn('{{%organization_detail}}', 'start_day', '0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday, 5 = Saturday, 6 = Sunday');
        $this->addCommentOnColumn('{{%organization_detail}}', 'end_day', '0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday, 5 = Saturday, 6 = Sunday');
        $this->addCommentOnColumn('{{%organization_detail}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-organization_detail-id_user',
            'organization_detail',
            'id_user'
        );

        $this->addForeignKey(
            'fk-organization_detail-id_user',
            'organization_detail',
            'id_user',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-organization_detail-id_state',
            'organization_detail',
            'id_state'
        );

        $this->addForeignKey(
            'fk-organization_detail-id_state',
            'organization_detail',
            'id_state',
            'lookup_state',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-organization_detail-id_attachment',
            'organization_detail',
            'id_attachment'
        );

        $this->addForeignKey(
            'fk-organization_detail-id_attachment',
            'organization_detail',
            'id_attachment',
            'attachment',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-organization_detail-id_attachment',
            'organization_detail'
        );

        $this->dropIndex(
            'fk-organization_detail-id_attachment',
            'organization_detail'
        );

        $this->dropForeignKey(
            'fk-organization_detail-id_state',
            'organization_detail'
        );

        $this->dropIndex(
            'fk-organization_detail-id_state',
            'organization_detail'
        );

        $this->dropForeignKey(
            'fk-organization_detail-id_user',
            'organization_detail'
        );

        $this->dropIndex(
            'fk-organization_detail-id_user',
            'organization_detail'
        );

        $this->dropTable('{{%organization_detail}}');
    }
}
