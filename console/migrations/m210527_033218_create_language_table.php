<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%language}}`.
 */
class m210527_033218_create_language_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%language}}', [
            'id' => $this->primaryKey(),
            'id_personal_detail' => $this->integer()->notNull(),
            'id_language' => $this->integer()->notNull(),
            'scale' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%language}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-language-id_personal_detail',
            'language',
            'id_personal_detail'
        );

        $this->addForeignKey(
            'fk-language-id_personal_detail',
            'language',
            'id_personal_detail',
            'personal_detail',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-language-id_language',
            'language',
            'id_language'
        );

        $this->addForeignKey(
            'fk-language-id_language',
            'language',
            'id_language',
            'lookup_language',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-language-id_language',
            'language'
        );

        $this->dropIndex(
            'fk-language-id_language',
            'language'
        );

        $this->dropForeignKey(
            'fk-language-id_personal_detail',
            'language'
        );

        $this->dropIndex(
            'fk-language-id_personal_detail',
            'language'
        );

        $this->dropTable('{{%language}}');
    }
}
