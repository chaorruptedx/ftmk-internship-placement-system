<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%education}}`.
 */
class m210527_034625_create_education_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%education}}', [
            'id' => $this->primaryKey(),
            'id_personal_detail' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'start_year' => $this->integer()->notNull(),
            'end_year' => $this->integer()->notNull(),
            'description' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%education}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-education-id_personal_detail',
            'education',
            'id_personal_detail'
        );

        $this->addForeignKey(
            'fk-education-id_personal_detail',
            'education',
            'id_personal_detail',
            'personal_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-education-id_personal_detail',
            'education'
        );

        $this->dropIndex(
            'fk-education-id_personal_detail',
            'education'
        );

        $this->dropTable('{{%education}}');
    }
}
