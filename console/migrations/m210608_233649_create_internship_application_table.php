<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%internship_application}}`.
 */
class m210608_233649_create_internship_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%internship_application}}', [
            'id' => $this->primaryKey(),
            'id_personal_detail' => $this->integer()->notNull(),
            'id_organization_detail' => $this->integer()->notNull(),
            'id_academic_session' => $this->integer()->notNull(),
            'application_status' => $this->smallInteger()->notNull(),
            'notes' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%internship_application}}', 'application_status', '0 = Reject, 1 = Pending, 2 = In Review, 3 = Organization Accept, 4 = Student Accept');
        $this->addCommentOnColumn('{{%internship_application}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-internship_application-id_personal_detail',
            'internship_application',
            'id_personal_detail'
        );

        $this->addForeignKey(
            'fk-internship_application-id_personal_detail',
            'internship_application',
            'id_personal_detail',
            'personal_detail',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-internship_application-id_organization_detail',
            'internship_application',
            'id_organization_detail'
        );

        $this->addForeignKey(
            'fk-internship_application-id_organization_detail',
            'internship_application',
            'id_organization_detail',
            'organization_detail',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-internship_application-id_academic_session',
            'internship_application',
            'id_academic_session'
        );

        $this->addForeignKey(
            'fk-internship_application-id_academic_session',
            'internship_application',
            'id_academic_session',
            'academic_session',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-internship_application-id_academic_session',
            'internship_application'
        );

        $this->dropIndex(
            'fk-internship_application-id_academic_session',
            'internship_application'
        );

        $this->dropForeignKey(
            'fk-internship_application-id_organization_detail',
            'internship_application'
        );

        $this->dropIndex(
            'fk-internship_application-id_organization_detail',
            'internship_application'
        );

        $this->dropForeignKey(
            'fk-internship_application-id_personal_detail',
            'internship_application'
        );

        $this->dropIndex(
            'fk-internship_application-id_personal_detail',
            'internship_application'
        );

        $this->dropTable('{{%internship_application}}');
    }
}
