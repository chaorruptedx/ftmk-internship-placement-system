<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attachment}}`.
 */
class m210517_195116_create_attachment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attachment}}', [
            'id' => $this->primaryKey(),
            'category' => $this->smallInteger()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'path' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%attachment}}', 'category', '1 = Profile Picture, 2 = Industrial Training Report');
        $this->addCommentOnColumn('{{%attachment}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%attachment}}');
    }
}
