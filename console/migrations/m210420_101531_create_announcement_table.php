<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%announcement}}`.
 */
class m210420_101531_create_announcement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%announcement}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%announcement}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-announcement-id_user',
            'announcement',
            'id_user'
        );

        $this->addForeignKey(
            'fk-announcement-id_user',
            'announcement',
            'id_user',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-announcement-id_user',
            'announcement'
        );

        $this->dropIndex(
            'fk-announcement-id_user',
            'announcement'
        );

        $this->dropTable('{{%announcement}}');
    }
}
