<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%academic_session}}`.
 */
class m210516_032130_create_academic_session_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%academic_session}}', [
            'id' => $this->primaryKey(),
            'semester' => $this->integer()->notNull(),
            'start_year' => $this->integer()->notNull(),
            'end_year' => $this->integer()->notNull(),
            'start_internship_date' => $this->date()->notNull(),
            'end_internship_date' => $this->date()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%academic_session}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'uk-acdmc_session-semester-start_year-end_year-status-deleted_at',
            '{{%academic_session}}',
            [
                'semester',
                'start_year',
                'end_year',
                'status',
                'deleted_at'
            ],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'uk-academic_session-semester-start_year-end_year-status-deleted_at',
            'academic_session'
        );

        $this->dropTable('{{%academic_session}}');
    }
}
