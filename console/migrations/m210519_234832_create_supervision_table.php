<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%supervision}}`.
 */
class m210519_234832_create_supervision_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%supervision}}', [
            'id' => $this->primaryKey(),
            'id_supervisor' => $this->integer()->notNull(),
            'id_supervisee' => $this->integer(),
            // 'id_academic_session' => $this->integer()->notNull(), // Temporary Comment, Due to Something (Minor Flow) Need to be Change
            'id_academic_session' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%supervision}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-supervision-id_supervisor',
            'supervision',
            'id_supervisor'
        );

        $this->addForeignKey(
            'fk-supervision-id_supervisor',
            'supervision',
            'id_supervisor',
            'personal_detail',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-supervision-id_supervisee',
            'supervision',
            'id_supervisee'
        );

        $this->addForeignKey(
            'fk-supervision-id_supervisee',
            'supervision',
            'id_supervisee',
            'personal_detail',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-supervision-id_academic_session',
            'supervision',
            'id_academic_session'
        );

        $this->addForeignKey(
            'fk-supervision-id_academic_session',
            'supervision',
            'id_academic_session',
            'academic_session',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-supervision-id_supervisee',
            'supervision'
        );

        $this->dropIndex(
            'fk-supervision-id_supervisee',
            'supervision'
        );

        $this->dropForeignKey(
            'fk-supervision-id_supervisor',
            'supervision'
        );

        $this->dropIndex(
            'fk-supervision-id_supervisor',
            'supervision'
        );

        $this->dropTable('{{%supervision}}');
    }
}
