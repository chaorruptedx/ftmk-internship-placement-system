<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%achievement}}`.
 */
class m210527_035409_create_achievement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%achievement}}', [
            'id' => $this->primaryKey(),
            'id_personal_detail' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'year' => $this->integer()->notNull(),
            'description' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%achievement}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-achievement-id_personal_detail',
            'achievement',
            'id_personal_detail'
        );

        $this->addForeignKey(
            'fk-achievement-id_personal_detail',
            'achievement',
            'id_personal_detail',
            'personal_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-achievement-id_personal_detail',
            'achievement'
        );

        $this->dropIndex(
            'fk-achievement-id_personal_detail',
            'achievement'
        );

        $this->dropTable('{{%achievement}}');
    }
}
