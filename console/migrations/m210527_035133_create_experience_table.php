<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%experience}}`.
 */
class m210527_035133_create_experience_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%experience}}', [
            'id' => $this->primaryKey(),
            'id_personal_detail' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'start_year' => $this->integer()->notNull(),
            'end_year' => $this->integer()->notNull(),
            'description' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%experience}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-experience-id_personal_detail',
            'experience',
            'id_personal_detail'
        );

        $this->addForeignKey(
            'fk-experience-id_personal_detail',
            'experience',
            'id_personal_detail',
            'personal_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-experience-id_personal_detail',
            'experience'
        );

        $this->dropIndex(
            'fk-experience-id_personal_detail',
            'experience'
        );

        $this->dropTable('{{%experience}}');
    }
}
