<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lookup_faculty}}`.
 */
class m210421_042017_create_lookup_faculty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lookup_faculty}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%lookup_faculty}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'uk-lookup_faculty-code-status-deleted_at',
            '{{%lookup_faculty}}',
            [
                'code',
                'status',
                'deleted_at'
            ],
            true
        );

        $this->batchInsert('{{%lookup_faculty}}', ['code', 'name', 'created_at', 'updated_at'], [
            ['FKEKK', 'Faculty of Electronics and Computer Engineering', time(), time()],
            ['FKE', 'Faculty of Electrical Engineering', time(), time()],
            ['FKM', 'Faculty of Mechanical Engineering', time(), time()],
            ['FKP', 'Faculty of Manufacturing Engineering', time(), time()],
            ['FTMK', 'Faculty of Information and Communications Technology', time(), time()],
            ['FPTT', 'Faculty of Technology Management and Technopreneurship', time(), time()],
            ['FTKEE', 'Faculty of Electrical and Electronic Engineering Technology', time(), time()],
            ['FTKMP', 'Faculty of Mechanical and Manufacturing Engineering Technology', time(), time()],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'uk-lookup_faculty-code-status-deleted_at',
            'lookup_faculty'
        );

        $this->dropTable('{{%lookup_faculty}}');
    }
}
