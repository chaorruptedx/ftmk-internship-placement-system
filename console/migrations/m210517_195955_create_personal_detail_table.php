<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%personal_detail}}`.
 */
class m210517_195955_create_personal_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%personal_detail}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_programme' => $this->integer()->notNull(),
            'id_state' => $this->integer(),
            'id_attachment' => $this->integer(),
            'id_referee_one' => $this->integer(),
            'id_referee_two' => $this->integer(),
            'nric_no' => $this->bigInteger(),
            'user_no' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'gender_no' => $this->smallInteger()->notNull(),
            'religion' => $this->string(),
            'tel_no' => $this->integer(),
            'address' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%personal_detail}}', 'id_attachment', 'Category = Profile Picture');
        $this->addCommentOnColumn('{{%personal_detail}}', 'user_no', 'Staff No. or Matric No.');
        $this->addCommentOnColumn('{{%personal_detail}}', 'gender_no', '1 = Male, 2 = Female');
        $this->addCommentOnColumn('{{%personal_detail}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'fk-personal_detail-id_user',
            'personal_detail',
            'id_user'
        );

        $this->addForeignKey(
            'fk-personal_detail-id_user',
            'personal_detail',
            'id_user',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-personal_detail-id_programme',
            'personal_detail',
            'id_programme'
        );

        $this->addForeignKey(
            'fk-personal_detail-id_programme',
            'personal_detail',
            'id_programme',
            'lookup_programme',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-personal_detail-id_state',
            'personal_detail',
            'id_state'
        );

        $this->addForeignKey(
            'fk-personal_detail-id_state',
            'personal_detail',
            'id_state',
            'lookup_state',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-personal_detail-id_attachment',
            'personal_detail',
            'id_attachment'
        );

        $this->addForeignKey(
            'fk-personal_detail-id_attachment',
            'personal_detail',
            'id_attachment',
            'attachment',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-personal_detail-id_referee_one',
            'personal_detail',
            'id_referee_one'
        );

        $this->addForeignKey(
            'fk-personal_detail-id_referee_one',
            'personal_detail',
            'id_referee_one',
            'personal_detail',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk-personal_detail-id_referee_two',
            'personal_detail',
            'id_referee_two'
        );

        $this->addForeignKey(
            'fk-personal_detail-id_referee_two',
            'personal_detail',
            'id_referee_two',
            'personal_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-personal_detail-id_referee_two',
            'personal_detail'
        );

        $this->dropIndex(
            'fk-personal_detail-id_referee_two',
            'personal_detail'
        );

        $this->dropForeignKey(
            'fk-personal_detail-id_referee_one',
            'personal_detail'
        );

        $this->dropIndex(
            'fk-personal_detail-id_referee_one',
            'personal_detail'
        );

        $this->dropForeignKey(
            'fk-personal_detail-id_attachment',
            'personal_detail'
        );

        $this->dropIndex(
            'fk-personal_detail-id_attachment',
            'personal_detail'
        );

        $this->dropForeignKey(
            'fk-personal_detail-id_state',
            'personal_detail'
        );

        $this->dropIndex(
            'fk-personal_detail-id_state',
            'personal_detail'
        );

        $this->dropForeignKey(
            'fk-personal_detail-id_programme',
            'personal_detail'
        );

        $this->dropIndex(
            'fk-personal_detail-id_programme',
            'personal_detail'
        );

        $this->dropForeignKey(
            'fk-personal_detail-id_user',
            'personal_detail'
        );

        $this->dropIndex(
            'fk-personal_detail-id_user',
            'personal_detail'
        );

        $this->dropTable('{{%personal_detail}}');
    }
}
