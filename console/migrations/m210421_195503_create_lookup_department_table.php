<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lookup_department}}`.
 */
class m210421_195503_create_lookup_department_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lookup_department}}', [
            'id' => $this->primaryKey(),
            'id_faculty' => $this->integer()->notNull(),
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%lookup_department}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'uk-lookup_department-code-status-deleted_at',
            '{{%lookup_department}}',
            [
                'code',
                'status',
                'deleted_at'
            ],
            true
        );

        $this->createIndex(
            'fk-lookup_department-id_faculty',
            'lookup_department',
            'id_faculty'
        );

        $this->addForeignKey(
            'fk-lookup_department-id_faculty',
            'lookup_department',
            'id_faculty',
            'lookup_faculty',
            'id',
            'CASCADE'
        );

        $this->batchInsert('{{%lookup_department}}', ['id_faculty', 'code', 'name', 'created_at', 'updated_at'], [ // System Focus on FTMK only
            ['5', 'CSC', 'Department of Computer System & Communication', time(), time()],
            ['5', 'ICA', 'Department of Intelligent Computing and Analytics', time(), time()],
            ['5', 'IM', 'Department of Interactive Media', time(), time()],
            ['5', 'SE', 'Department of Software Engineering', time(), time()],
            ['5', 'DIP', 'Department of Diploma', time(), time()],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-lookup_department-id_faculty',
            'lookup_department'
        );

        $this->dropIndex(
            'fk-lookup_department-id_faculty',
            'lookup_department'
        );

        $this->dropIndex(
            'uk-lookup_department-code-status-deleted_at',
            'lookup_department'
        );

        $this->dropTable('{{%lookup_department}}');
    }
}
