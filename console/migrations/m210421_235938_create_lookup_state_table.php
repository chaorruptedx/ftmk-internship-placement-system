<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lookup_state}}`.
 */
class m210421_235938_create_lookup_state_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lookup_state}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%lookup_state}}', 'type', '1 = Saturday and Sunday Holidays, 2 = Friday and Saturday Holidays');
        $this->addCommentOnColumn('{{%lookup_state}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'uk-lookup_state-name-status-deleted_at',
            '{{%lookup_state}}',
            [
                'name',
                'status',
                'deleted_at'
            ],
            true
        );

        $this->batchInsert('{{%lookup_state}}', ['name', 'type', 'created_at', 'updated_at'], [
            ['Johor', '2', time(), time()],
            ['Kedah', '2', time(), time()],
            ['Kelantan', '2', time(), time()],
            ['Malacca', '1', time(), time()],
            ['Negeri Sembilan', '1', time(), time()],
            ['Pahang', '1', time(), time()],
            ['Penang', '1', time(), time()],
            ['Perak', '1', time(), time()],
            ['Perlis', '1', time(), time()],
            ['Sabah', '1', time(), time()],
            ['Sarawak', '1', time(), time()],
            ['Selangor', '1', time(), time()],
            ['Terengganu', '2', time(), time()],
            ['Wilayah Persekutuan', '1', time(), time()],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'uk-lookup_state-name-status-deleted_at',
            'lookup_state'
        );

        $this->dropTable('{{%lookup_state}}');
    }
}
