<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'role_no' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%user}}', 'role_no', '1 = Administrator, 2 = Coordinator, 3 = Supervisor, 4 = Student, 5 = Organization');
        $this->addCommentOnColumn('{{%user}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
