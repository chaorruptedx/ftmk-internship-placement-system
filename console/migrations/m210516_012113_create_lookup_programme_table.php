<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lookup_programme}}`.
 */
class m210516_012113_create_lookup_programme_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lookup_programme}}', [
            'id' => $this->primaryKey(),
            'id_department' => $this->integer()->notNull(),
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%lookup_programme}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'uk-lookup_programme-code-status-deleted_at',
            '{{%lookup_programme}}',
            [
                'code',
                'status',
                'deleted_at'
            ],
            true
        );

        $this->createIndex(
            'fk-lookup_programme-id_department',
            'lookup_programme',
            'id_department'
        );

        $this->addForeignKey(
            'fk-lookup_programme-id_department',
            'lookup_programme',
            'id_department',
            'lookup_department',
            'id',
            'CASCADE'
        );

        $this->batchInsert('{{%lookup_programme}}', ['id_department', 'code', 'name', 'created_at', 'updated_at'], [ // System Focus on FTMK only
            ['1', 'BITC', 'Bachelor of Computer Science (Computer Networking)', time(), time()],
            ['1', 'BITZ', 'Bachelor of Computer Science (Computer Security)', time(), time()],
            ['2', 'BITI', 'Bachelor of Computer Science (Artificial Intelligence)', time(), time()],
            ['3', 'BITE', 'Bachelor of Information Technology (Game Technology)', time(), time()],
            ['3', 'BITM', 'Bachelor of Computer Science (Interactive Media)', time(), time()],
            ['4', 'BITD', 'Bachelor of Computer Science (Database Management)', time(), time()],
            ['4', 'BITS', 'Bachelor of Computer Science (Software Development)', time(), time()],
            ['5', 'DIT', 'Diploma in Information and Communication Technology', time(), time()],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-lookup_programme-id_department',
            'lookup_programme'
        );

        $this->dropIndex(
            'fk-lookup_programme-id_department',
            'lookup_programme'
        );

        $this->dropIndex(
            'uk-lookup_programme-code-status-deleted_at',
            'lookup_programme'
        );

        $this->dropTable('{{%lookup_programme}}');
    }
}
