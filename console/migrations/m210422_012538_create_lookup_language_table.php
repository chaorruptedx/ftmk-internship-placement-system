<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lookup_language}}`.
 */
class m210422_012538_create_lookup_language_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lookup_language}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addCommentOnColumn('{{%lookup_language}}', 'status', '1 = Active, 0 = Inactive, -1 = Deleted');

        $this->createIndex(
            'uk-lookup_language-name-status-deleted_at',
            '{{%lookup_language}}',
            [
                'name',
                'status',
                'deleted_at'
            ],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'uk-lookup_language-name-status-deleted_at',
            'lookup_state'
        );

        $this->dropTable('{{%lookup_language}}');
    }
}
