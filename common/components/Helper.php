<?php
namespace common\components;

class Helper 
{
    public static function listStatus()
    {
        $listStatus = array(
            1 => 'Active',
            0 => 'Inactive',
        );

        return $listStatus;
    }

    public static function getStatus($status)
    {
        if ($status == "status_active")
            return 1;
        else if ($status == "status_inactive")
            return 0;
        else if ($status == "status_deleted")
            return -1;
        else if ($status == "deleted_at_default")
            return 0;
    }

    public static function getStatusName($id_status)
    {
        $listStatus = self::listStatus();

        foreach ($listStatus as $index => $key)
        {
            if ($index == $id_status)
                return $key;
        }

        return false;
    }

    public static function listRole()
    {
        $listRole = array(
            1 => 'Administrator',
            2 => 'Coordinator',
            3 => 'Supervisor',
            4 => 'Student',
            5 => 'Organization'
        );

        return $listRole;
    }

    public static function getRoleNo($role)
    {
        $listRole = self::listRole();

        foreach ($listRole as $index => $key)
        {
            if ($key == "Administrator" && $role == "role_admin")
                return $index;
            elseif ($key == "Coordinator" && $role == "role_coordinator")
                return $index;
            elseif ($key == "Supervisor" && $role == "role_supervisor")
                return $index;
            elseif ($key == "Student" && $role == "role_student")
                return $index;
            elseif ($key == "Organization" && $role == "role_organization")
                return $index;
            
        }

        return false;
    }

    public static function getRoleName($role_no)
    {
        $listRole = self::listRole();

        foreach ($listRole as $index => $key)
        {
            if ($index == $role_no)
                return $key;
        }

        return false;
    }

    public static function listStateType()
    {
        $listRole = array(
            1 => 'Saturday and Sunday Holidays',
            2 => 'Friday and Saturday Holidays',
        );

        return $listRole;
    }

    public static function getStateTypeName($type)
    {
        $listStateType = self::listStateType();

        foreach ($listStateType as $index => $key)
        {
            if ($index == $type)
                return $key;
        }

        return false;
    }

    public static function getSemester()
    {
        $listSemester = array(
            1 => '1',
            2 => '2',
            3 => '3',
        );

        return $listSemester;
    }

    public static function listGender()
    {
        $listGender = array(
            1 => 'Male',
            2 => 'Female',
        );

        return $listGender;
    }

    public static function getGender($gender_no = 0)
    {
        $listGender = self::listGender();

        return $listGender[$gender_no];
    }

    public static function listOrganizationType()
    {
        $listOrganizationType = array(
            1 => 'Government',
            2 => 'Private',
        );

        return $listOrganizationType;
    }

    public static function getOrganizationType($type)
    {
        $listOrganizationType = self::listOrganizationType();

        return $listOrganizationType[$type];
    }

    public static function listDay()
    {
        $listDay = array(
            0 => 'Monday',
            1 => 'Tuesday',
            2 => 'Wednesday',
            3 => 'Thursday',
            4 => 'Friday',
            5 => 'Saturday',
            6 => 'Sunday',
        );

        return $listDay;
    }

    public static function getDay($day)
    {
        $listDay = self::listDay();

        return $listDay[$day];
    }

    public static function listApplicationStatusStudent()
    {
        $listApplicationStatus = array(
            0 => 'Reject',
            1 => 'Applied',
            2 => 'In Review',
            3 => 'Waiting for Student Approval',
            4 => 'Student Accepted'
        );

        return $listApplicationStatus;
    }

    public static function getApplicationStatusStudent($application_status)
    {
        $listApplicationStatus = self::listApplicationStatusStudent();

        return $listApplicationStatus[$application_status];
    }

    public static function listApplicationStatusOrganization()
    {
        $listApplicationStatus = array(
            0 => 'Reject',
            1 => 'New',
            2 => 'In Review',
            3 => 'Awaiting Student Reply',
            4 => 'Student Accepted'
        );

        return $listApplicationStatus;
    }

    public static function getApplicationStatusOrganization($application_status)
    {
        $listApplicationStatus = self::listApplicationStatusOrganization();

        return $listApplicationStatus[$application_status];
    }
}