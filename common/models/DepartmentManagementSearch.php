<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LookupDepartment;

/**
 * DepartmentManagementSearch represents the model behind the search form of `common\models\LookupDepartment`.
 */
class DepartmentManagementSearch extends LookupDepartment
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['faculty.code']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_faculty', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['code', 'name', 'faculty.code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupDepartment::find()
            ->joinWith([
                'faculty'
            ])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['faculty.code'] = [
            'asc' => ['lookup_faculty.code' => SORT_ASC],
            'desc' => ['lookup_faculty.code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lookup_department.id' => $this->id,
            'lookup_department.id_faculty' => $this->id_faculty,
            'lookup_department.status' => $this->status,
            'lookup_department.created_at' => $this->created_at,
            'lookup_department.updated_at' => $this->updated_at,
            'lookup_department.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'lookup_department.code', $this->code])
            ->andFilterWhere(['like', 'lookup_department.name', $this->name])
            ->andFilterWhere(['like', 'lookup_faculty.code', $this->getAttribute('faculty.code')]);

        return $dataProvider;
    }
}
