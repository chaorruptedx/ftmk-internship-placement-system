<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%job_scope_requirement}}".
 *
 * @property int $id
 * @property int $id_programme
 * @property int $id_organization_detail
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property OrganizationDetail $organizationDetail
 * @property LookupProgramme $programme
 */
class JobScopeRequirement extends \yii\db\ActiveRecord
{
    public $id_programme_array;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%job_scope_requirement}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_programme_array'], 'required'],
            [['id_programme', 'id_organization_detail', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['id_organization_detail'], 'exist', 'skipOnError' => true, 'targetClass' => OrganizationDetail::className(), 'targetAttribute' => ['id_organization_detail' => 'id']],
            [['id_programme'], 'exist', 'skipOnError' => true, 'targetClass' => LookupProgramme::className(), 'targetAttribute' => ['id_programme' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_programme' => Yii::t('app', 'Id Programme'),
            'id_programme_array' => Yii::t('app', 'Job Scope Requirement'),
            'id_organization_detail' => Yii::t('app', 'Id Organization Detail'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[OrganizationDetail]].
     *
     * @return \yii\db\ActiveQuery|OrganizationDetailQuery
     */
    public function getOrganizationDetail()
    {
        return $this->hasOne(OrganizationDetail::className(), ['id' => 'id_organization_detail']);
    }

    /**
     * Gets query for [[Programme]].
     *
     * @return \yii\db\ActiveQuery|LookupProgrammeQuery
     */
    public function getProgramme()
    {
        return $this->hasOne(LookupProgramme::className(), ['id' => 'id_programme']);
    }

    /**
     * {@inheritdoc}
     * @return JobScopeRequirementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JobScopeRequirementQuery(get_called_class());
    }

    public function saveJobScopeRequirement($id_organization_detail)
    {
        $modelJobScopeRequirements = JobScopeRequirement::find()
            ->where(['id_organization_detail' => $id_organization_detail])
            ->active()
            ->all();

        if (isset($modelJobScopeRequirements) && is_array($modelJobScopeRequirements)) // Delete All Old Data
        {
            foreach ($modelJobScopeRequirements as $modelJobScopeRequirement)
            {
                $modelJobScopeRequirement->id_programme_array = true;
                $modelJobScopeRequirement->deleteJobScopeRequirement();
            }
        }

        if (isset($this->id_programme_array) && is_array($this->id_programme_array)) // Insert New Data
        {
            foreach ($this->id_programme_array as $key => $value)
            {
                $modelJobScopeRequirement = new JobScopeRequirement;

                $modelJobScopeRequirement->id_programme = $value;
                $modelJobScopeRequirement->id_programme_array = true;
                $modelJobScopeRequirement->id_organization_detail = $id_organization_detail;
                $modelJobScopeRequirement->status = Helper::getStatus('status_active');
                $modelJobScopeRequirement->deleted_at = Helper::getStatus('deleted_at_default');
        
                if (!$modelJobScopeRequirement->validate()) {
                    return null;
                }
                else
                {
                    $modelJobScopeRequirement->save();
                }
            }
        }

        return true;
    }

    public function deleteJobScopeRequirement()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
