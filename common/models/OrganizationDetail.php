<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%organization_detail}}".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_state
 * @property int $id_attachment
 * @property string $name
 * @property int $type 1 = Government, 2 = Private
 * @property string $address
 * @property int $tel_no
 * @property int $start_day 0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday, 5 = Saturday, 6 = Sunday
 * @property int $end_day 0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday, 5 = Saturday, 6 = Sunday
 * @property int $open_hour
 * @property int $close_hour
 * @property string $job_description
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property Attachment $attachment
 * @property LookupState $state
 * @property User $user
 */
class OrganizationDetail extends \yii\db\ActiveRecord
{
    public $email;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%organization_detail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_state', 'name', 'type', 'address', 'tel_no', 'start_day', 'end_day', 'open_hour', 'close_hour', 'job_description', 'accepting_internship'], 'required'],
            [['id_user', 'id_state', 'id_attachment', 'type', 'tel_no', 'start_day', 'end_day', 'accepting_internship', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['job_description'], 'string'],
            [['name', 'address'], 'string', 'max' => 255],
            [['open_hour', 'close_hour', 'email'], 'safe'],
            [['id_attachment'], 'exist', 'skipOnError' => true, 'targetClass' => Attachment::className(), 'targetAttribute' => ['id_attachment' => 'id']],
            [['id_state'], 'exist', 'skipOnError' => true, 'targetClass' => LookupState::className(), 'targetAttribute' => ['id_state' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'ID User'),
            'id_state' => Yii::t('app', 'State'),
            'id_attachment' => Yii::t('app', 'ID Attachment'),
            'name' => Yii::t('app', 'Organization Name'),
            'type' => Yii::t('app', 'Type'),
            'address' => Yii::t('app', 'Address'),
            'tel_no' => Yii::t('app', 'Tel. No.'),
            'start_day' => Yii::t('app', 'Start Day'),
            'end_day' => Yii::t('app', 'End Day'),
            'open_hour' => Yii::t('app', 'Open Hour'),
            'close_hour' => Yii::t('app', 'Close Hour'),
            'job_description' => Yii::t('app', 'Job Description'),
            'accepting_internship' => Yii::t('app', 'Accepting Internship'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[JobScopeRequirements]].
     *
     * @return \yii\db\ActiveQuery|JobScopeRequirementQuery
     */
    public function getJobScopeRequirements()
    {
        return $this->hasMany(JobScopeRequirement::className(), ['id_organization_detail' => 'id'])->where(['job_scope_requirement.status' => '1']);
    }

    /**
     * Gets query for [[Attachment]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getAttachment()
    {
        return $this->hasOne(Attachment::className(), ['id' => 'id_attachment']);
    }

    /**
     * Gets query for [[State]].
     *
     * @return \yii\db\ActiveQuery|LookupStateQuery
     */
    public function getState()
    {
        return $this->hasOne(LookupState::className(), ['id' => 'id_state']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * {@inheritdoc}
     * @return OrganizationDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrganizationDetailQuery(get_called_class());
    }

    public static function findByIDUser($id_user)
    {
        $model = self::find()
            ->where([
                'id_user' => $id_user,
            ])
            ->active()
            ->one();

        return $model;
    }

    public function saveOrganizationDetail($id_user, $id_attachment)
    {
        $this->id_user = $id_user;
        $this->id_attachment = $id_attachment;
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }
}
