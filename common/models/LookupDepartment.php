<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%lookup_department}}".
 *
 * @property int $id
 * @property int $id_faculty
 * @property string $code
 * @property string $name
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property LookupFaculty $faculty
 * @property LookupProgramme[] $lookupProgrammes
 */
class LookupDepartment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lookup_department}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_faculty', 'code', 'name'], 'required'],
            [['id_faculty', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code', 'status', 'deleted_at'], 'unique', 'targetAttribute' => ['code', 'status', 'deleted_at']],
            [['id_faculty'], 'exist', 'skipOnError' => true, 'targetClass' => LookupFaculty::className(), 'targetAttribute' => ['id_faculty' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_faculty' => Yii::t('app', 'Faculty'),
            'code' => Yii::t('app', 'Department Code'),
            'name' => Yii::t('app', 'Department Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[Faculty]].
     *
     * @return \yii\db\ActiveQuery|LookupFacultyQuery
     */
    public function getFaculty()
    {
        return $this->hasOne(LookupFaculty::className(), ['id' => 'id_faculty']);
    }

    /**
     * Gets query for [[LookupProgrammes]].
     *
     * @return \yii\db\ActiveQuery|LookupProgrammeQuery
     */
    public function getLookupProgrammes()
    {
        return $this->hasMany(LookupProgramme::className(), ['id_department' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return LookupDepartmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LookupDepartmentQuery(get_called_class());
    }

    public function saveDepartment()
    {
        $this->id_faculty = 5; // FTMK
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function deleteDepartment()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
