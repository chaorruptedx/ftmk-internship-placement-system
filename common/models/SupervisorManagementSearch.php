<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Supervision;

/**
 * SupervisorManagementSearch represents the model behind the search form of `common\models\Supervision`.
 */
class SupervisorManagementSearch extends Supervision
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['supervisor.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_supervisor', 'id_supervisee', 'id_academic_session', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['supervisor.name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Supervision::find()
            ->joinWith([
                'supervisor'
            ])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['supervisor.name'] = [
            'asc' => ['personal_detail.name' => SORT_ASC],
            'desc' => ['personal_detail.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_supervisor' => $this->id_supervisor,
            'id_supervisee' => $this->id_supervisee,
            'id_academic_session' => $this->id_academic_session,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'personal_detail.name', $this->getAttribute('supervisor.name')]);

        return $dataProvider;
    }
}
