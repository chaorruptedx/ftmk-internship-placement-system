<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Announcement;

/**
 * AnnouncementSearch represents the model behind the search form of `common\models\Announcement`.
 */
class AnnouncementSearch extends Announcement
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['announcementRoles.role_no']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['title', 'content', 'announcementRoles.role_no'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Announcement::find()
            ->joinWith(['announcementRoles'])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['announcementRoles.role_no'] = [
            'asc' => ['announcement_role.role_no' => SORT_ASC],
            'desc' => ['announcement_role.role_no' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'announcement.id' => $this->id,
            'announcement.id_user' => $this->id_user,
            'announcement.status' => $this->status,
            'announcement.created_at' => $this->created_at,
            'announcement.updated_at' => $this->updated_at,
            'announcement.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'announcement.title', $this->title])
            ->andFilterWhere(['like', 'announcement.content', $this->content])
            ->andFilterWhere(['in', 'announcement_role.role_no', $this->getAttribute('announcementRoles.role_no')]);

        return $dataProvider;
    }
}
