<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[AnnouncementRole]].
 *
 * @see AnnouncementRole
 */
class AnnouncementRoleQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andFilterWhere(['announcement_role.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return AnnouncementRole[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AnnouncementRole|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
