<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%supervision}}".
 *
 * @property int $id
 * @property int $id_supervisor
 * @property int|null $id_supervisee
 * @property int $id_academic_session
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property AcademicSession $academicSession
 * @property PersonalDetail $supervisee
 * @property PersonalDetail $supervisor
 */
class Supervision extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%supervision}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_supervisor', 'id_academic_session'], 'required'],
            [['id_supervisor', 'id_supervisee', 'id_academic_session', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['id_academic_session'], 'exist', 'skipOnError' => true, 'targetClass' => AcademicSession::className(), 'targetAttribute' => ['id_academic_session' => 'id']],
            [['id_supervisee'], 'exist', 'skipOnError' => true, 'targetClass' => PersonalDetail::className(), 'targetAttribute' => ['id_supervisee' => 'id']],
            [['id_supervisor'], 'exist', 'skipOnError' => true, 'targetClass' => PersonalDetail::className(), 'targetAttribute' => ['id_supervisor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_supervisor' => Yii::t('app', 'Faculty Supervisor'),
            'id_supervisee' => Yii::t('app', 'Student'),
            'id_academic_session' => Yii::t('app', 'Academic Session'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[AcademicSession]].
     *
     * @return \yii\db\ActiveQuery|AcademicSessionQuery
     */
    public function getAcademicSession()
    {
        return $this->hasOne(AcademicSession::className(), ['id' => 'id_academic_session']);
    }

    /**
     * Gets query for [[Supervisee]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getSupervisee()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_supervisee']);
    }

    /**
     * Gets query for [[Supervisor]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getSupervisor()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_supervisor']);
    }

    /**
     * {@inheritdoc}
     * @return SupervisionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SupervisionQuery(get_called_class());
    }

    public function saveSupervision()
    {
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function deleteSupervision()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }

    public static function saveAssignStudent($id_supervisor, $id_students)
    {   
        $modelSupervisions = self::find()
            ->where([
                'id_supervisor' => $id_supervisor,
            ])
            ->active()
            ->all();

        if (!empty($modelSupervisions) && is_array($modelSupervisions))
        {
            foreach ($modelSupervisions as $modelSupervision)
            {
                if ($id_students === null || !in_array($modelSupervision->id_supervisee, $id_students)) // Remove Student from Current Supervisor
                {
                    $modelSupervision->status = Helper::getStatus('status_deleted');
                    $modelSupervision->deleted_at = time();

                    $modelSupervision->save(false);

                    if (($key = array_search($modelSupervision->id_supervisee, $id_students)) !== false)
                    {
                        unset($id_students[$key]);
                    }
                }
                else if (in_array($modelSupervision->id_supervisee, $id_students)) // Ignore Student Already in Current Supervisor
                {
                    if (($key = array_search($modelSupervision->id_supervisee, $id_students)) !== false)
                    {
                        unset($id_students[$key]);
                    }
                }
            }
        }

        if ($id_students !== null && is_array($id_students))
        {
            foreach ($id_students as $id_student) // Add New Student under Current Supervisor
            {
                $modelSupervisionNew = new Supervision();

                $modelSupervisionNew->id_supervisor = $id_supervisor;
                $modelSupervisionNew->id_supervisee = $id_student;
                $modelSupervisionNew->status = Helper::getStatus('status_active');
                $modelSupervisionNew->deleted_at = Helper::getStatus('deleted_at_default');

                $modelSupervisionNew->save(false);
            }
        }

        return 0;
    }
}
