<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[Supervision]].
 *
 * @see Supervision
 */
class SupervisionQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andFilterWhere(['supervision.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return Supervision[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Supervision|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
