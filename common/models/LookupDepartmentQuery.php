<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[LookupDepartment]].
 *
 * @see LookupDepartment
 */
class LookupDepartmentQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andFilterWhere(['lookup_department.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return LookupDepartment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LookupDepartment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
