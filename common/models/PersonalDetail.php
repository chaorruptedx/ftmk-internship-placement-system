<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%personal_detail}}".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_programme
 * @property int|null $id_state
 * @property int|null $id_attachment Category = Profile Picture
 * @property int|null $nric_no
 * @property string $user_no Staff No. or Matric No.
 * @property string $name
 * @property int $gender_no 1 = Male, 2 = Female
 * @property string|null $religion
 * @property int|null $tel_no
 * @property string|null $address
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property Attachment $attachment
 * @property LookupProgramme $programme
 * @property LookupState $state
 * @property User $user
 */
class PersonalDetail extends \yii\db\ActiveRecord
{
    const SCENARIO_SAVE_COORDINATOR_SUPERVISOR = 'save_coordinator_supervisor';
    const SCENARIO_SAVE_COORDINATOR_SUPERVISOR_FINAL = 'save_coordinator_supervisor_final';
    const SCENARIO_SAVE_STUDENT = 'save_student';
    const SCENARIO_SAVE_STUDENT_FINAL = 'save_student_final';
    const SCENARIO_SAVE_STUDENT_RESUME = 'save_student_resume';

    public $student_status, $student_unsupervised, $student_supervised;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%personal_detail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_programme', 'user_no', 'name', 'gender_no'], 'required', 'on' => 'save_coordinator_supervisor'],
            [['id_user', 'id_programme', 'user_no', 'name', 'gender_no'], 'required', 'on' => 'save_coordinator_supervisor_final'],
            [['id_programme', 'nric_no', 'user_no', 'name', 'gender_no'], 'required', 'on' => 'save_student'],
            [['id_user', 'id_programme', 'nric_no', 'user_no', 'name', 'gender_no'], 'required', 'on' => 'save_student_final'],
            [['tel_no', 'address', 'id_referee_one', 'id_referee_two'], 'required', 'on' => 'save_student_resume'],
            [['id_referee_one', 'id_referee_two'], function ($attribute, $params, $validator) {
                if ($this->id_referee_one == $this->id_referee_two)
                {
                    $this->addError('id_referee_one', 'Both Referee Must Be Unique');
                    $this->addError('id_referee_two', 'Both Referee Must Be Unique');
                }
            }, 'on' => 'save_student_resume'],

            [['id_user', 'id_programme', 'id_state', 'id_attachment', 'nric_no', 'gender_no', 'tel_no', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['user_no', 'name', 'religion', 'address'], 'string', 'max' => 255],
            [['id_referee_one', 'id_referee_two', 'student_status'], 'safe'],

            [['id_attachment'], 'exist', 'skipOnError' => true, 'targetClass' => Attachment::className(), 'targetAttribute' => ['id_attachment' => 'id']],
            [['id_programme'], 'exist', 'skipOnError' => true, 'targetClass' => LookupProgramme::className(), 'targetAttribute' => ['id_programme' => 'id']],
            [['id_state'], 'exist', 'skipOnError' => true, 'targetClass' => LookupState::className(), 'targetAttribute' => ['id_state' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'ID User'),
            'id_programme' => Yii::t('app', 'Programme'),
            'id_state' => Yii::t('app', 'State'),
            'id_attachment' => Yii::t('app', 'Attachment'),
            'id_referee_one' => Yii::t('app', 'Referee 1'),
            'id_referee_two' => Yii::t('app', 'Referee 2'),
            'nric_no' => Yii::t('app', 'NRIC No.'),
            'user_no' => Yii::t('app', 'User No.'),
            'name' => Yii::t('app', 'Name'),
            'gender_no' => Yii::t('app', 'Gender'),
            'religion' => Yii::t('app', 'Religion'),
            'tel_no' => Yii::t('app', 'Tel. No.'),
            'address' => Yii::t('app', 'Address'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[Achievements]].
     *
     * @return \yii\db\ActiveQuery|AchievementQuery
     */
    public function getAchievements()
    {
        return $this->hasMany(Achievement::className(), ['id_personal_detail' => 'id'])->active();
    }

    /**
     * Gets query for [[Educations]].
     *
     * @return \yii\db\ActiveQuery|EducationQuery
     */
    public function getEducations()
    {
        return $this->hasMany(Education::className(), ['id_personal_detail' => 'id'])->active();
    }

    /**
     * Gets query for [[Experiences]].
     *
     * @return \yii\db\ActiveQuery|ExperienceQuery
     */
    public function getExperiences()
    {
        return $this->hasMany(Experience::className(), ['id_personal_detail' => 'id'])->active();
    }

    /**
     * Gets query for [[InternshipApplications]].
     *
     * @return \yii\db\ActiveQuery|InternshipApplicationQuery
     */
    public function getInternshipApplications()
    {
        return $this->hasMany(InternshipApplication::className(), ['id_personal_detail' => 'id']);
    }

    /**
     * Gets query for [[Languages]].
     *
     * @return \yii\db\ActiveQuery|LanguageQuery
     */
    public function getLanguages()
    {
        return $this->hasMany(Language::className(), ['id_personal_detail' => 'id'])->active();
    }

    /**
     * Gets query for [[Attachment]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getAttachment()
    {
        return $this->hasOne(Attachment::className(), ['id' => 'id_attachment']);
    }

    /**
     * Gets query for [[Programme]].
     *
     * @return \yii\db\ActiveQuery|LookupProgrammeQuery
     */
    public function getProgramme()
    {
        return $this->hasOne(LookupProgramme::className(), ['id' => 'id_programme']);
    }

    /**
     * Gets query for [[RefereeOne]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getRefereeOne()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_referee_one']);
    }

    /**
     * Gets query for [[RefereeTwo]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getRefereeTwo()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_referee_two']);
    }

    /**
     * Gets query for [[State]].
     *
     * @return \yii\db\ActiveQuery|LookupStateQuery
     */
    public function getState()
    {
        return $this->hasOne(LookupState::className(), ['id' => 'id_state']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * Gets query for [[Skills]].
     *
     * @return \yii\db\ActiveQuery|SkillQuery
     */
    public function getSkills()
    {
        return $this->hasMany(Skill::className(), ['id_personal_detail' => 'id'])->active();
    }

    /**
     * Gets query for [[Supervisions]].
     *
     * @return \yii\db\ActiveQuery|SupervisionQuery
     */
    public function getSupervisee()
    {
        return $this->hasOne(Supervision::className(), ['id_supervisee' => 'id'])->active();
    }

    /**
     * Gets query for [[Supervisions0]].
     *
     * @return \yii\db\ActiveQuery|SupervisionQuery
     */
    public function getSupervisor()
    {
        return $this->hasOne(Supervision::className(), ['id_supervisor' => 'id'])->active();
    }

    /**
     * {@inheritdoc}
     * @return PersonalDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonalDetailQuery(get_called_class());
    }

    public static function findByID($id)
    {
        $model = self::find()
            ->where([
                'id' => $id,
            ])
            ->active()
            ->one();

        return $model;
    }

    public static function findByIDUser($id_user)
    {
        $model = self::find()
            ->where([
                'id_user' => $id_user,
            ])
            ->active()
            ->one();

        return $model;
    }

    public function savePersonalDetail($id_user, $role_no)
    {
        $this->id_user = $id_user;
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (in_array($role_no, array(2, 3))) // Coordinator, Supervisor
        {
            $this->scenario = self::SCENARIO_SAVE_COORDINATOR_SUPERVISOR_FINAL;
        }
        else if ($role_no == 4) // Student
        {
            $this->scenario = self::SCENARIO_SAVE_STUDENT_FINAL;
        }

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function savePersonalDetailStudentResume($id_attachment)
    {
        $this->id_attachment = $id_attachment;
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function deletePersonalDetail()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
