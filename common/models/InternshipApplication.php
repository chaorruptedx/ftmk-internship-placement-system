<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%internship_application}}".
 *
 * @property int $id
 * @property int $id_personal_detail
 * @property int $id_organization_detail
 * @property int $application_status 0 = Reject, 1 = Pending, 2 = In Review, 3 = Organization Accept, 4 = Student Accept
 * @property string|null $notes
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property OrganizationDetail $organizationDetail
 * @property User $personalDetail
 */
class InternshipApplication extends \yii\db\ActiveRecord
{
    public $new, $in_review, $awaiting_student_reply, $student_accepted;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%internship_application}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_personal_detail', 'id_organization_detail', 'application_status'], 'required'],
            [['id_personal_detail', 'id_organization_detail', 'application_status', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['notes'], 'string', 'max' => 255],
            [['id_organization_detail'], 'exist', 'skipOnError' => true, 'targetClass' => OrganizationDetail::className(), 'targetAttribute' => ['id_organization_detail' => 'id']],
            [['id_personal_detail'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_personal_detail' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_personal_detail' => Yii::t('app', 'Id Personal Detail'),
            'id_organization_detail' => Yii::t('app', 'Id Organization Detail'),
            'application_status' => Yii::t('app', 'Application Status'),
            'notes' => Yii::t('app', 'Notes'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Date of Application'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[AcademicSession]].
     *
     * @return \yii\db\ActiveQuery|AcademicSessionQuery
     */
    public function getAcademicSession()
    {
        return $this->hasOne(AcademicSession::className(), ['id' => 'id_academic_session']);
    }

    /**
     * Gets query for [[OrganizationDetail]].
     *
     * @return \yii\db\ActiveQuery|OrganizationDetailQuery
     */
    public function getOrganizationDetail()
    {
        return $this->hasOne(OrganizationDetail::className(), ['id' => 'id_organization_detail']);
    }

    /**
     * Gets query for [[PersonalDetail]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getPersonalDetail()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_personal_detail']);
    }

    /**
     * {@inheritdoc}
     * @return InternshipApplicationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InternshipApplicationQuery(get_called_class());
    }

    public function saveInternshipApplication($id_personal_detail, $id_organization_detail, $application_status, $id_academic_session = null)
    {
        $this->id_personal_detail = $id_personal_detail;
        $this->id_organization_detail = $id_organization_detail;
        if ($id_academic_session !== null)
            $this->id_academic_session = $id_academic_session;
        $this->application_status = $application_status;
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public static function findModelInternshipApplication($id)
    {
        $model = self::find()
            ->where([
                'id' => $id,
            ])
            ->active()
            ->one();

        return $model;
    }

    public static function getInternshipApplication($id_personal_detail, $id_organization_detail)
    {
        $model = self::find()
            ->where([
                'id_personal_detail' => $id_personal_detail,
                'id_organization_detail' => $id_organization_detail,
            ])
            ->active()
            ->one();

        return $model;
    }

    public static function getActiveInternshipApplication($id_personal_detail, $id_organization_detail)
    {
        $model = self::find()
            ->where([
                'id_personal_detail' => $id_personal_detail,
                'id_organization_detail' => $id_organization_detail,
                'application_status' => [1, 2, 3, 4],
            ])
            ->active()
            ->one();

        return $model;
    }

    public static function checkExistingApplication($id_personal_detail, $id_organization_detail, $array_application_status)
    {
        $model = self::find()
            ->where([
                'id_personal_detail' => $id_personal_detail,
                'id_organization_detail' => $id_organization_detail,
                'application_status' => $array_application_status,
            ])
            ->active()
            ->one();

        if ($model !== null)
            return true;
        else
            return false;
    }
}
