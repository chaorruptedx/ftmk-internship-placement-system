<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PersonalDetail;

/**
 * ResumeSearch represents the model behind the search form of `common\models\PersonalDetail`.
 */
class ResumeSearch extends PersonalDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_programme', 'id_state', 'id_attachment', 'nric_no', 'gender_no', 'tel_no', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['user_no', 'name', 'religion', 'address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonalDetail::find()->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_programme' => $this->id_programme,
            'id_state' => $this->id_state,
            'id_attachment' => $this->id_attachment,
            'nric_no' => $this->nric_no,
            'gender_no' => $this->gender_no,
            'tel_no' => $this->tel_no,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'user_no', $this->user_no])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'religion', $this->religion])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
