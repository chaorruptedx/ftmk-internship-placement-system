<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrganizationDetail;

/**
 * InternshipApplicationSearch represents the model behind the search form of `common\models\OrganizationDetail`.
 */
class InternshipApplicationSearch extends OrganizationDetail
{
    public $id_programme;

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['state.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_state', 'id_attachment', 'type', 'tel_no', 'start_day', 'end_day', 'status', 'created_at', 'updated_at', 'deleted_at', 'id_programme', 'accepting_internship'], 'integer'],
            [['name', 'address', 'open_hour', 'close_hour', 'job_description', 'state.name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrganizationDetail::find()
            ->joinWith([
                'state',
                'jobScopeRequirements',
            ])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['state.name'] = [
            'asc' => ['lookup_state.name' => SORT_ASC],
            'desc' => ['lookup_state.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'organization_detail.id' => $this->id,
            'organization_detail.id_user' => $this->id_user,
            'organization_detail.id_state' => $this->id_state,
            'organization_detail.id_attachment' => $this->id_attachment,
            'organization_detail.type' => $this->type,
            'organization_detail.tel_no' => $this->tel_no,
            'organization_detail.start_day' => $this->start_day,
            'organization_detail.end_day' => $this->end_day,
            'organization_detail.open_hour' => $this->open_hour,
            'organization_detail.close_hour' => $this->close_hour,
            'organization_detail.accepting_internship' => $this->accepting_internship,
            'organization_detail.status' => $this->status,
            'organization_detail.created_at' => $this->created_at,
            'organization_detail.updated_at' => $this->updated_at,
            'organization_detail.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'organization_detail.name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'job_description', $this->job_description])
            ->andFilterWhere(['like', 'lookup_state.name', $this->getAttribute('state.name')])
            ->andFilterWhere(['in', 'job_scope_requirement.id_programme', $this->id_programme]);

        return $dataProvider;
    }
}
