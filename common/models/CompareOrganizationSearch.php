<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrganizationDetail;

/**
 * CompareOrganizationSearch represents the model behind the search form of `common\models\OrganizationDetail`.
 */
class CompareOrganizationSearch extends OrganizationDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_state', 'id_attachment', 'type', 'tel_no', 'start_day', 'end_day', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['name', 'address', 'open_hour', 'close_hour', 'job_description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrganizationDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_state' => $this->id_state,
            'id_attachment' => $this->id_attachment,
            'type' => $this->type,
            'tel_no' => $this->tel_no,
            'start_day' => $this->start_day,
            'end_day' => $this->end_day,
            'open_hour' => $this->open_hour,
            'close_hour' => $this->close_hour,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'job_description', $this->job_description]);

        return $dataProvider;
    }
}
