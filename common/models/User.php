<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\components\Helper;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property integer $role_no 1 = Administrator, 2 = Coordinator, 3 = Supervisor, 4 = Student, 5 = Organization
 * @property integer $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_UPDATE_USER = 'update_user';

    public $password, $admin, $coordinator, $supervisor, $student, $organization;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim', 'on' => 'update_user'],
            ['username', 'required', 'on' => 'update_user'],
            ['username', 'unique', 'targetAttribute' => ['username', 'status', 'deleted_at'], 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.', 'on' => 'update_user'],
            ['username', 'string', 'min' => 2, 'max' => 255, 'on' => 'update_user'],

            ['email', 'trim', 'on' => 'update_user'],
            ['email', 'required', 'on' => 'update_user'],
            ['email', 'email', 'on' => 'update_user'],
            ['email', 'string', 'max' => 255, 'on' => 'update_user'],
            ['email', 'unique', 'targetAttribute' => ['email', 'status', 'deleted_at'], 'targetClass' => '\common\models\User', 'message' => 'This {attribute} has already been taken.', 'on' => 'update_user'],

            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength'], 'on' => 'update_user'],

            ['role_no', 'required', 'on' => 'update_user'],
            ['role_no', 'in', 'range' => array_keys(Helper::listRole()), 'on' => 'update_user'],

            ['role_no', 'in', 'range' => [Helper::getRoleNo('role_admin'), Helper::getRoleNo('role_coordinator'), Helper::getRoleNo('role_supervisor'), Helper::getRoleNo('role_student'), Helper::getRoleNo('role_organization')]],
            ['status', 'in', 'range' => [Helper::getStatus('status_active'), Helper::getStatus('status_inactive'), Helper::getStatus('status_deleted')]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'role_no' => Yii::t('app', 'Role Name'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'role_no' => [Helper::getRoleNo('role_student'), Helper::getRoleNo('role_organization')], 'status' => Helper::getStatus('status_active')]);
    }

    public static function findByUsernameAdmin($username)
    {
        return static::findOne(['username' => $username, 'role_no' => [Helper::getRoleNo('role_admin'), Helper::getRoleNo('role_coordinator'), Helper::getRoleNo('role_supervisor')], 'status' => Helper::getStatus('status_active')]);
    }

    public static function findByUsernameAll($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => Helper::getStatus('status_active'),
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => Helper::getStatus('status_active')
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function isUserAdmin($username)
    {
        if (static::findOne(['username' => $username, 'role_no' => Helper::getRoleNo('role_admin'), 'status' => Helper::getStatus('status_active')]))
        {       
            return true;
        }
        else
        {         
            return false;
        }    
    }

    public static function isUserCoordinator($username)
    {
        if (static::findOne(['username' => $username, 'role_no' => Helper::getRoleNo('role_coordinator'), 'status' => Helper::getStatus('status_active')]))
        {       
            return true;
        }
        else
        {         
            return false;
        }    
    }

    public static function isUserSupervisor($username)
    {
        if (static::findOne(['username' => $username, 'role_no' => Helper::getRoleNo('role_supervisor'), 'status' => Helper::getStatus('status_active')]))
        {       
            return true;
        }
        else
        {         
            return false;
        }    
    }

    public static function isUserStudent($username)
    {
        if (static::findOne(['username' => $username, 'role_no' => Helper::getRoleNo('role_student'), 'status' => Helper::getStatus('status_active')]))
        {       
            return true;
        }
        else
        {         
            return false;
        }    
    }

    public static function isUserOrganization($username)
    {
        if (static::findOne(['username' => $username, 'role_no' => Helper::getRoleNo('role_organization'), 'status' => Helper::getStatus('status_active')]))
        {       
            return true;
        }
        else
        {         
            return false;
        }    
    }

    /**
     * Gets query for [[Announcements]].
     *
     * @return \yii\db\ActiveQuery|AnnouncementQuery
     */
    public function getAnnouncements()
    {
        return $this->hasMany(Announcement::className(), ['id_user' => 'id']);
    }

    /**
     * Gets query for [[OrganizationDetail]].
     *
     * @return \yii\db\ActiveQuery|OrganizationDetailQuery
     */
    public function getOrganizationDetail()
    {
        return $this->hasOne(OrganizationDetail::className(), ['id_user' => 'id']);
    }

    /**
     * Gets query for [[PersonalDetail]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getPersonalDetail()
    {
        return $this->hasOne(PersonalDetail::className(), ['id_user' => 'id'])->active();
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public function saveUser($post)
    {
        $this->username = $post['User']['username'];
        $this->email = $post['User']['email'];
        if (!empty($post['User']['password']))
            $this->password = $post['User']['password'];
        if (isset($post['User']['role_no']) && !empty($post['User']['role_no']))
            $this->role_no = $post['User']['role_no'];
        $this->scenario = self::SCENARIO_UPDATE_USER;

        if (!$this->validate()) {
            return null;
        }
        
        if (!empty($post['User']['password']))
            $this->setPassword($this->password);

        return $this->save();
    }

    public function deleteUser()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
