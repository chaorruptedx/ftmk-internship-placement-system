<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%experience}}".
 *
 * @property int $id
 * @property int $id_personal_detail
 * @property string $name
 * @property int $start_year
 * @property int $end_year
 * @property string $description
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property PersonalDetail $personalDetail
 */
class Experience extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%experience}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'start_year', 'end_year', 'description'], 'required'],
            [['id_personal_detail', 'start_year', 'end_year', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['id_personal_detail'], 'exist', 'skipOnError' => true, 'targetClass' => PersonalDetail::className(), 'targetAttribute' => ['id_personal_detail' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_personal_detail' => Yii::t('app', 'Id Personal Detail'),
            'name' => Yii::t('app', 'Name'),
            'start_year' => Yii::t('app', 'Start Year'),
            'end_year' => Yii::t('app', 'End Year'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[PersonalDetail]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getPersonalDetail()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_personal_detail']);
    }

    /**
     * {@inheritdoc}
     * @return ExperienceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExperienceQuery(get_called_class());
    }

    public function saveExperience($id_personal_detail)
    {
        $this->id_personal_detail = $id_personal_detail;
        $this->status = Helper::getStatus('status_active'); 
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public static function deleteExperiences($ids)
    {
        foreach ($ids as $id)
        {
            $modelExperience = self::findOne($id);

            $modelExperience->status = Helper::getStatus('status_deleted');
            $modelExperience->deleted_at = time();
            
            $modelExperience->save();
        }

        return true;
    }
}
