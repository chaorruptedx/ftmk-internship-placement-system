<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%announcement}}".
 *
 * @property int $id
 * @property int $id_user
 * @property string $title
 * @property string $content
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $deleted_at
 *
 * @property User $user
 */
class Announcement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%announcement}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'title', 'content'], 'required'],
            [['id_user', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * Gets query for [[AnnouncementRoles]].
     *
     * @return \yii\db\ActiveQuery|AnnouncementRoleQuery
     */
    public function getAnnouncementRoles()
    {
        return $this->hasMany(AnnouncementRole::className(), ['id_announcement' => 'id'])->active();
    }

    /**
     * {@inheritdoc}
     * @return AnnouncementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AnnouncementQuery(get_called_class());
    }

    public function saveAnnouncement()
    {
        $this->id_user = Yii::$app->user->identity->id;
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function deleteAnnouncement()
    {
        $this->id_user = Yii::$app->user->identity->id;
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
