<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property int $id
 * @property int $id_personal_detail
 * @property int $id_language
 * @property int $scale
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property PersonalDetail $personalDetail
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_language', 'scale'], 'required'],
            [['id_personal_detail', 'id_language', 'scale', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['id_language'], 'exist', 'skipOnError' => true, 'targetClass' => LookupLanguage::className(), 'targetAttribute' => ['id_language' => 'id']],
            [['id_personal_detail'], 'exist', 'skipOnError' => true, 'targetClass' => PersonalDetail::className(), 'targetAttribute' => ['id_personal_detail' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_personal_detail' => Yii::t('app', 'ID Personal Detail'),
            'id_language' => Yii::t('app', 'Language'),
            'scale' => Yii::t('app', 'Scale'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[Language]].
     *
     * @return \yii\db\ActiveQuery|LookupLanguageQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(LookupLanguage::className(), ['id' => 'id_language']);
    }

    /**
     * Gets query for [[PersonalDetail]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getPersonalDetail()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_personal_detail']);
    }

    /**
     * {@inheritdoc}
     * @return LanguageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LanguageQuery(get_called_class());
    }

    public function saveLanguage($id_personal_detail)
    {
        $this->id_personal_detail = $id_personal_detail;
        $this->status = Helper::getStatus('status_active'); 
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public static function deleteLanguages($ids)
    {
        foreach ($ids as $id)
        {
            $modelLanguage = self::findOne($id);

            $modelLanguage->status = Helper::getStatus('status_deleted');
            $modelLanguage->deleted_at = time();
            
            $modelLanguage->save();
        }

        return true;
    }
}
