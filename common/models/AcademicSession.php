<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%academic_session}}".
 *
 * @property int $id
 * @property int $semester
 * @property int $start_year
 * @property int $end_year
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 */
class AcademicSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%academic_session}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester', 'start_year', 'end_year', 'start_internship_date', 'end_internship_date'], 'required'],
            [['semester', 'start_year', 'end_year', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['start_internship_date', 'end_internship_date'], 'date', 'format' => 'php:Y-m-d'],
            [['semester', 'start_year', 'end_year'], 'unique', 'targetAttribute' => ['semester', 'start_year', 'end_year', 'status', 'deleted_at'], 'message' => 'This {attribute} has already been taken.'],
            ['status', function ($attribute, $params, $validator) {
                if ($this->status == 1)
                {
                    $modelAcademicSession = AcademicSession::find()
                        ->where([
                            'status' => $this->status,
                        ])
                        ->andFilterWhere(['<>', 'id', $this->id])
                        ->asArray()
                        ->all();

                    if (COUNT($modelAcademicSession) >= 1)
                        $this->addError('status', 'Only one academic session can be active at a time.');
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'semester' => Yii::t('app', 'Semester'),
            'start_year' => Yii::t('app', 'Start Year'),
            'end_year' => Yii::t('app', 'End Year'),
            'start_internship_date' => Yii::t('app', 'Start Internship Date'),
            'end_internship_date' => Yii::t('app', 'End Internship Date'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return AcademicSessionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AcademicSessionQuery(get_called_class());
    }

    public function saveAcademicSession()
    {
        if (!isset($this->status))
            $this->status = Helper::getStatus('status_active');
            
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function deleteAcademicSession()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
