<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[InternshipApplication]].
 *
 * @see InternshipApplication
 */
class InternshipApplicationQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['internship_application.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return InternshipApplication[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return InternshipApplication|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
