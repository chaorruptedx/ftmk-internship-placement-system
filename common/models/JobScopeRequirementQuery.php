<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[JobScopeRequirement]].
 *
 * @see JobScopeRequirement
 */
class JobScopeRequirementQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['job_scope_requirement.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return JobScopeRequirement[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return JobScopeRequirement|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
