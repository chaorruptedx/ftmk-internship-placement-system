<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\Helper;
use common\models\LookupState;

/**
 * StateManagementSearch represents the model behind the search form of `common\models\LookupState`.
 */
class StateManagementSearch extends LookupState
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupState::find()->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lookup_state.id' => $this->id,
            'lookup_state.type' => $this->type,
            'lookup_state.status' => $this->status,
            'lookup_state.created_at' => $this->created_at,
            'lookup_state.updated_at' => $this->updated_at,
            'lookup_state.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'lookup_state.name', $this->name]);

        return $dataProvider;
    }
}
