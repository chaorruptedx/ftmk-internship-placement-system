<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InternshipApplication;

/**
 * StudentApplicantSearch represents the model behind the search form of `common\models\InternshipApplication`.
 */
class StudentApplicantSearch extends InternshipApplication
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['personalDetail.name', 'personalDetail.programme.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_personal_detail', 'id_organization_detail', 'application_status', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['notes', 'personalDetail.name', 'personalDetail.programme.name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id_organization_detail)
    {
        $query = InternshipApplication::find()
            ->joinWith(['organizationDetail', 'personalDetail.programme'])
            ->where(['organization_detail.id' => $id_organization_detail])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['personalDetail.name'] = [
            'asc' => ['personal_detail.name' => SORT_ASC],
            'desc' => ['personal_detail.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['personalDetail.programme.name'] = [
            'asc' => ['lookup_programme.name' => SORT_ASC],
            'desc' => ['lookup_programme.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'internship_application.id' => $this->id,
            'internship_application.id_personal_detail' => $this->id_personal_detail,
            'internship_application.id_organization_detail' => $this->id_organization_detail,
            'internship_application.application_status' => $this->application_status,
            'internship_application.status' => $this->status,
            'internship_application.created_at' => $this->created_at,
            'internship_application.updated_at' => $this->updated_at,
            'internship_application.deleted_at' => $this->deleted_at,
            'internship_application.deleted_at' => $this->deleted_at,
            'lookup_programme.id' => $this->getAttribute('personalDetail.programme.name'),
        ]);

        $query->andFilterWhere(['like', 'internship_application.notes', $this->notes])
            ->andFilterWhere(['like', 'personal_detail.name', $this->getAttribute('personalDetail.name')]);

        return $dataProvider;
    }
}
