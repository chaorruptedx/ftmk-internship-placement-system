<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[PersonalDetail]].
 *
 * @see PersonalDetail
 */
class PersonalDetailQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andFilterWhere(['personal_detail.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return PersonalDetail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PersonalDetail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
