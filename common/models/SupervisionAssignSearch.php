<?php

namespace common\models;

use yii\base\Model;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\PersonalDetail;

/**
 * SupervisionAssignSearch represents the model behind the search form of `common\models\PersonalDetail`.
 */
class SupervisionAssignSearch extends PersonalDetail
{
    public $id_supervisor;
    
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['programme.code', 'user.email', 'supervisee.supervisor.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_programme', 'id_state', 'id_attachment', 'nric_no', 'gender_no', 'tel_no', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['user_no', 'name', 'religion', 'address', 'programme.code', 'user.email', 'supervisee.supervisor.name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonalDetail::find()
            ->joinWith([
                'user',
                'programme',
            ])
            ->where(['user.role_no' => 3]) // Supervisor
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['programme.code'] = [
            'asc' => ['lookup_programme.code' => SORT_ASC],
            'desc' => ['lookup_programme.code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['user.email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'personal_detail.id' => $this->id,
            'personal_detail.id_user' => $this->id_user,
            'personal_detail.id_programme' => $this->id_programme,
            'personal_detail.id_state' => $this->id_state,
            'personal_detail.id_attachment' => $this->id_attachment,
            'personal_detail.nric_no' => $this->nric_no,
            'personal_detail.gender_no' => $this->gender_no,
            'personal_detail.status' => $this->status,
            'personal_detail.created_at' => $this->created_at,
            'personal_detail.updated_at' => $this->updated_at,
            'personal_detail.deleted_at' => $this->deleted_at,
            'lookup_programme.id' => $this->getAttribute('programme.code'),
        ]);

        $query->andFilterWhere(['like', 'personal_detail.user_no', $this->user_no])
            ->andFilterWhere(['like', 'personal_detail.name', $this->name])
            ->andFilterWhere(['like', 'personal_detail.religion', $this->religion])
            ->andFilterWhere(['like', 'personal_detail.address', $this->address])
            ->andFilterWhere(['like', 'personal_detail.tel_no', $this->tel_no])
            ->andFilterWhere(['like', 'user.email', $this->getAttribute('user.email')]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchStudent($params)
    {
        $query = PersonalDetail::find()
            ->joinWith([
                'user',
                'programme',
                'supervisee',
            ])
            ->where([
                'user.role_no' => 4, // Student
            ])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['programme.code'] = [
            'asc' => ['lookup_programme.code' => SORT_ASC],
            'desc' => ['lookup_programme.code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'personal_detail.id' => $this->id,
            'personal_detail.id_user' => $this->id_user,
            'personal_detail.id_programme' => $this->id_programme,
            'personal_detail.id_state' => $this->id_state,
            'personal_detail.id_attachment' => $this->id_attachment,
            'personal_detail.nric_no' => $this->nric_no,
            'personal_detail.gender_no' => $this->gender_no,
            'personal_detail.status' => $this->status,
            'personal_detail.created_at' => $this->created_at,
            'personal_detail.updated_at' => $this->updated_at,
            'personal_detail.deleted_at' => $this->deleted_at,
            'lookup_programme.id' => $this->getAttribute('programme.code'),
            'supervision.id_supervisor' => $this->id_supervisor,
        ]);

        $query->andFilterWhere(['like', 'personal_detail.user_no', $this->user_no])
            ->andFilterWhere(['like', 'personal_detail.name', $this->name])
            ->andFilterWhere(['like', 'personal_detail.religion', $this->religion])
            ->andFilterWhere(['like', 'personal_detail.address', $this->address])
            ->andFilterWhere(['like', 'personal_detail.tel_no', $this->tel_no])
            ->andFilterWhere(['like', 'user.email', $this->getAttribute('user.email')]);

        return $dataProvider;
    }

        /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAssignStudent($params)
    {
        $this->load($params);

        $query = PersonalDetail::find()
            ->select([
                'personal_detail.id',
                'personal_detail.id_programme',
                'personal_detail.name',
                'personal_detail.user_no',
                'lookup_programme.code',
                'personal_detail.tel_no',
                '(CASE
                    WHEN internship_application.id IS NULL THEN "no_placement_yet"
                    WHEN supervision.id_supervisor = '. $this->id_supervisor .' && supervision.status = 1 THEN "current_supervisor"
                    WHEN supervision.id_supervisor IS NOT NULL && supervision.status = 1 THEN "other_supervisor"
                    ELSE null
                END) AS student_status',
            ])
            ->joinWith([
                'user',
                'programme',
            ])
            ->leftJoin('supervision', 'supervision.id_supervisee = personal_detail.id AND supervision.status = 1')
            ->leftJoin('personal_detail supervisor', 'supervisor.id = supervision.id_supervisor')
            ->joinWith(['internshipApplications'])
            ->where(['user.role_no' => 4]) // Student
            ->active();
 
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['programme.code'] = [
            'asc' => ['lookup_programme.code' => SORT_ASC],
            'desc' => ['lookup_programme.code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['supervisee.supervisor.name'] = [
            'asc' => ['supervisor.name' => SORT_ASC],
            'desc' => ['supervisor.name' => SORT_DESC],
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'personal_detail.id' => $this->id,
            'personal_detail.id_user' => $this->id_user,
            'personal_detail.id_programme' => $this->id_programme,
            'personal_detail.id_state' => $this->id_state,
            'personal_detail.id_attachment' => $this->id_attachment,
            'personal_detail.nric_no' => $this->nric_no,
            'personal_detail.gender_no' => $this->gender_no,
            'personal_detail.status' => $this->status,
            'personal_detail.created_at' => $this->created_at,
            'personal_detail.updated_at' => $this->updated_at,
            'personal_detail.deleted_at' => $this->deleted_at,
            'lookup_programme.id' => $this->getAttribute('programme.code'),
        ]);

        $query->andFilterWhere(['like', 'personal_detail.user_no', $this->user_no])
            ->andFilterWhere(['like', 'personal_detail.name', $this->name])
            ->andFilterWhere(['like', 'personal_detail.religion', $this->religion])
            ->andFilterWhere(['like', 'personal_detail.address', $this->address])
            ->andFilterWhere(['like', 'personal_detail.tel_no', $this->tel_no])
            ->andFilterWhere(['like', 'user.email', $this->getAttribute('user.email')])
            ->andFilterWhere(['like', 'supervisor.name', $this->getAttribute('supervisee.supervisor.name')]);

        return $dataProvider;
    }
}
