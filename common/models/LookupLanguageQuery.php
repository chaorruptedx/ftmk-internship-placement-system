<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[LookupLanguage]].
 *
 * @see LookupLanguage
 */
class LookupLanguageQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['lookup_language.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return LookupLanguage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LookupLanguage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
