<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[OrganizationDetail]].
 *
 * @see OrganizationDetail
 */
class OrganizationDetailQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['organization_detail.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return OrganizationDetail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return OrganizationDetail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
