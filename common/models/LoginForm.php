<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    const SCENARIO_ROLE_USER = 'role_user';
    const SCENARIO_ROLE_ADMIN = 'role_admin';

    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword', 'on' => 'role_user'],
            ['password', 'validatePasswordAdmin', 'on' => 'role_admin'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function validatePasswordAdmin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUserAdmin();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $this->scenario = self::SCENARIO_ROLE_USER;

        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }

    public function loginAdmin()
    {   
        $this->scenario = self::SCENARIO_ROLE_ADMIN;

        if ($this->validate())
        {
            return Yii::$app->user->login($this->getUserAdmin(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } 
        else
        {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUserAdmin()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsernameAdmin($this->username);
        }

        return $this->_user;
    }
}
