<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%skill}}".
 *
 * @property int $id
 * @property int $id_personal_detail
 * @property string $name
 * @property int $scale
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property PersonalDetail $personalDetail
 */
class Skill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%skill}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'scale'], 'required'],
            [['id_personal_detail', 'scale'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['scale'], 'integer', 'max' => 100],
            [['id_personal_detail'], 'exist', 'skipOnError' => true, 'targetClass' => PersonalDetail::className(), 'targetAttribute' => ['id_personal_detail' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_personal_detail' => Yii::t('app', 'Id Personal Detail'),
            'name' => Yii::t('app', 'Name'),
            'scale' => Yii::t('app', 'Scale'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[PersonalDetail]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getPersonalDetail()
    {
        return $this->hasOne(PersonalDetail::className(), ['id' => 'id_personal_detail']);
    }

    /**
     * {@inheritdoc}
     * @return SkillQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SkillQuery(get_called_class());
    }

    public function saveSkill($id_personal_detail)
    {
        $this->id_personal_detail = $id_personal_detail;
        $this->status = Helper::getStatus('status_active'); 
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public static function deleteSkills($ids)
    {
        foreach ($ids as $id)
        {
            $modelSkill = self::findOne($id);

            $modelSkill->status = Helper::getStatus('status_deleted');
            $modelSkill->deleted_at = time();
            
            $modelSkill->save();
        }

        return true;
    }
}
