<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use common\components\Helper;

/**
 * This is the model class for table "{{%attachment}}".
 *
 * @property int $id
 * @property int $category 1 = Profile Picture, 2 = Industrial Training Report
 * @property string $name
 * @property string $type
 * @property string $path
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 */
class Attachment extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%attachment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'on' => 'create'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'on' => 'update'],
            [['name', 'type', 'path'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'path' => Yii::t('app', 'Path'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[PersonalDetails]].
     *
     * @return \yii\db\ActiveQuery|PersonalDetailQuery
     */
    public function getPersonalDetail()
    {
        return $this->hasOne(PersonalDetail::className(), ['id_attachment' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AttachmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AttachmentQuery(get_called_class());
    }

    public function saveAttachment($id_user, $category)
    {
        if ($this->validate())
        {
            $path = Yii::getAlias('@upload') . "/" . $id_user . "/" . $category;
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);

            $this->category = $category;
            $this->name = $this->imageFile->baseName;
            $this->type = $this->imageFile->extension;
            $this->path = '/upload/'. $id_user . '/' . $category . '/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $this->status = Helper::getStatus('status_active');
            $this->deleted_at = Helper::getStatus('deleted_at_default');

            $this->imageFile->saveAs(Yii::getAlias('@upload'). '/' . $id_user . '/' . $category . '/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            
            $this->save(false);

            return true;
        }
        else
        {
            return false;
        }
    }
}
