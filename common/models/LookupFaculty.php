<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%lookup_faculty}}".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $deleted_at
 */
class LookupFaculty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lookup_faculty}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique', 'targetAttribute' => ['code', 'status', 'deleted_at'], 'message' => 'This {attribute} has already been taken.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Faculty Code'),
            'name' => Yii::t('app', 'Faculty Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return LookupFacultyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LookupFacultyQuery(get_called_class());
    }

    public function saveFaculty()
    {
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function deleteFaculty()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
