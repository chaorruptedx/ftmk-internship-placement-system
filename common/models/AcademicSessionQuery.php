<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[AcademicSession]].
 *
 * @see AcademicSession
 */
class AcademicSessionQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['in', 'academic_session.status', array(Helper::getStatus('status_active'), Helper::getStatus('status_inactive'))]);
    }

    /**
     * {@inheritdoc}
     * @return AcademicSession[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AcademicSession|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
