<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\components\Helper;

/**
 * Bank Status
 */
class BankStatus extends Model
{
    public static function listFaculty()
    {
        $lookupFacultyModel = LookupFaculty::find()
            ->select([
                'id',
                'CONCAT(code, " - ", name) AS faculty_name'
            ])
            ->active()
            ->asArray()
            ->all();

        $listFaculty = ArrayHelper::map($lookupFacultyModel, 'id', 'faculty_name');

        return $listFaculty;
    }

    public static function listDepartment()
    {
        $lookupDepartmentModel = LookupDepartment::find()
            ->select([
                'id',
                'CONCAT(code, " - ", name) AS department_name'
            ])
            ->active()
            ->asArray()
            ->all();

        $listDepartment = ArrayHelper::map($lookupDepartmentModel, 'id', 'department_name');

        return $listDepartment;
    }

    public static function listProgramme()
    {
        $lookupProgrammeModel = LookupProgramme::find()
            ->select([
                'id',
                'CONCAT(code, " - ", name) AS programme_name'
            ])
            ->active()
            ->asArray()
            ->all();

        $listProgramme = ArrayHelper::map($lookupProgrammeModel, 'id', 'programme_name');

        return $listProgramme;
    }

    public static function listAcademicSession()
    {
        $academicSessionModel = AcademicSession::find()
            ->select([
                'id',
                'CONCAT(semester, " - ", start_year, "/", end_year) AS academic_session'
            ])
            ->active()
            ->asArray()
            ->all();

        $listAcademicSession = ArrayHelper::map($academicSessionModel, 'id', 'academic_session');

        return $listAcademicSession;
    }

    public static function getAcademicSession($id)
    {
        $academicSessionModel = AcademicSession::find()
            ->select([
                'id',
                'CONCAT(semester, " - ", start_year, "/", end_year) AS academic_session'
            ])
            ->where([
                'id' => $id,
            ])
            ->asArray()
            ->one();

        return $academicSessionModel['academic_session'];
    }

    public static function getRoleNamesByIDAnnouncementDetailView($id_announcement)
    {
        $announcementRoleModel = AnnouncementRole::find()
            ->select([
                'id',
                'role_no'
            ])
            ->where(['id_announcement' => $id_announcement])
            ->active()
            ->asArray()
            ->all();

        $listAnnouncementRoleNo = ArrayHelper::map($announcementRoleModel, 'id', 'role_no');

        if (isset($listAnnouncementRoleNo) && is_array($listAnnouncementRoleNo))
        {
            $no = 0;

            foreach ($listAnnouncementRoleNo as $key => $value)
            {
                $role_names .= ++$no.'. '.Helper::getRoleName($value).'<br>';
            }
        }

        return $role_names;
    }

    public static function listSupervisor()
    {
        $personalDetail = PersonalDetail::find()
            ->select([
                'personal_detail.id',
                'personal_detail.name'
            ])
            ->joinWith([
                'user'
            ])
            ->where([
                'user.role_no' => 3, // Supervisor
            ])
            ->active()
            ->asArray()
            ->all();

        $listSupervisor = ArrayHelper::map($personalDetail, 'id', 'name');

        return $listSupervisor;
    }

    public static function listLanguage()
    {
        $lookupLanguageModel = LookupLanguage::find()
            ->select([
                'id',
                'name'
            ])
            ->active()
            ->asArray()
            ->all();

        $listLanguage = ArrayHelper::map($lookupLanguageModel, 'id', 'name');

        return $listLanguage;
    }

    public static function listState()
    {
        $lookupStateModel = LookupState::find()
            ->select([
                'id',
                'name'
            ])
            ->active()
            ->asArray()
            ->all();

        $listState = ArrayHelper::map($lookupStateModel, 'id', 'name');

        return $listState;
    }

    public static function listOrganizationDetail()
    {
        $modelOrganizationDetail = OrganizationDetail::find()
            ->select([
                'id',
                'name'
            ])
            ->active()
            ->asArray()
            ->all();

        $listOrganizationDetail = ArrayHelper::map($modelOrganizationDetail, 'id', 'name');

        return $listOrganizationDetail;
    }

    public static function listLecturer()
    {
        $personalDetail = PersonalDetail::find()
            ->select([
                'personal_detail.id',
                'personal_detail.name'
            ])
            ->joinWith([
                'user'
            ])
            ->where(['in', 'user.role_no', array('2', '3')]) // Cooridnator and Supervisor
            ->active()
            ->asArray()
            ->all();

        $listLecturer = ArrayHelper::map($personalDetail, 'id', 'name');

        return $listLecturer;
    }
}