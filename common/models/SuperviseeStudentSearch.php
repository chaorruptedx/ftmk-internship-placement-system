<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Supervision;

/**
 * SuperviseeStudentSearch represents the model behind the search form of `common\models\Supervision`.
 */
class SuperviseeStudentSearch extends Supervision
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['supervisee.name', 'supervisee.user_no', 'supervisee.programme.code', 'supervisee.user.email', 'supervisee.tel_no']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_supervisor', 'id_supervisee', 'id_academic_session', 'status', 'created_at', 'updated_at', 'deleted_at', 'supervisee.tel_no'], 'integer'],
            [['supervisee.name', 'supervisee.user_no', 'supervisee.programme.code', 'supervisee.user.email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Supervision::find()
            ->joinWith([
                'supervisee.user',
                'supervisee.programme',
            ])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['supervisee.name'] = [
            'asc' => ['personal_detail.name' => SORT_ASC],
            'desc' => ['personal_detail.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['supervisee.user_no'] = [
            'asc' => ['personal_detail.user_no' => SORT_ASC],
            'desc' => ['personal_detail.user_no' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['supervisee.programme.code'] = [
            'asc' => ['lookup_programme.code' => SORT_ASC],
            'desc' => ['lookup_programme.code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['supervisee.user.email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['supervisee.tel_no'] = [
            'asc' => ['personal_detail.tel_no' => SORT_ASC],
            'desc' => ['personal_detail.tel_no' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'supervision.id' => $this->id,
            'supervision.id_supervisor' => $this->id_supervisor,
            'supervision.id_supervisee' => $this->id_supervisee,
            'supervision.id_academic_session' => $this->id_academic_session,
            'supervision.status' => $this->status,
            'supervision.created_at' => $this->created_at,
            'supervision.updated_at' => $this->updated_at,
            'supervision.deleted_at' => $this->deleted_at,
            'lookup_programme.id' => $this->getAttribute('supervisee.programme.code'),
        ]);

        $query->andFilterWhere(['like', 'personal_detail.name', $this->getAttribute('supervisee.name')])
            ->andFilterWhere(['like', 'personal_detail.user_no', $this->getAttribute('supervisee.user_no')])
            ->andFilterWhere(['like', 'user.email', $this->getAttribute('supervisee.user.email')])
            ->andFilterWhere(['like', 'personal_detail.tel_no', $this->getAttribute('supervisee.tel_no')]);

        return $dataProvider;
    }
}
