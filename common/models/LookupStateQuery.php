<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[LookupState]].
 *
 * @see LookupState
 */
class LookupStateQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['lookup_state.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return LookupState[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LookupState|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
