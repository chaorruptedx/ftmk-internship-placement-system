<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LookupLanguage;

/**
 * LanguageManagementSearch represents the model behind the search form of `common\models\LookupLanguage`.
 */
class LanguageManagementSearch extends LookupLanguage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupLanguage::find()->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lookup_language.id' => $this->id,
            'lookup_language.status' => $this->status,
            'lookup_language.created_at' => $this->created_at,
            'lookup_language.updated_at' => $this->updated_at,
            'lookup_language.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'lookup_language.name', $this->name]);

        return $dataProvider;
    }
}
