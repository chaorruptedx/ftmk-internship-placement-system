<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[Education]].
 *
 * @see Education
 */
class EducationQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andFilterWhere(['education.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return Education[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Education|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
