<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%lookup_programme}}".
 *
 * @property int $id
 * @property int $id_department
 * @property string $code
 * @property string $name
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $deleted_at
 *
 * @property LookupFaculty $faculty
 */
class LookupProgramme extends \yii\db\ActiveRecord
{
    public $id_faculty;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lookup_programme}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_department', 'code', 'name'], 'required'],
            [['id_department', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique', 'targetAttribute' => ['code', 'status', 'deleted_at'], 'message' => 'This {attribute} has already been taken.'],
            [['id_department'], 'exist', 'skipOnError' => true, 'targetClass' => LookupDepartment::className(), 'targetAttribute' => ['id_department' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_faculty' => Yii::t('app', 'Faculty'),
            'id_department' => Yii::t('app', 'Department Code'),
            'code' => Yii::t('app', 'Programme Code'),
            'name' => Yii::t('app', 'Programme Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[Faculty]].
     *
     * @return \yii\db\ActiveQuery|LookupFacultyQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(LookupDepartment::className(), ['id' => 'id_department']);
    }

    /**
     * {@inheritdoc}
     * @return LookupProgrammeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LookupProgrammeQuery(get_called_class());
    }

    public function saveProgramme()
    {
        $this->status = Helper::getStatus('status_active');
        $this->deleted_at = Helper::getStatus('deleted_at_default');

        if (!$this->validate()) {
            return null;
        }

        return $this->save();
    }

    public function deleteProgramme()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
