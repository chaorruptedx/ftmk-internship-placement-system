<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\Helper;

/**
 * This is the model class for table "{{%announcement_role}}".
 *
 * @property int $id
 * @property int $id_announcement
 * @property int $role_no
 * @property int $status 1 = Active, 0 = Inactive, -1 = Deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 *
 * @property Announcement $announcement
 */
class AnnouncementRole extends \yii\db\ActiveRecord
{
    public $role_no_array;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%announcement_role}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_no_array'], 'required'],
            [['id_announcement', 'role_no', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['id_announcement'], 'exist', 'skipOnError' => true, 'targetClass' => Announcement::className(), 'targetAttribute' => ['id_announcement' => 'id']],
            [['role_no_array'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_announcement' => Yii::t('app', 'ID Announcement'),
            'role_no' => Yii::t('app', 'Role Name'),
            'role_no_array' => Yii::t('app', 'Role Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[Announcement]].
     *
     * @return \yii\db\ActiveQuery|AnnouncementQuery
     */
    public function getAnnouncement()
    {
        return $this->hasOne(Announcement::className(), ['id' => 'id_announcement']);
    }

    /**
     * {@inheritdoc}
     * @return AnnouncementRoleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AnnouncementRoleQuery(get_called_class());
    }

    public function saveAnnouncementRole($id_announcement)
    {
        $modelAnnouncementRoles = AnnouncementRole::find()
            ->where(['id_announcement' => $id_announcement])
            ->active()
            ->all();

        if (isset($modelAnnouncementRoles) && is_array($modelAnnouncementRoles)) // Delete All Old Data
        {
            foreach ($modelAnnouncementRoles as $modelAnnouncementRole)
            {
                $modelAnnouncementRole->role_no_array = true;
                $modelAnnouncementRole->deleteAnnouncementRole();
            }
        }

        if (isset($this->role_no_array) && is_array($this->role_no_array)) // Insert New Data
        {
            foreach ($this->role_no_array as $key => $value)
            {
                $modelAnnouncementRole = new AnnouncementRole;

                $modelAnnouncementRole->id_announcement = $id_announcement;
                $modelAnnouncementRole->role_no = $value;
                $modelAnnouncementRole->role_no_array = true;
                $modelAnnouncementRole->status = Helper::getStatus('status_active');
                $modelAnnouncementRole->deleted_at = Helper::getStatus('deleted_at_default');
        
                if (!$modelAnnouncementRole->validate()) {
                    return null;
                }
                else
                {
                    $modelAnnouncementRole->save();
                }
            }
        }

        return true;
    }

    public function deleteAnnouncementRole()
    {
        $this->status = Helper::getStatus('status_deleted');
        $this->deleted_at = time();

        return $this->save();
    }
}
