<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InternshipApplication;

class StudentDashboardSearch extends InternshipApplication
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['organizationDetail.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['application_status'], 'integer'],
            [['organizationDetail.name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchInternshipApplication($params, $id_personal_detail)
    {
        $query = InternshipApplication::find()
            ->where([
                'id_personal_detail' => $id_personal_detail,
            ])
            ->joinWith(['organizationDetail'])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['organizationDetail.name'] = [
            'asc' => ['organization_detail.name' => SORT_ASC],
            'desc' => ['organization_detail.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'internship_application.application_status' => $this->application_status,
        ]);

        $query->andFilterWhere(['like', 'organization_detail.name', $this->getAttribute('organizationDetail.name')]);

        return $dataProvider;
    }
}