<?php

namespace common\models;
use common\components\Helper;

/**
 * This is the ActiveQuery class for [[LookupFaculty]].
 *
 * @see LookupFaculty
 */
class LookupFacultyQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andFilterWhere(['lookup_faculty.status' => Helper::getStatus('status_active')]);
    }

    /**
     * {@inheritdoc}
     * @return LookupFaculty[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LookupFaculty|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
