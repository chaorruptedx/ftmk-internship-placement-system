<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LookupProgramme;

/**
 * ProgrammeManagementSearch represents the model behind the search form of `common\models\LookupProgramme`.
 */
class ProgrammeManagementSearch extends LookupProgramme
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['department.code']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_department', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['code', 'name', 'department.code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupProgramme::find()
            ->joinWith([
                'department'
            ])
            ->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['department.code'] = [
            'asc' => ['lookup_department.code' => SORT_ASC],
            'desc' => ['lookup_department.code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lookup_programme.id' => $this->id,
            'lookup_programme.id_department' => $this->id_department,
            'lookup_programme.status' => $this->status,
            'lookup_programme.created_at' => $this->created_at,
            'lookup_programme.updated_at' => $this->updated_at,
            'lookup_programme.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'lookup_programme.code', $this->code])
            ->andFilterWhere(['like', 'lookup_programme.name', $this->name])
            ->andFilterWhere(['like', 'lookup_department.code', $this->getAttribute('department.code')]);

        return $dataProvider;
    }
}
